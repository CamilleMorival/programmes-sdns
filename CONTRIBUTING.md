# Comment contribuer

Cette page liste des bonnes pratiques pour contribuer à ce dépôt.

## Fichier README.md

Chaque dossier doit comporter un fichier `README.md`.
 
Ce fichier décrit le contenu du dossier. Il est affiché par GitLab lors de la navigation.

Le texte du fichier `README.md` est rédigé au format [Markdown](https://fr.wikipedia.org/wiki/Markdown#Quelques_exemples).

## Ajout d'une organisation

Pour ajouter une organisation, il faut 
- ajouter les membres de l'organisation comme "developpeurs" du projet, à partir de leurs identifiants de compte GitLab
- ajouter un dossier dédié à la racine
- présenter l'organisation dans le README du dossier
  - description succinte
  - résumé des activités en lien avec le SNDS
  - noms et contacts des responsables de la maintenance du dossier, sachant que des mainteneurs spécifiques peuvent être défini pour chaque sous-dossier, voire chaque fichier 
  - lien vers une page internet de l'organisation.


## Structure d'un dossier d'organisation

Chaque organisation définie une structure de dossiers pour classer ses programmes.

Typiquement, chaque dossier doit comporter au maximum 20 programmes. Au-delà, il est recommandé de créer des sous-dossiers thématiques.


## Éléments de contexte sur les programmes

Chaque programme doit être accompagné d'informations de contextes, telles que :
  - auteur•e•s ;
  - sujet et contexte d'étude ;
  - dates d'écriture ;
  - données étudiées ;
  - période étudié ;
  - environnement d'exécution (portail SAS de la CNAM, système fils, etc.) ;
  - nombre d'utilisateurs ;
  - processus de relecture éventuel ;
  - etc.
 
Pour des programmes contenus dans un seul fichier, il semble préférable d'intégrer ces informations comme commentaire au début du fichier. Ajouter alors une courte présentation de chaque programme dans le README du dossier.

Pour des programmes qui sont divisés en plusieurs fichiers, intégrer 
toutes ces informations dans le README du dossier.

## Informations générales sur la thématique 

Les éléments de contexte associés au programme doivent être spécifique à ce programme.
 
Les informations plus générale sur une thématique doivent être renseignées dans une [fiche](http://documentation-snds.health-data-hub.fr/fiches/) de la documentation du SNDS.

Il s'agit de créer une fiche thématique si elle n'existe pas, ou de compléter une fiche existante avec ses connaissances. 
Il est notamment intéressant d'ajouter à la fiche un lien vers le programme sur gitlab.
