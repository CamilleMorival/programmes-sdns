/* Object du programme  : utlisation de la table cr��e par la CNAMTS pour les pathologies*/
/*Date du programme : 10/10/16*/

/*Base de donn�es : DCIR*/


* on r�cup�re dans la table carto les donn�es normandes pour le RG/SLM ;
proc sql; 
drop table carto; 
%connectora; 
create table carto as select *
from connection to oracle(
select a.*,  ben_idt_ano,b.ben_nir_psa, b.ben_rng_gem, old_cod_reg, ben_res_com, ben_res_dpt
from  CT_IND_2015_G4 a, ct_IDE_2015_G4 b, IR_BEN_R c
where regime in ('RG', 'RG/SLM', 'SLM')
and a.id_carto=b.id_carto
and b.ben_rng_gem=c.ben_rng_gem
and b.ben_nir_psa=c.ben_nir_psa
and old_cod_reg in ('23', '25')) ;
disconnect from oracle ; 
quit;

proc sql ;
create table tempoResu as
select source, count(*) as nb ,
count(*)*100/(select count(*) from carto) , count(*)*100/(select count(*) from carto) 
from carto
 group by source ; run; 



proc sql;
%connectora;
select * from connection to oracle (
select count(distinct ben_idt_ano)as nbben from carto2
);
disconnect from oracle;
quit;


/* R�cup�ration des TOP des diff�rentes Pathologies par individus et de leurs communes */
proc sql;
create table carto2 as select distinct
ben_idt_ano,
ben_nir_psa,
ben_rng_gem,
ben_sex_cod,
dpt,
cla_age_5,
TOP_ABPCOIR_IND,
TOP_ALDAUTR_IND,
TOP_CANAUTR_ACT,
TOP_CANAUTR_SUR,
TOP_CANCOLO_ACT,
TOP_CANCOLO_SUR,
TOP_CANPOUM_ACT,
TOP_CANPOUM_SUR,
TOP_CANPROS_ACT,
 TOP_CANPROS_SUR,
TOP_CANSEIF_ACT,
TOP_CANSEIF_SUR,
TOP_CVAOMI_IND,
TOP_CVAUTRE_IND,
TOP_CVAVC_AIG,
TOP_CVAVC_SEQ,
TOP_CVCORON_CHR,
TOP_CVEMBOL_AIG,
TOP_CVIC_AIG,
TOP_CVIC_CHR,
TOP_CVIDM_AIG,
TOP_CVTRRYC_IND,
TOP_CVVALVE_IND,
TOP_FANTIHTA_MED,
TOP_FDIABET_IND,
TOP_FHYPOLI_MED,
TOP_HFOIPAN_IND,
TOP_IRAUTRE_IND,
TOP_IRCRRCH_IND,
TOP_IRHEMOP_IND,
TOP_IRMMHER_IND,
TOP_IRMUCO_IND,
TOP_IRPOLYA_IND,
TOP_IRSPONA_IND,
TOP_IRVIH_IND,
TOP_MATERNI_IND,
TOP_NAUTRES_IND,
TOP_NDEMENC_IND,
TOP_NEPILEP_IND,
TOP_NMYOMYA_IND,
TOP_NPARAPL_IND,
TOP_NPARKIN_IND,
TOP_NSEPLAQ_IND,
TOP_PADDICT_IND,
TOP_PANTIDE_MED,
TOP_PANXIOL_MED,
TOP_PDEPNEV_IND,
TOP_PHYPNOT_MED,
TOP_PNEUROL_MED,
TOP_PRETARD_IND,
TOP_PSYAUTR_IND,
TOP_PSYCHOS_IND,
TOP_PTRENFA_IND,
TOP_RDIALYSE_IND,
TOP_RTRANS_AIG,
TOP_RTRANS_CHR,
sup_PAddAlc_ind,
sup_PAddTab_ind,
sup_PAddCan_ind,
sup_PAddAut_ind,
ben_res_dpt, /* du referentiel des b�n�ficiaires ie derni�re adresse connue*/
ben_res_com /* du ref�rentiel des b�n�ficiaires ie derniere adresse connue*/
from carto;
run;

/* On met dans ORACLE la table Cr�e pour la jointure avec latable ER_PRS_F (prestations) */
data orauser.carto2; set carto2;
run;

proc sql;
select count(*) from orauser.carto2;
quit;


/*on recupere la commune au moment des soins de 2015 pour pouvoir corriger les codes communes des b�n�ficiaires 
qui ne sont plus Normands en 2017 mais qui l'�tait en 2015 et corriger ceux dont le code du r�f�rentiel estinconnu ou null*/
****modfication jerome brocca pour all�ger la requ�te ;*/



/* Mois de debut FLX avec un mois de d�calage */
/* Premier flux et dernier flux*/;

%let flxdeb_n2 =201502 ;/*YYYYMM*/
%let flxfin_n2 =201601 ;/*YYYYMM*/


/***La macro sert � compiler les r�sultats de la requ�te mois par mois**/

%macro compil(mois,annee);

proc sql;
    %connectora;
        CREATE TABLE extract as select * from connection to oracle
(select a.ben_idt_ano, a.ben_nir_psa, a.ben_rng_gem,exe_soi_dtd, b.ben_res_com, b.ben_res_dpt, 
count (a.ben_idt_ano ) as compteur,
		max(flx_dis_dtd) as fluxdis
from  carto2 a , er_prs_f b
where (a.ben_nir_psa=b.ben_nir_psa
and a.ben_rng_gem=b.ben_rng_gem)
		and    flx_dis_dtd=to_date(%str(%'&Annee&Mois%'),'YYYYMM')
group by  a.ben_idt_ano,a.ben_nir_psa, a.ben_rng_gem,exe_soi_dtd, b.ben_res_com, b.ben_res_dpt);
    disconnect from oracle;
    quit;

	/**Le premier mois la macro charge dans ADRESSE EXTRACT **/
%IF %str(%'&Annee&Mois%')=%str(%'&flxdeb_n2%') %THEN %DO ;
					data adresse;
							set extract;
							run;
							%END;
   /*LES MOIS SUIVANTS CA COMPILE LES INFOS */
 %ELSE %DO;
          proc datasets force;    
           append base=adresse 
           data=extract;  
           run;
%END;
%mend compil;


 /*Appel de la Macro sur les flux de Janvier 2015 � D�cembre 2015 i.e ==>Flux mis � dispo le 1er jour du mois d'apr�s**/ 
%compil(02,2015);
%compil(03,2015);
%compil(04,2015);
%compil(05,2015);
%compil(06,2015);
%compil(07,2015);
%compil(08,2015);
%compil(09,2015);
%compil(10,2015);
%compil(11,2015);
%compil(12,2015);
%compil(01,2016);

/***fin de modif Jerome Brocca */ ;

/** Affectation du Minimum du code commune pour ceux qui en ont plusieurs � la premi�re  date de soins **/
proc sql; 
drop table sasdata1.adresse; 
create table sasdata1.adresse  as 
 select distinct a.ben_idt_ano,a.ben_nir_psa, a.ben_rng_gem, min(a.ben_res_com) as ben_res_com, 
min (a.ben_res_dpt) as ben_res_dpt
	from (select ben_idt_ano,ben_nir_psa, ben_rng_gem,min (exe_soi_dtd) as exe_soi_dtd
			from adresse
			where ben_res_com ne '000' and ben_res_dpt ne '999'
			group by 1) as b, adresse as a
where a.ben_nir_psa=b.ben_nir_psa 
and a.ben_rng_gem=b.ben_rng_gem
/*and a.ben_idt_ano=b.ben_idt_ano*/
and a.exe_soi_dtd=b.exe_soi_dtd
group by 1; 
quit; 

/**V�rification*/
proc sql; 
select count (*)
from sasdata1.adresse; 
/*select count (*)
from work.adresse; */
select ben_nir_psa, ben_rng_gem, count (*)
from sasdata1.adresse
group by 1,2
/*having count (*)>1*/; 
quit; 



/* On affecte la commune de soins � la table (jointure � gauche pour pas en perdre si pas de soins)*/
proc sql; 
create table sasdata1.carto3 as 
select a.*, b.ben_res_com as ben_res_com2, b.ben_res_dpt as ben_res_dpt2
from carto2 as a left join sasdata1.adresse as b
on (a.ben_idt_ano=b.ben_idt_ano); 
quit; 


proc sql;
create table test as select 
ben_idt_ano, 
dpt as dpt_carto,
ben_res_dpt as dpt_referentiel,
ben_res_dpt2 as dpt_soin,
ben_res_dpt||ben_res_com as codcom_carto2015,
ben_res_dpt2||ben_res_com2 as codcom_soin2015
from carto3;
quit;

data test2; set test;
if codcom_soin2015=" " then codcom_soi2015=codcom_carto2015;
run;
proc sql; 
select count (*)
from carto; 
select count (*)
from carto3; 
select count (*)
from carto3
where ben_res_com = "000"; 
quit; 


* On reconstitue le code INSEE du R�f�rentiel b�n�f et celle de soins;
data sasdata1.carto4; set sasdata1.carto3;
codcom_adr=substr(ben_res_dpt,2,2)||ben_res_com;
codcom_soins=substr(ben_res_dpt2,2,2)||ben_res_com2;
run;
proc sql;
select count(*) from sasdata1.carto4;
select count(*) from sasdata1.carto4 where codcom_adr=codcom_soins;
select count(*) from sasdata1.carto4 where codcom_adr="00000" or codcom_soins="00000";
select count(*) from sasdata1.carto4 where codcom_adr="00000";
quit;


proc sql;
select distinct ben_idt_ano, count(*) from sasdata1.carto4
group by 1;
quit;

* On affecte le benef � sa derni�re adresse connue si il est dans un d�pt de Normandie. Sinon on prend celle des soins;
data sasdata1.carto4; set sasdata1.carto4; 
if substr(codcom_adr,1,2) in ('14','50','61','27','76')then codcom_tot=codcom_adr ;else codcom_tot=codcom_soins;
run;

* estimation de la perte ;

proc sql;
select count(distinct ben_idt_ano) from sasdata1.carto4 
where codcom_tot="";
quit;


* on tente de r�cup�rer les codgeo PMSI pour limiter notre perte ;
* extraction du ben_nir_psa qui correspond au nir_ano_17 dans le PMSI ; 
proc sql;
create table nir_pmsi as select distinct ben_nir_psa from sasdata1.carto4 where codcom_tot="";
quit;

* on met la table dans orauser pour pouvoir faire la jointure avec les tables du PMSI ;
Data orauser.nir_pmsi; set nir_pmsi;
run;

* Recherche des Codes PMSI MCO de 2015; 
proc sql;
drop table geo_pmsi;
%connectora;
create table geo_pmsi as select * from connection to oracle (
select distinct NIR_ANO_17,
bdi_cod
FROM  T_MCO15B t1, T_MCO15C t2
WHERE t1.rsa_num=t2.rsa_num 
and T1.eta_num=t2.eta_num
and nir_ano_17 in (select ben_nir_psa from nir_pmsi)
order by bdi_cod
);
disconnect from oracle;
quit;
proc sql;
select count(*) from geo_pmsi;
quit;

proc sql;
select nir_ano_17,count(*) from geo_pmsi 
where substr(bdi_cod,1,2) in ('14','50','61','27','76')
group by 1 ;
quit;

* on passe table de corresponce entre le code PMSI qui est un regroupement de codes postaux et le code commune dans les 
bases AM ;

proc sql;
create table geo_pmsi_ok as select distinct nir_ano_17, bdi_cod,code_com, code_geo, pop 
from geo_pmsi left join  sasdata1.pmsi_codgeo 
on bdi_cod=code_geo
where annee="2015"
and substr(bdi_cod,1,2) in ('14','50','61','27','76');
quit;

*A 1 codegeo PMSI correspond plusieurs communes, pour ne pas doublonner les lignes, on choisit d'affecter 
l individu � la commune qui a la population la plus importante ;

proc sql;
create table temp as 
select distinct nir_ano_17, max(pop)as pop from geo_pmsi_ok  
group by 1;
quit;

proc sql;
create table temp2 as 
select distinct * from geo_pmsi_OK
where (nir_ano_17||put(pop,6.)) in (select (nir_ano_17||put(pop,6.)) from temp)
;
quit;

proc sql;
create table patho_code_PMSI as
select distinct nir_ano_17, code_com as codcom_pmsi from temp2;
quit;



proc sql;
create table sasdata1.carto5 as select * from sasdata1.carto4 left join patho_code_PMSI 
on nir_ano_17=ben_nir_psa ;
quit;
proc sql;
select count(*) from sasdata1.carto4;
select count(*) from sasdata1.carto5;
select count(*) from sasdata1.carto5 where codcom_PMSI <> "";
quit;

/* Si on ne trouve ni adresse normanade ni commune de soins normande on affecte le code INSEE de la commune la plus peupl�e du code PMSI */
data sasdata1.carto5; set sasdata1.carto5;
if (codcom_adr in ("","00000") or codcom_soins in ("","00000"))and codcom_PMSI <>"" 
then codcom_tot=codcom_PMSI;
run;

proc sql;
select count(*) from sasdata1.carto4 where codcom_tot in ("","00000");
select count(*) from sasdata1.carto5 where codcom_tot in ("","00000");
quit;




**** on fait passer le programme de correction des codes communes table de Michel Seguin ;


proc sql; 
create table sasdata1.carto6 as select * from sasdata1.carto5 left join sasdata1.T_fin_geo_loc_france  
on codcom_tot=code_jointure;
quit;
proc sql;
select count(*) from sasdata1.carto5;
quit;
proc sql;
select count(*) from sasdata1.carto6;
quit;



******standardisation des taux ; 

data sasdata1.carto6; set sasdata1.carto6;
length  class_age $10 sexe $8;
if cla_age_5 in ("00-04","05-09") then class_age="[0-9]";
if cla_age_5 in ("10-14","15-19") then class_age="[10-19]";
if cla_age_5 in ("20-24","25-29") then class_age="[20-29]";
if cla_age_5 in ("30-34","35-39") then class_age="[30-39]";
if cla_age_5 in ("40-44","45-49") then class_age="[40-49]";
if cla_age_5 in ("50-54","55-59") then class_age="[50-59]";
if cla_age_5 in ("60-64","65-69") then class_age="[60-69]";
if cla_age_5 in ("70-74","75-79") then class_age="[70-79]";
if cla_age_5 in ("80-84","85-89") then class_age="[80-89]";
if cla_age_5 in ("90-94","95et+") then class_age="[90+]";
/*if ben_sex_cod in (1) then sexe="homme" ;else sexe="femme";*/
/*agreg=compress(code_insee||ben_sex_cod||class_age)*/;
run;
** creation 'une table communale qui va rester dans le portail ;
proc sql;
create table sasdata1.carto6B as select 
code_insee,
BEN_SEX_COD,
class_age, 
sum(TOP_ABPCOIR_IND)as TOP_ABPCOIR_IND,
sum(TOP_CANAUTR_ACT) as TOP_CANAUTR_ACT,
sum(TOP_CANAUTR_SUR)as TOP_CANAUTR_SUR,
sum(TOP_CANCOLO_ACT) as TOP_CANCOLO_ACT,
sum(TOP_CANCOLO_SUR) as TOP_CANCOLO_SUR,
sum(TOP_CANPOUM_ACT) as TOP_CANPOUM_ACT,
sum(TOP_CANPOUM_SUR) as TOP_CANPOUM_SUR,
sum(TOP_CANPROS_ACT) as TOP_CANPROS_ACT,
sum(TOP_CANPROS_SUR) as TOP_CANPROS_SUR,
sum(TOP_CANSEIF_ACT) as TOP_CANSEIF_ACT,
sum(TOP_CANSEIF_SUR) as TOP_CANSEIF_SUR,
sum(TOP_CVAOMI_IND) as TOP_CVAOMI_IND,
sum(TOP_CVAUTRE_IND) as TOP_CVAUTRE_IND,
sum(TOP_CVAVC_AIG) as TOP_CVAVC_AIG ,
sum(TOP_CVAVC_SEQ) as TOP_CVAVC_SEQ,
sum(TOP_CVCORON_CHR) as TOP_CVCORON_CHR,
sum(TOP_CVEMBOL_AIG) as TOP_CVEMBOL_AIG,
sum(TOP_CVIC_AIG) as TOP_CVIC_AIG,
sum(TOP_CVIC_CHR) as TOP_CVIC_CHR,
sum(TOP_CVIDM_AIG) as TOP_CVIDM_AIG,
sum(TOP_CVTRRYC_IND) as TOP_CVTRRYC_IND,
sum(TOP_CVVALVE_IND) as TOP_CVVALVE_IND,
sum(TOP_FANTIHTA_MED) as TOP_FANTIHTA_MED,
sum(TOP_FDIABET_IND) as TOP_FDIABET_IND,
sum(TOP_FHYPOLI_MED) as TOP_FHYPOLI_MED,
sum(TOP_HFOIPAN_IND) as TOP_HFOIPAN_IND,
sum(TOP_IRAUTRE_IND) as TOP_IRAUTRE_IND,
sum(TOP_IRCRRCH_IND) as TOP_IRCRRCH_IND,
sum(TOP_IRHEMOP_IND) as TOP_IRHEMOP_IND,
sum(TOP_IRMMHER_IND) as TOP_IRMMHER_IND,
sum(TOP_IRMUCO_IND) as TOP_IRMUCO_IND,
sum(TOP_IRPOLYA_IND) as TOP_IRPOLYA_IND,
sum(TOP_IRSPONA_IND) as TOP_IRSPONA_IND,
sum(TOP_IRVIH_IND)as TOP_IRVIH_IND,
sum(TOP_MATERNI_IND) as TOP_MATERNI_IND,
sum(TOP_NAUTRES_IND) as TOP_NAUTRES_IND,
sum(TOP_NDEMENC_IND) as TOP_NDEMENC_IND ,
sum(TOP_NEPILEP_IND)as TOP_NEPILEP_IND,
sum(TOP_NMYOMYA_IND) as TOP_NMYOMYA_IND,
sum(TOP_NPARAPL_IND) as TOP_NPARAPL_IND,
sum(TOP_NPARKIN_IND) as TOP_NPARKIN_IND,
sum(TOP_NSEPLAQ_IND) as TOP_NSEPLAQ_IND,
sum(TOP_PADDICT_IND) as TOP_PADDICT_IND,
sum(TOP_PANTIDE_MED) as TOP_PANTIDE_MED,
sum(TOP_PANXIOL_MED) as TOP_PANXIOL_MED,
sum(TOP_PDEPNEV_IND) as TOP_PDEPNEV_IND,
sum(TOP_PHYPNOT_MED)as TOP_PHYPNOT_MED,
sum(TOP_PNEUROL_MED)as TOP_PNEUROL_MED,
sum(TOP_PRETARD_IND) as TOP_PRETARD_IND,
sum(TOP_PSYAUTR_IND) as TOP_PSYAUTR_IND,
sum(TOP_PSYCHOS_IND)as TOP_PSYCHOS_IND,
sum(TOP_PTRENFA_IND) as TOP_PTRENFA_IND,
sum(TOP_RDIALYSE_IND) as TOP_RDIALYSE_IND,
sum(TOP_RTRANS_AIG) as TOP_RTRANS_AIG,
sum(TOP_RTRANS_CHR)as TOP_RTRANS_CHR,
sum(sup_PAddAlc_ind) as TOP_ALCOOL,
sum(sup_PAddTab_ind) as TOP_TABAC,
sum(sup_PAddCan_ind) as TOP_canabis,
sum(sup_PAddAut_ind) as TOP_ADDICT_AUT
from sasdata1.carto6 
group by 1,2,3; 
quit; 


data sasdata1.carto6b; set sasdata1.carto6b;
agreg=compress(code_insee||ben_sex_cod||class_age);
agreg2=compress(ben_sex_cod||class_age);
if code_insee="" then delete;
run;

*** jointure avec le d�nominateur table pop_ref_carto ;

data sasdata1.pop_ref_carto; set sasdata1.pop_ref_carto;
agreg=compress(cod_comm||ben_sex_cod||class_age);
agreg2=compress(ben_sex_cod||class_age) ;
run;
proc sort data=sasdata1.pop_ref_carto;
by agreg;
run;
proc sort data=sasdata1.carto6b; 
by agreg;
run;
data sasdata1.carto6ter;
merge sasdata1.carto6b (in=a) sasdata1.pop_ref_carto (in=b);
by agreg;
if a ;
run;
data sasdata1.carto6ter; set sasdata1.carto6ter;
if class_age="" then delete;
if ben_sex_cod=0 then delete;
if cod_comm="" then delete;
run;
* ajout des taux r�gionaux pour standardis�s; 

proc sql;
create table com as select 
cod_comm as code_insee,
sum(nb_tot_ref) as nb_tot_ben_com
from sasdata1.pop_ref_carto
group by 1
;
quit;

proc sort data=sasdata1.carto6ter; 
by code_insee;
run;
data sasdata1.carto6ter2 ;
merge sasdata1.carto6ter (in=a) com;
by code_insee;
if a;
run;

proc sql;
create table com2 as select 
cod_comm as code_insee,
ben_sex_cod,
class_age,
sum(nb_tot_ref) as nb_tot_ben
from sasdata1.pop_ref_carto
group by 1,2,3
;
quit;
data com2; set com2; 
agreg=compress(code_insee||ben_sex_cod||class_age);
run;
proc sort data=com2;
 by agreg;
run;
proc sort data=sasdata1.carto6ter2;
by agreg;
run;
data sasdata1.carto6ter2b;
merge sasdata1.carto6ter2 (in=a) com2 ;
by agreg;
if a;
run;


/*******reg ****/ 
proc sql;
create table sasdata1.reg 
as select 
ben_sex_cod, 
class_age,
sum(nb_tot_ref) as nb_tot_ref_reg
from sasdata1.pop_ref_carto
group by 1,2
;
quit;
data SASdata1.reg; set sasdata1.reg;
agreg2=compress(ben_sex_cod||class_age);
if ben_sex_cod=0 then delete;
if class_age="" then delete;
run;
proc sort data=sasdata1.reg;
by agreg2;
run;
proc sort data=sasdata1.carto6ter2b;
by agreg2;
run;

data sasdata1.carto6ter3;
merge sasdata1.carto6ter2b sasdata1.reg;
by agreg2;
run;
data sasdata1.carto6ter3;
set sasdata1.carto6ter3;
length region $9;
region="Normandie";
run;
proc sql;
create table sasdata1.reg2 
as select  
"Normandie" as region,
sum(nb_tot_ref) as nb_tot_ref_reg_tot
from sasdata1.pop_ref_carto
where ben_sex_cod not in (0) 
and class_age not eq ""
groupe by 1;
quit;
data sasdata1.carto6ter4;
merge sasdata1.carto6ter3 SASDATA1.reg2;
by region;
run;




* france;
data sasdata1.nat; set sasdata1.pop_conso_nonconso_nat;
pays="france";
agreg2=compress(ben_sex_cod||class_age);
rename nb_tot_ref=nb_tot_ref_nat;
drop nb_conso_ref;
if ben_sex_cod=0 then delete;
if class_age="" then delete;
run;
proc sort data=sasdata1.nat;
by agreg2;
run;
proc sort data=sasdata1.carto6ter4;
by agreg2;
run;

data sasdata1.carto6ter5;
merge sasdata1.carto6ter4 sasdata1.nat;
by agreg2;
run;

data sasdata1.carto6ter5; set sasdata1.carto6ter5;
pays="france";
run;
proc sql;
create table sasdata1.nat2 as select "france" as pays,sum(nb_tot_ref_nat)as nb_tot_ref_nat_tot from sasdata1.nat
group by 1;
quit;
data sasdata1.carto6ter6;
merge sasdata1.carto6ter5 sasdata1.nat2;
by Pays;
run;
data sasdata1.carto6ter6; set sasdata1.carto6ter6;
pond_com=nb_tot_ben/nb_tot_ben_com;
pond_reg=nb_tot_ref_reg/nb_tot_ref_reg_tot;
pond_nat=nb_tot_ref_nat/nb_tot_ref_nat_tot;
run;


/******* calcul des taux pour standardisation ************************/

** calcul des effectifs pond�r�s reg et nationale ; 
proc sql;
create table sasdata1.carto6ter7 as select *,
((TOP_ABPCOIR_IND*pond_reg)/pond_com) AS effpondREG_TOP_ABPCOIR_IND,
((TOP_CANAUTR_ACT*pond_reg)/pond_com) AS effpondREG_TOP_CANAUTR_ACT,
((TOP_CANAUTR_SUR*pond_reg)/pond_com) AS effpondREG_TOP_CANAUTR_SUR,
((TOP_CANCOLO_ACT*pond_reg)/pond_com) AS effpondREG_TOP_CANCOLO_ACT,
((TOP_CANCOLO_SUR*pond_reg)/pond_com) AS effpondREG_TOP_CANCOLO_SUR,
((TOP_CANPOUM_ACT*pond_reg)/pond_com) AS effpondREG_TOP_CANPOUM_ACT,
((TOP_CANPOUM_SUR*pond_reg)/pond_com) AS effpondREG_TOP_CANPOUM_SUR,
((TOP_CANPROS_ACT*pond_reg)/pond_com) AS effpondREG_TOP_CANPROS_ACT,
((TOP_CANPROS_SUR*pond_reg)/pond_com) AS effpondREG_TOP_CANPROS_SUR,
((TOP_CANSEIF_ACT*pond_reg)/pond_com) AS effpondREG_TOP_CANSEIF_ACT,
((TOP_CANSEIF_SUR*pond_reg)/pond_com) AS effpondREG_TOP_CANSEIF_SUR,
((TOP_CVAOMI_IND*pond_reg)/pond_com) AS effpondREG_TOP_CVAOMI_IND,
((TOP_CVAUTRE_IND*pond_reg)/pond_com) AS effpondREG_TOP_CVAUTRE_IND,
((TOP_CVAVC_AIG*pond_reg)/pond_com) AS effpondREG_TOP_CVAVC_AIG,
((TOP_CVAVC_SEQ*pond_reg)/pond_com) AS effpondREG_TOP_CVAVC_SEQ,
((TOP_CVCORON_CHR*pond_reg)/pond_com) AS effpondREG_TOP_CVCORON_CHR,
((TOP_CVEMBOL_AIG*pond_reg)/pond_com) AS effpondREG_TOP_CVEMBOL_AIG,
((TOP_CVIC_AIG*pond_reg)/pond_com) AS effpondREG_TOP_CVIC_AIG,
((TOP_CVIC_CHR*pond_reg)/pond_com) AS effpondREG_TOP_CVIC_CHR,
((TOP_CVIDM_AIG*pond_reg)/pond_com) AS effpondREG_TOP_CVIDM_AIG,
((TOP_CVTRRYC_IND*pond_reg)/pond_com) AS effpondREG_TOP_CVTRRYC_IND,
((TOP_CVVALVE_IND*pond_reg)/pond_com) AS effpondREG_TOP_CVVALVE_IND,
((TOP_FANTIHTA_MED*pond_reg)/pond_com) AS effpondREG_TOP_FANTIHTA_MED,
((TOP_FDIABET_IND*pond_reg)/pond_com) AS effpondREG_TOP_FDIABET_IND,
((TOP_FHYPOLI_MED*pond_reg)/pond_com) AS effpondREG_TOP_FHYPOLI_MED,
((TOP_HFOIPAN_IND*pond_reg)/pond_com) AS effpondREG_TOP_HFOIPAN_IND,
((TOP_IRAUTRE_IND*pond_reg)/pond_com) AS effpondREG_TOP_IRAUTRE_IND,
((TOP_IRCRRCH_IND*pond_reg)/pond_com) AS effpondREG_TOP_IRCRRCH_IND,
((TOP_IRHEMOP_IND*pond_reg)/pond_com) AS effpondREG_TOP_IRHEMOP_IND,
((TOP_IRMMHER_IND*pond_reg)/pond_com) AS effpondREG_TOP_IRMMHER_IND,
((TOP_IRMUCO_IND*pond_reg)/pond_com) AS effpondREG_TOP_IRMUCO_IND,
((TOP_IRPOLYA_IND*pond_reg)/pond_com) AS effpondREG_TOP_IRPOLYA_IND,
((TOP_IRSPONA_IND*pond_reg)/pond_com) AS effpondREG_TOP_IRSPONA_IND,
((TOP_IRVIH_IND*pond_reg)/pond_com) AS effpondREG_TOP_IRVIH_IND,
((TOP_MATERNI_IND*pond_reg)/pond_com) AS effpondREG_TOP_MATERNI_IND,
((TOP_NAUTRES_IND*pond_reg)/pond_com) AS effpondREG_TOP_NAUTRES_IND,
((TOP_NDEMENC_IND*pond_reg)/pond_com) AS effpondREG_TOP_NDEMENC_IND,
((TOP_NEPILEP_IND*pond_reg)/pond_com) AS effpondREG_TOP_NEPILEP_IND,
((TOP_NMYOMYA_IND*pond_reg)/pond_com) AS effpondREG_TOP_NMYOMYA_IND,
((TOP_NPARAPL_IND*pond_reg)/pond_com) AS effpondREG_TOP_NPARAPL_IND,
((TOP_NPARKIN_IND*pond_reg)/pond_com) AS effpondREG_TOP_NPARKIN_IND,
((TOP_NSEPLAQ_IND*pond_reg)/pond_com) AS effpondREG_TOP_NSEPLAQ_IND,
((TOP_PADDICT_IND*pond_reg)/pond_com) AS effpondREG_TOP_PADDICT_IND,
((TOP_PANTIDE_MED*pond_reg)/pond_com) AS effpondREG_TOP_PANTIDE_MED,
((TOP_PANXIOL_MED*pond_reg)/pond_com) AS effpondREG_TOP_PANXIOL_MED,
((TOP_PDEPNEV_IND*pond_reg)/pond_com) AS effpondREG_TOP_PDEPNEV_IND,
((TOP_PHYPNOT_MED*pond_reg)/pond_com) AS effpondREG_TOP_PHYPNOT_MED,
((TOP_PNEUROL_MED*pond_reg)/pond_com) AS effpondREG_TOP_PNEUROL_MED,
((TOP_PRETARD_IND*pond_reg)/pond_com) AS effpondREG_TOP_PRETARD_IND,
((TOP_PSYAUTR_IND*pond_reg)/pond_com) AS effpondREG_TOP_PSYAUTR_IND,
((TOP_PSYCHOS_IND*pond_reg)/pond_com) AS effpondREG_TOP_PSYCHOS_IND,
((TOP_PTRENFA_IND*pond_reg)/pond_com) AS effpondREG_TOP_PTRENFA_IND,
((TOP_RDIALYSE_IND*pond_reg)/pond_com) AS effpondREG_TOP_RDIALYSE_IND,
((TOP_RTRANS_AIG*pond_reg)/pond_com) AS effpondREG_TOP_RTRANS_AIG,
((TOP_RTRANS_CHR*pond_reg)/pond_com) AS effpondREG_TOP_RTRANS_CHR,
((TOP_ALCOOL*pond_reg)/pond_com) AS effpondREG_TOP_ALCOOL,
((TOP_TABAC*pond_reg)/pond_com) AS effpondREG_TOP_TABAC,
((TOP_canabis*pond_reg)/pond_com) AS effpondREG_TOP_canabis,
((TOP_ADDICT_AUT*pond_reg)/pond_com) AS effpondREG_TOP_ADDICT_AUT,
((TOP_ABPCOIR_IND*pond_nat)/pond_com) AS effpondNAT_TOP_ABPCOIR_IND,
((TOP_CANAUTR_ACT*pond_nat)/pond_com) AS effpondNAT_TOP_CANAUTR_ACT,
((TOP_CANAUTR_SUR*pond_nat)/pond_com) AS effpondNAT_TOP_CANAUTR_SUR,
((TOP_CANCOLO_ACT*pond_nat)/pond_com) AS effpondNAT_TOP_CANCOLO_ACT,
((TOP_CANCOLO_SUR*pond_nat)/pond_com) AS effpondNAT_TOP_CANCOLO_SUR,
((TOP_CANPOUM_ACT*pond_nat)/pond_com) AS effpondNAT_TOP_CANPOUM_ACT,
((TOP_CANPOUM_SUR*pond_nat)/pond_com) AS effpondNAT_TOP_CANPOUM_SUR,
((TOP_CANPROS_ACT*pond_nat)/pond_com) AS effpondNAT_TOP_CANPROS_ACT,
((TOP_CANPROS_SUR*pond_nat)/pond_com) AS effpondNAT_TOP_CANPROS_SUR,
((TOP_CANSEIF_ACT*pond_nat)/pond_com) AS effpondNAT_TOP_CANSEIF_ACT,
((TOP_CANSEIF_SUR*pond_nat)/pond_com) AS effpondNAT_TOP_CANSEIF_SUR,
((TOP_CVAOMI_IND*pond_nat)/pond_com) AS effpondNAT_TOP_CVAOMI_IND,
((TOP_CVAUTRE_IND*pond_nat)/pond_com) AS effpondNAT_TOP_CVAUTRE_IND,
((TOP_CVAVC_AIG*pond_nat)/pond_com) AS effpondNAT_TOP_CVAVC_AIG,
((TOP_CVAVC_SEQ*pond_nat)/pond_com) AS effpondNAT_TOP_CVAVC_SEQ,
((TOP_CVCORON_CHR*pond_nat)/pond_com) AS effpondNAT_TOP_CVCORON_CHR,
((TOP_CVEMBOL_AIG*pond_nat)/pond_com) AS effpondNAT_TOP_CVEMBOL_AIG,
((TOP_CVIC_AIG*pond_nat)/pond_com) AS effpondNAT_TOP_CVIC_AIG,
((TOP_CVIC_CHR*pond_nat)/pond_com) AS effpondNAT_TOP_CVIC_CHR,
((TOP_CVIDM_AIG*pond_nat)/pond_com) AS effpondNAT_TOP_CVIDM_AIG,
((TOP_CVTRRYC_IND*pond_nat)/pond_com) AS effpondNAT_TOP_CVTRRYC_IND,
((TOP_CVVALVE_IND*pond_nat)/pond_com) AS effpondNAT_TOP_CVVALVE_IND,
((TOP_FANTIHTA_MED*pond_nat)/pond_com) AS effpondNAT_TOP_FANTIHTA_MED,
((TOP_FDIABET_IND*pond_nat)/pond_com) AS effpondNAT_TOP_FDIABET_IND,
((TOP_FHYPOLI_MED*pond_nat)/pond_com) AS effpondNAT_TOP_FHYPOLI_MED,
((TOP_HFOIPAN_IND*pond_nat)/pond_com) AS effpondNAT_TOP_HFOIPAN_IND,
((TOP_IRAUTRE_IND*pond_nat)/pond_com) AS effpondNAT_TOP_IRAUTRE_IND,
((TOP_IRCRRCH_IND*pond_nat)/pond_com) AS effpondNAT_TOP_IRCRRCH_IND,
((TOP_IRHEMOP_IND*pond_nat)/pond_com) AS effpondNAT_TOP_IRHEMOP_IND,
((TOP_IRMMHER_IND*pond_nat)/pond_com) AS effpondNAT_TOP_IRMMHER_IND,
((TOP_IRMUCO_IND*pond_nat)/pond_com) AS effpondNAT_TOP_IRMUCO_IND,
((TOP_IRPOLYA_IND*pond_nat)/pond_com) AS effpondNAT_TOP_IRPOLYA_IND,
((TOP_IRSPONA_IND*pond_nat)/pond_com) AS effpondNAT_TOP_IRSPONA_IND,
((TOP_IRVIH_IND*pond_nat)/pond_com) AS effpondNAT_TOP_IRVIH_IND,
((TOP_MATERNI_IND*pond_nat)/pond_com) AS effpondNAT_TOP_MATERNI_IND,
((TOP_NAUTRES_IND*pond_nat)/pond_com) AS effpondNAT_TOP_NAUTRES_IND,
((TOP_NDEMENC_IND*pond_nat)/pond_com) AS effpondNAT_TOP_NDEMENC_IND,
((TOP_NEPILEP_IND*pond_nat)/pond_com) AS effpondNAT_TOP_NEPILEP_IND,
((TOP_NMYOMYA_IND*pond_nat)/pond_com) AS effpondNAT_TOP_NMYOMYA_IND,
((TOP_NPARAPL_IND*pond_nat)/pond_com) AS effpondNAT_TOP_NPARAPL_IND,
((TOP_NPARKIN_IND*pond_nat)/pond_com) AS effpondNAT_TOP_NPARKIN_IND,
((TOP_NSEPLAQ_IND*pond_nat)/pond_com) AS effpondNAT_TOP_NSEPLAQ_IND,
((TOP_PADDICT_IND*pond_nat)/pond_com) AS effpondNAT_TOP_PADDICT_IND,
((TOP_PANTIDE_MED*pond_nat)/pond_com) AS effpondNAT_TOP_PANTIDE_MED,
((TOP_PANXIOL_MED*pond_nat)/pond_com) AS effpondNAT_TOP_PANXIOL_MED,
((TOP_PDEPNEV_IND*pond_nat)/pond_com) AS effpondNAT_TOP_PDEPNEV_IND,
((TOP_PHYPNOT_MED*pond_nat)/pond_com) AS effpondNAT_TOP_PHYPNOT_MED,
((TOP_PNEUROL_MED*pond_nat)/pond_com) AS effpondNAT_TOP_PNEUROL_MED,
((TOP_PRETARD_IND*pond_nat)/pond_com) AS effpondNAT_TOP_PRETARD_IND,
((TOP_PSYAUTR_IND*pond_nat)/pond_com) AS effpondNAT_TOP_PSYAUTR_IND,
((TOP_PSYCHOS_IND*pond_nat)/pond_com) AS effpondNAT_TOP_PSYCHOS_IND,
((TOP_PTRENFA_IND*pond_nat)/pond_com) AS effpondNAT_TOP_PTRENFA_IND,
((TOP_RDIALYSE_IND*pond_nat)/pond_com) AS effpondNAT_TOP_RDIALYSE_IND,
((TOP_RTRANS_AIG*pond_nat)/pond_com) AS effpondNAT_TOP_RTRANS_AIG,
((TOP_RTRANS_CHR*pond_nat)/pond_com) AS effpondNAT_TOP_RTRANS_CHR,
((TOP_ALCOOL*pond_nat)/pond_com) AS effpondNAT_TOP_ALCOOL,
((TOP_TABAC*pond_nat)/pond_com) AS effpondNAT_TOP_TABAC,
((TOP_canabis*pond_nat)/pond_com) AS effpondNAT_TOP_canabis,
((TOP_ADDICT_AUT*pond_nat)/pond_com) AS effpondNAT_TOP_ADDICT_AUT
from sasdata1.carto6ter6;
quit;


/* agregat sur les territoires  de notre choix avec la table commune */

/** correction des vieux codes communes FB pour EPCI  ;*/
data sasdata1.carto6ter7; set sasdata1.carto6ter7;
if code_insee="14178" then code_insee_cor="14474";
if code_insee="14513" then code_insee_cor="50592"; 
if code_insee="14697" then code_insee_cor="14654";
if code_insee="61031" then code_insee_cor="61486";
if code_insee="61109" then code_insee_cor="61292";
if code_insee="61177" then code_insee_cor="61486";
if code_insee="61223" then code_insee_cor="61486";
if code_insee="61249" then code_insee_cor="61375";
if code_insee="61377" then code_insee_cor="61486";
if code_insee="61410" then code_insee_cor="61487";
if code_insee="61417" then code_insee_cor="61375";	
if code_insee="61511" then code_insee_cor="61375";
if code_insee="61513" then code_insee_cor="61486";	
if code_insee not in ("14178","14513","14697","61031","61109","61177","61223","61249","61377","61410","61417",
"61511","61513") then code_insee_cor=code_insee;
run;
proc sql;
create table sasdata1.carto7 as select  * from sasdata1.carto6ter7 a left join sasdata1.communes2018 b
on a.code_insee_cor=b.codcom
;
quit;
proc sql;
select distinct code_insee from sasdata1.carto7 where substr(code_insee,1,2) in ('14','50','61','27','76') and 
EPCI2018=""; quit;

/* agregation par commune ; */

proc sql; 
create table sasdata1.com_final as 
select distinct 
code_insee_cor,
sum(TOP_ABPCOIR_IND)as TOP_ABPCOIR_IND,
sum(TOP_CANAUTR_ACT) as TOP_CANAUTR_ACT,
sum(TOP_CANAUTR_SUR)as TOP_CANAUTR_SUR,
sum(TOP_CANCOLO_ACT) as TOP_CANCOLO_ACT,
sum(TOP_CANCOLO_SUR) as TOP_CANCOLO_SUR,
sum(TOP_CANPOUM_ACT) as TOP_CANPOUM_ACT,
sum(TOP_CANPOUM_SUR) as TOP_CANPOUM_SUR,
sum(TOP_CANPROS_ACT) as TOP_CANPROS_ACT,
sum(TOP_CANPROS_SUR) as TOP_CANPROS_SUR,
sum(TOP_CANSEIF_ACT) as TOP_CANSEIF_ACT,
sum(TOP_CANSEIF_SUR) as TOP_CANSEIF_SUR,
sum(TOP_CVAOMI_IND) as TOP_CVAOMI_IND,
sum(TOP_CVAUTRE_IND) as TOP_CVAUTRE_IND,
sum(TOP_CVAVC_AIG) as TOP_CVAVC_AIG ,
sum(TOP_CVAVC_SEQ) as TOP_CVAVC_SEQ,
sum(TOP_CVCORON_CHR) as TOP_CVCORON_CHR,
sum(TOP_CVEMBOL_AIG) as TOP_CVEMBOL_AIG,
sum(TOP_CVIC_AIG) as TOP_CVIC_AIG,
sum(TOP_CVIC_CHR) as TOP_CVIC_CHR,
sum(TOP_CVIDM_AIG) as TOP_CVIDM_AIG,
sum(TOP_CVTRRYC_IND) as TOP_CVTRRYC_IND,
sum(TOP_CVVALVE_IND) as TOP_CVVALVE_IND,
sum(TOP_FANTIHTA_MED) as TOP_FANTIHTA_MED,
sum(TOP_FDIABET_IND) as TOP_FDIABET_IND,
sum(TOP_FHYPOLI_MED) as TOP_FHYPOLI_MED,
sum(TOP_HFOIPAN_IND) as TOP_HFOIPAN_IND,
sum(TOP_IRAUTRE_IND) as TOP_IRAUTRE_IND,
sum(TOP_IRCRRCH_IND) as TOP_IRCRRCH_IND,
sum(TOP_IRHEMOP_IND) as TOP_IRHEMOP_IND,
sum(TOP_IRMMHER_IND) as TOP_IRMMHER_IND,
sum(TOP_IRMUCO_IND) as TOP_IRMUCO_IND,
sum(TOP_IRPOLYA_IND) as TOP_IRPOLYA_IND,
sum(TOP_IRSPONA_IND) as TOP_IRSPONA_IND,
sum(TOP_IRVIH_IND)as TOP_IRVIH_IND,
sum(TOP_MATERNI_IND) as TOP_MATERNI_IND,
sum(TOP_NAUTRES_IND) as TOP_NAUTRES_IND,
sum(TOP_NDEMENC_IND) as TOP_NDEMENC_IND ,
sum(TOP_NEPILEP_IND)as TOP_NEPILEP_IND,
sum(TOP_NMYOMYA_IND) as TOP_NMYOMYA_IND,
sum(TOP_NPARAPL_IND) as TOP_NPARAPL_IND,
sum(TOP_NPARKIN_IND) as TOP_NPARKIN_IND,
sum(TOP_NSEPLAQ_IND) as TOP_NSEPLAQ_IND,
sum(TOP_PADDICT_IND) as TOP_PADDICT_IND,
sum(TOP_PANTIDE_MED) as TOP_PANTIDE_MED,
sum(TOP_PANXIOL_MED) as TOP_PANXIOL_MED,
sum(TOP_PDEPNEV_IND) as TOP_PDEPNEV_IND,
sum(TOP_PHYPNOT_MED)as TOP_PHYPNOT_MED,
sum(TOP_PNEUROL_MED)as TOP_PNEUROL_MED,
sum(TOP_PRETARD_IND) as TOP_PRETARD_IND,
sum(TOP_PSYAUTR_IND) as TOP_PSYAUTR_IND,
sum(TOP_PSYCHOS_IND)as TOP_PSYCHOS_IND,
sum(TOP_PTRENFA_IND) as TOP_PTRENFA_IND,
sum(TOP_RDIALYSE_IND) as TOP_RDIALYSE_IND,
sum(TOP_RTRANS_AIG) as TOP_RTRANS_AIG,
sum(TOP_RTRANS_CHR)as TOP_RTRANS_CHR,
sum(TOP_ALCOOL) as TOP_ALCOOL,
sum(TOP_TABAC) as TOP_TABAC,
sum(TOP_canabis) as TOP_canabis,
sum(TOP_ADDICT_AUT) as TOP_ADDICT_AUT,
SUM(NB_TOT_REF) as NB_TOT_REF,
SUM(effpondREG_TOP_ABPCOIR_IND) AS effpondREG_TOP_ABPCOIR_IND,
SUM(effpondREG_TOP_CANAUTR_ACT) AS effpondREG_TOP_CANAUTR_ACT,
SUM(effpondREG_TOP_CANAUTR_SUR) AS effpondREG_TOP_CANAUTR_SUR,
SUM(effpondREG_TOP_CANCOLO_ACT) AS effpondREG_TOP_CANCOLO_ACT,
SUM(effpondREG_TOP_CANCOLO_SUR) AS effpondREG_TOP_CANCOLO_SUR,
SUM(effpondREG_TOP_CANPOUM_ACT) AS effpondREG_TOP_CANPOUM_ACT,
SUM(effpondREG_TOP_CANPOUM_SUR) AS effpondREG_TOP_CANPOUM_SUR,
SUM(effpondREG_TOP_CANPROS_ACT) AS effpondREG_TOP_CANPROS_ACT,
SUM(effpondREG_TOP_CANPROS_SUR) AS effpondREG_TOP_CANPROS_SUR,
SUM(effpondREG_TOP_CANSEIF_ACT) AS effpondREG_TOP_CANSEIF_ACT,
SUM(effpondREG_TOP_CANSEIF_SUR) AS effpondREG_TOP_CANSEIF_SUR,
SUM(effpondREG_TOP_CVAOMI_IND) AS effpondREG_TOP_CVAOMI_IND,
SUM(effpondREG_TOP_CVAUTRE_IND) AS effpondREG_TOP_CVAUTRE_IND,
SUM(effpondREG_TOP_CVAVC_AIG) AS effpondREG_TOP_CVAVC_AIG,
SUM(effpondREG_TOP_CVAVC_SEQ) AS effpondREG_TOP_CVAVC_SEQ,
SUM(effpondREG_TOP_CVCORON_CHR) AS effpondREG_TOP_CVCORON_CHR,
SUM(effpondREG_TOP_CVEMBOL_AIG) AS effpondREG_TOP_CVEMBOL_AIG,
SUM(effpondREG_TOP_CVIC_AIG) AS effpondREG_TOP_CVIC_AIG,
SUM(effpondREG_TOP_CVIC_CHR) AS effpondREG_TOP_CVIC_CHR,
SUM(effpondREG_TOP_CVIDM_AIG) AS effpondREG_TOP_CVIDM_AIG,
SUM(effpondREG_TOP_CVTRRYC_IND) AS effpondREG_TOP_CVTRRYC_IND,
SUM(effpondREG_TOP_CVVALVE_IND) AS effpondREG_TOP_CVVALVE_IND,
SUM(effpondREG_TOP_FANTIHTA_MED) AS effpondREG_TOP_FANTIHTA_MED,
SUM(effpondREG_TOP_FDIABET_IND) AS effpondREG_TOP_FDIABET_IND,
SUM(effpondREG_TOP_FHYPOLI_MED) AS effpondREG_TOP_FHYPOLI_MED,
SUM(effpondREG_TOP_HFOIPAN_IND) AS effpondREG_TOP_HFOIPAN_IND,
SUM(effpondREG_TOP_IRAUTRE_IND) AS effpondREG_TOP_IRAUTRE_IND,
SUM(effpondREG_TOP_IRCRRCH_IND) AS effpondREG_TOP_IRCRRCH_IND,
SUM(effpondREG_TOP_IRHEMOP_IND) AS effpondREG_TOP_IRHEMOP_IND,
SUM(effpondREG_TOP_IRMMHER_IND) AS effpondREG_TOP_IRMMHER_IND,
SUM(effpondREG_TOP_IRMUCO_IND) AS effpondREG_TOP_IRMUCO_IND,
SUM(effpondREG_TOP_IRPOLYA_IND) AS effpondREG_TOP_IRPOLYA_IND,
SUM(effpondREG_TOP_IRSPONA_IND) AS effpondREG_TOP_IRSPONA_IND,
SUM(effpondREG_TOP_IRVIH_IND) AS effpondREG_TOP_IRVIH_IND,
SUM(effpondREG_TOP_MATERNI_IND) AS effpondREG_TOP_MATERNI_IND,
SUM(effpondREG_TOP_NAUTRES_IND) AS effpondREG_TOP_NAUTRES_IND,
SUM(effpondREG_TOP_NDEMENC_IND) AS effpondREG_TOP_NDEMENC_IND,
SUM(effpondREG_TOP_NEPILEP_IND) AS effpondREG_TOP_NEPILEP_IND,
SUM(effpondREG_TOP_NMYOMYA_IND) AS effpondREG_TOP_NMYOMYA_IND,
SUM(effpondREG_TOP_NPARAPL_IND) AS effpondREG_TOP_NPARAPL_IND,
SUM(effpondREG_TOP_NPARKIN_IND) AS effpondREG_TOP_NPARKIN_IND,
SUM(effpondREG_TOP_NSEPLAQ_IND) AS effpondREG_TOP_NSEPLAQ_IND,
SUM(effpondREG_TOP_PADDICT_IND) AS effpondREG_TOP_PADDICT_IND,
SUM(effpondREG_TOP_PANTIDE_MED) AS effpondREG_TOP_PANTIDE_MED,
SUM(effpondREG_TOP_PANXIOL_MED) AS effpondREG_TOP_PANXIOL_MED,
SUM(effpondREG_TOP_PDEPNEV_IND) AS effpondREG_TOP_PDEPNEV_IND,
SUM(effpondREG_TOP_PHYPNOT_MED) AS effpondREG_TOP_PHYPNOT_MED,
SUM(effpondREG_TOP_PNEUROL_MED) AS effpondREG_TOP_PNEUROL_MED,
SUM(effpondREG_TOP_PRETARD_IND) AS effpondREG_TOP_PRETARD_IND,
SUM(effpondREG_TOP_PSYAUTR_IND) AS effpondREG_TOP_PSYAUTR_IND,
SUM(effpondREG_TOP_PSYCHOS_IND) AS effpondREG_TOP_PSYCHOS_IND,
SUM(effpondREG_TOP_PTRENFA_IND) AS effpondREG_TOP_PTRENFA_IND,
SUM(effpondREG_TOP_RDIALYSE_IND) AS effpondREG_TOP_RDIALYSE_IND,
SUM(effpondREG_TOP_RTRANS_AIG) AS effpondREG_TOP_RTRANS_AIG,
SUM(effpondREG_TOP_RTRANS_CHR) AS effpondREG_TOP_RTRANS_CHR,
SUM(effpondREG_TOP_ALCOOL) AS effpondREG_TOP_ALCOOL,
SUM(effpondREG_TOP_TABAC) AS effpondREG_TOP_TABAC,
SUM(effpondREG_TOP_canabis) AS effpondREG_TOP_canabis,
SUM(effpondREG_TOP_ADDICT_AUT) AS effpondREG_TOP_ADDICT_AUT,
SUM(effpondNAT_TOP_ABPCOIR_IND) AS effpondNAT_TOP_ABPCOIR_IND,
SUM(effpondNAT_TOP_CANAUTR_ACT) AS effpondNAT_TOP_CANAUTR_ACT,
SUM(effpondNAT_TOP_CANAUTR_SUR) AS effpondNAT_TOP_CANAUTR_SUR,
SUM(effpondNAT_TOP_CANCOLO_ACT) AS effpondNAT_TOP_CANCOLO_ACT,
SUM(effpondNAT_TOP_CANCOLO_SUR) AS effpondNAT_TOP_CANCOLO_SUR,
SUM(effpondNAT_TOP_CANPOUM_ACT) AS effpondNAT_TOP_CANPOUM_ACT,
SUM(effpondNAT_TOP_CANPOUM_SUR) AS effpondNAT_TOP_CANPOUM_SUR,
SUM(effpondNAT_TOP_CANPROS_ACT) AS effpondNAT_TOP_CANPROS_ACT,
SUM(effpondNAT_TOP_CANPROS_SUR) AS effpondNAT_TOP_CANPROS_SUR,
SUM(effpondNAT_TOP_CANSEIF_ACT) AS effpondNAT_TOP_CANSEIF_ACT,
SUM(effpondNAT_TOP_CANSEIF_SUR) AS effpondNAT_TOP_CANSEIF_SUR,
SUM(effpondNAT_TOP_CVAOMI_IND) AS effpondNAT_TOP_CVAOMI_IND,
SUM(effpondNAT_TOP_CVAUTRE_IND) AS effpondNAT_TOP_CVAUTRE_IND,
SUM(effpondNAT_TOP_CVAVC_AIG) AS effpondNAT_TOP_CVAVC_AIG,
SUM(effpondNAT_TOP_CVAVC_SEQ) AS effpondNAT_TOP_CVAVC_SEQ,
SUM(effpondNAT_TOP_CVCORON_CHR) AS effpondNAT_TOP_CVCORON_CHR,
SUM(effpondNAT_TOP_CVEMBOL_AIG) AS effpondNAT_TOP_CVEMBOL_AIG,
SUM(effpondNAT_TOP_CVIC_AIG) AS effpondNAT_TOP_CVIC_AIG,
SUM(effpondNAT_TOP_CVIC_CHR) AS effpondNAT_TOP_CVIC_CHR,
SUM(effpondNAT_TOP_CVIDM_AIG) AS effpondNAT_TOP_CVIDM_AIG,
SUM(effpondNAT_TOP_CVTRRYC_IND) AS effpondNAT_TOP_CVTRRYC_IND,
SUM(effpondNAT_TOP_CVVALVE_IND) AS effpondNAT_TOP_CVVALVE_IND,
SUM(effpondNAT_TOP_FANTIHTA_MED) AS effpondNAT_TOP_FANTIHTA_MED,
SUM(effpondNAT_TOP_FDIABET_IND) AS effpondNAT_TOP_FDIABET_IND,
SUM(effpondNAT_TOP_FHYPOLI_MED) AS effpondNAT_TOP_FHYPOLI_MED,
SUM(effpondNAT_TOP_HFOIPAN_IND) AS effpondNAT_TOP_HFOIPAN_IND,
SUM(effpondNAT_TOP_IRAUTRE_IND) AS effpondNAT_TOP_IRAUTRE_IND,
SUM(effpondNAT_TOP_IRCRRCH_IND) AS effpondNAT_TOP_IRCRRCH_IND,
SUM(effpondNAT_TOP_IRHEMOP_IND) AS effpondNAT_TOP_IRHEMOP_IND,
SUM(effpondNAT_TOP_IRMMHER_IND) AS effpondNAT_TOP_IRMMHER_IND,
SUM(effpondNAT_TOP_IRMUCO_IND) AS effpondNAT_TOP_IRMUCO_IND,
SUM(effpondNAT_TOP_IRPOLYA_IND) AS effpondNAT_TOP_IRPOLYA_IND,
SUM(effpondNAT_TOP_IRSPONA_IND) AS effpondNAT_TOP_IRSPONA_IND,
SUM(effpondNAT_TOP_IRVIH_IND) AS effpondNAT_TOP_IRVIH_IND,
SUM(effpondNAT_TOP_MATERNI_IND) AS effpondNAT_TOP_MATERNI_IND,
SUM(effpondNAT_TOP_NAUTRES_IND) AS effpondNAT_TOP_NAUTRES_IND,
SUM(effpondNAT_TOP_NDEMENC_IND) AS effpondNAT_TOP_NDEMENC_IND,
SUM(effpondNAT_TOP_NEPILEP_IND) AS effpondNAT_TOP_NEPILEP_IND,
SUM(effpondNAT_TOP_NMYOMYA_IND) AS effpondNAT_TOP_NMYOMYA_IND,
SUM(effpondNAT_TOP_NPARAPL_IND) AS effpondNAT_TOP_NPARAPL_IND,
SUM(effpondNAT_TOP_NPARKIN_IND) AS effpondNAT_TOP_NPARKIN_IND,
SUM(effpondNAT_TOP_NSEPLAQ_IND) AS effpondNAT_TOP_NSEPLAQ_IND,
SUM(effpondNAT_TOP_PADDICT_IND) AS effpondNAT_TOP_PADDICT_IND,
SUM(effpondNAT_TOP_PANTIDE_MED) AS effpondNAT_TOP_PANTIDE_MED,
SUM(effpondNAT_TOP_PANXIOL_MED) AS effpondNAT_TOP_PANXIOL_MED,
SUM(effpondNAT_TOP_PDEPNEV_IND) AS effpondNAT_TOP_PDEPNEV_IND,
SUM(effpondNAT_TOP_PHYPNOT_MED) AS effpondNAT_TOP_PHYPNOT_MED,
SUM(effpondNAT_TOP_PNEUROL_MED) AS effpondNAT_TOP_PNEUROL_MED,
SUM(effpondNAT_TOP_PRETARD_IND) AS effpondNAT_TOP_PRETARD_IND,
SUM(effpondNAT_TOP_PSYAUTR_IND) AS effpondNAT_TOP_PSYAUTR_IND,
SUM(effpondNAT_TOP_PSYCHOS_IND) AS effpondNAT_TOP_PSYCHOS_IND,
SUM(effpondNAT_TOP_PTRENFA_IND) AS effpondNAT_TOP_PTRENFA_IND,
SUM(effpondNAT_TOP_RDIALYSE_IND) AS effpondNAT_TOP_RDIALYSE_IND,
SUM(effpondNAT_TOP_RTRANS_AIG) AS effpondNAT_TOP_RTRANS_AIG,
SUM(effpondNAT_TOP_RTRANS_CHR) AS effpondNAT_TOP_RTRANS_CHR,
SUM(effpondNAT_TOP_ALCOOL) AS effpondNAT_TOP_ALCOOL,
SUM(effpondNAT_TOP_TABAC) AS effpondNAT_TOP_TABAC,
SUM(effpondNAT_TOP_canabis) AS effpondNAT_TOP_canabis,
SUM(effpondNAT_TOP_ADDICT_AUT) AS effpondNAT_TOP_ADDICT_AUT
from sasdata1.carto7 
group by 1; 
quit; 

proc sql; 
create table sasdata1.reg as 
select
"region" as code_insee_cor,
sum(TOP_ABPCOIR_IND)as TOP_ABPCOIR_IND,
sum(TOP_CANAUTR_ACT) as TOP_CANAUTR_ACT,
sum(TOP_CANAUTR_SUR)as TOP_CANAUTR_SUR,
sum(TOP_CANCOLO_ACT) as TOP_CANCOLO_ACT,
sum(TOP_CANCOLO_SUR) as TOP_CANCOLO_SUR,
sum(TOP_CANPOUM_ACT) as TOP_CANPOUM_ACT,
sum(TOP_CANPOUM_SUR) as TOP_CANPOUM_SUR,
sum(TOP_CANPROS_ACT) as TOP_CANPROS_ACT,
sum(TOP_CANPROS_SUR) as TOP_CANPROS_SUR,
sum(TOP_CANSEIF_ACT) as TOP_CANSEIF_ACT,
sum(TOP_CANSEIF_SUR) as TOP_CANSEIF_SUR,
sum(TOP_CVAOMI_IND) as TOP_CVAOMI_IND,
sum(TOP_CVAUTRE_IND) as TOP_CVAUTRE_IND,
sum(TOP_CVAVC_AIG) as TOP_CVAVC_AIG ,
sum(TOP_CVAVC_SEQ) as TOP_CVAVC_SEQ,
sum(TOP_CVCORON_CHR) as TOP_CVCORON_CHR,
sum(TOP_CVEMBOL_AIG) as TOP_CVEMBOL_AIG,
sum(TOP_CVIC_AIG) as TOP_CVIC_AIG,
sum(TOP_CVIC_CHR) as TOP_CVIC_CHR,
sum(TOP_CVIDM_AIG) as TOP_CVIDM_AIG,
sum(TOP_CVTRRYC_IND) as TOP_CVTRRYC_IND,
sum(TOP_CVVALVE_IND) as TOP_CVVALVE_IND,
sum(TOP_FANTIHTA_MED) as TOP_FANTIHTA_MED,
sum(TOP_FDIABET_IND) as TOP_FDIABET_IND,
sum(TOP_FHYPOLI_MED) as TOP_FHYPOLI_MED,
sum(TOP_HFOIPAN_IND) as TOP_HFOIPAN_IND,
sum(TOP_IRAUTRE_IND) as TOP_IRAUTRE_IND,
sum(TOP_IRCRRCH_IND) as TOP_IRCRRCH_IND,
sum(TOP_IRHEMOP_IND) as TOP_IRHEMOP_IND,
sum(TOP_IRMMHER_IND) as TOP_IRMMHER_IND,
sum(TOP_IRMUCO_IND) as TOP_IRMUCO_IND,
sum(TOP_IRPOLYA_IND) as TOP_IRPOLYA_IND,
sum(TOP_IRSPONA_IND) as TOP_IRSPONA_IND,
sum(TOP_IRVIH_IND)as TOP_IRVIH_IND,
sum(TOP_MATERNI_IND) as TOP_MATERNI_IND,
sum(TOP_NAUTRES_IND) as TOP_NAUTRES_IND,
sum(TOP_NDEMENC_IND) as TOP_NDEMENC_IND ,
sum(TOP_NEPILEP_IND)as TOP_NEPILEP_IND,
sum(TOP_NMYOMYA_IND) as TOP_NMYOMYA_IND,
sum(TOP_NPARAPL_IND) as TOP_NPARAPL_IND,
sum(TOP_NPARKIN_IND) as TOP_NPARKIN_IND,
sum(TOP_NSEPLAQ_IND) as TOP_NSEPLAQ_IND,
sum(TOP_PADDICT_IND) as TOP_PADDICT_IND,
sum(TOP_PANTIDE_MED) as TOP_PANTIDE_MED,
sum(TOP_PANXIOL_MED) as TOP_PANXIOL_MED,
sum(TOP_PDEPNEV_IND) as TOP_PDEPNEV_IND,
sum(TOP_PHYPNOT_MED)as TOP_PHYPNOT_MED,
sum(TOP_PNEUROL_MED)as TOP_PNEUROL_MED,
sum(TOP_PRETARD_IND) as TOP_PRETARD_IND,
sum(TOP_PSYAUTR_IND) as TOP_PSYAUTR_IND,
sum(TOP_PSYCHOS_IND)as TOP_PSYCHOS_IND,
sum(TOP_PTRENFA_IND) as TOP_PTRENFA_IND,
sum(TOP_RDIALYSE_IND) as TOP_RDIALYSE_IND,
sum(TOP_RTRANS_AIG) as TOP_RTRANS_AIG,
sum(TOP_RTRANS_CHR)as TOP_RTRANS_CHR,
sum(TOP_ALCOOL) as TOP_ALCOOL,
sum(TOP_TABAC) as TOP_TABAC,
sum(TOP_canabis) as TOP_canabis,
sum(TOP_ADDICT_AUT) as TOP_ADDICT_AUT,
SUM(NB_TOT_REF) as NB_TOT_REF,
SUM(effpondREG_TOP_ABPCOIR_IND) AS effpondREG_TOP_ABPCOIR_IND,
SUM(effpondREG_TOP_CANAUTR_ACT) AS effpondREG_TOP_CANAUTR_ACT,
SUM(effpondREG_TOP_CANAUTR_SUR) AS effpondREG_TOP_CANAUTR_SUR,
SUM(effpondREG_TOP_CANCOLO_ACT) AS effpondREG_TOP_CANCOLO_ACT,
SUM(effpondREG_TOP_CANCOLO_SUR) AS effpondREG_TOP_CANCOLO_SUR,
SUM(effpondREG_TOP_CANPOUM_ACT) AS effpondREG_TOP_CANPOUM_ACT,
SUM(effpondREG_TOP_CANPOUM_SUR) AS effpondREG_TOP_CANPOUM_SUR,
SUM(effpondREG_TOP_CANPROS_ACT) AS effpondREG_TOP_CANPROS_ACT,
SUM(effpondREG_TOP_CANPROS_SUR) AS effpondREG_TOP_CANPROS_SUR,
SUM(effpondREG_TOP_CANSEIF_ACT) AS effpondREG_TOP_CANSEIF_ACT,
SUM(effpondREG_TOP_CANSEIF_SUR) AS effpondREG_TOP_CANSEIF_SUR,
SUM(effpondREG_TOP_CVAOMI_IND) AS effpondREG_TOP_CVAOMI_IND,
SUM(effpondREG_TOP_CVAUTRE_IND) AS effpondREG_TOP_CVAUTRE_IND,
SUM(effpondREG_TOP_CVAVC_AIG) AS effpondREG_TOP_CVAVC_AIG,
SUM(effpondREG_TOP_CVAVC_SEQ) AS effpondREG_TOP_CVAVC_SEQ,
SUM(effpondREG_TOP_CVCORON_CHR) AS effpondREG_TOP_CVCORON_CHR,
SUM(effpondREG_TOP_CVEMBOL_AIG) AS effpondREG_TOP_CVEMBOL_AIG,
SUM(effpondREG_TOP_CVIC_AIG) AS effpondREG_TOP_CVIC_AIG,
SUM(effpondREG_TOP_CVIC_CHR) AS effpondREG_TOP_CVIC_CHR,
SUM(effpondREG_TOP_CVIDM_AIG) AS effpondREG_TOP_CVIDM_AIG,
SUM(effpondREG_TOP_CVTRRYC_IND) AS effpondREG_TOP_CVTRRYC_IND,
SUM(effpondREG_TOP_CVVALVE_IND) AS effpondREG_TOP_CVVALVE_IND,
SUM(effpondREG_TOP_FANTIHTA_MED) AS effpondREG_TOP_FANTIHTA_MED,
SUM(effpondREG_TOP_FDIABET_IND) AS effpondREG_TOP_FDIABET_IND,
SUM(effpondREG_TOP_FHYPOLI_MED) AS effpondREG_TOP_FHYPOLI_MED,
SUM(effpondREG_TOP_HFOIPAN_IND) AS effpondREG_TOP_HFOIPAN_IND,
SUM(effpondREG_TOP_IRAUTRE_IND) AS effpondREG_TOP_IRAUTRE_IND,
SUM(effpondREG_TOP_IRCRRCH_IND) AS effpondREG_TOP_IRCRRCH_IND,
SUM(effpondREG_TOP_IRHEMOP_IND) AS effpondREG_TOP_IRHEMOP_IND,
SUM(effpondREG_TOP_IRMMHER_IND) AS effpondREG_TOP_IRMMHER_IND,
SUM(effpondREG_TOP_IRMUCO_IND) AS effpondREG_TOP_IRMUCO_IND,
SUM(effpondREG_TOP_IRPOLYA_IND) AS effpondREG_TOP_IRPOLYA_IND,
SUM(effpondREG_TOP_IRSPONA_IND) AS effpondREG_TOP_IRSPONA_IND,
SUM(effpondREG_TOP_IRVIH_IND) AS effpondREG_TOP_IRVIH_IND,
SUM(effpondREG_TOP_MATERNI_IND) AS effpondREG_TOP_MATERNI_IND,
SUM(effpondREG_TOP_NAUTRES_IND) AS effpondREG_TOP_NAUTRES_IND,
SUM(effpondREG_TOP_NDEMENC_IND) AS effpondREG_TOP_NDEMENC_IND,
SUM(effpondREG_TOP_NEPILEP_IND) AS effpondREG_TOP_NEPILEP_IND,
SUM(effpondREG_TOP_NMYOMYA_IND) AS effpondREG_TOP_NMYOMYA_IND,
SUM(effpondREG_TOP_NPARAPL_IND) AS effpondREG_TOP_NPARAPL_IND,
SUM(effpondREG_TOP_NPARKIN_IND) AS effpondREG_TOP_NPARKIN_IND,
SUM(effpondREG_TOP_NSEPLAQ_IND) AS effpondREG_TOP_NSEPLAQ_IND,
SUM(effpondREG_TOP_PADDICT_IND) AS effpondREG_TOP_PADDICT_IND,
SUM(effpondREG_TOP_PANTIDE_MED) AS effpondREG_TOP_PANTIDE_MED,
SUM(effpondREG_TOP_PANXIOL_MED) AS effpondREG_TOP_PANXIOL_MED,
SUM(effpondREG_TOP_PDEPNEV_IND) AS effpondREG_TOP_PDEPNEV_IND,
SUM(effpondREG_TOP_PHYPNOT_MED) AS effpondREG_TOP_PHYPNOT_MED,
SUM(effpondREG_TOP_PNEUROL_MED) AS effpondREG_TOP_PNEUROL_MED,
SUM(effpondREG_TOP_PRETARD_IND) AS effpondREG_TOP_PRETARD_IND,
SUM(effpondREG_TOP_PSYAUTR_IND) AS effpondREG_TOP_PSYAUTR_IND,
SUM(effpondREG_TOP_PSYCHOS_IND) AS effpondREG_TOP_PSYCHOS_IND,
SUM(effpondREG_TOP_PTRENFA_IND) AS effpondREG_TOP_PTRENFA_IND,
SUM(effpondREG_TOP_RDIALYSE_IND) AS effpondREG_TOP_RDIALYSE_IND,
SUM(effpondREG_TOP_RTRANS_AIG) AS effpondREG_TOP_RTRANS_AIG,
SUM(effpondREG_TOP_RTRANS_CHR) AS effpondREG_TOP_RTRANS_CHR,
SUM(effpondREG_TOP_ALCOOL) AS effpondREG_TOP_ALCOOL,
SUM(effpondREG_TOP_TABAC) AS effpondREG_TOP_TABAC,
SUM(effpondREG_TOP_canabis) AS effpondREG_TOP_canabis,
SUM(effpondREG_TOP_ADDICT_AUT) AS effpondREG_TOP_ADDICT_AUT,
SUM(effpondNAT_TOP_ABPCOIR_IND) AS effpondNAT_TOP_ABPCOIR_IND,
SUM(effpondNAT_TOP_CANAUTR_ACT) AS effpondNAT_TOP_CANAUTR_ACT,
SUM(effpondNAT_TOP_CANAUTR_SUR) AS effpondNAT_TOP_CANAUTR_SUR,
SUM(effpondNAT_TOP_CANCOLO_ACT) AS effpondNAT_TOP_CANCOLO_ACT,
SUM(effpondNAT_TOP_CANCOLO_SUR) AS effpondNAT_TOP_CANCOLO_SUR,
SUM(effpondNAT_TOP_CANPOUM_ACT) AS effpondNAT_TOP_CANPOUM_ACT,
SUM(effpondNAT_TOP_CANPOUM_SUR) AS effpondNAT_TOP_CANPOUM_SUR,
SUM(effpondNAT_TOP_CANPROS_ACT) AS effpondNAT_TOP_CANPROS_ACT,
SUM(effpondNAT_TOP_CANPROS_SUR) AS effpondNAT_TOP_CANPROS_SUR,
SUM(effpondNAT_TOP_CANSEIF_ACT) AS effpondNAT_TOP_CANSEIF_ACT,
SUM(effpondNAT_TOP_CANSEIF_SUR) AS effpondNAT_TOP_CANSEIF_SUR,
SUM(effpondNAT_TOP_CVAOMI_IND) AS effpondNAT_TOP_CVAOMI_IND,
SUM(effpondNAT_TOP_CVAUTRE_IND) AS effpondNAT_TOP_CVAUTRE_IND,
SUM(effpondNAT_TOP_CVAVC_AIG) AS effpondNAT_TOP_CVAVC_AIG,
SUM(effpondNAT_TOP_CVAVC_SEQ) AS effpondNAT_TOP_CVAVC_SEQ,
SUM(effpondNAT_TOP_CVCORON_CHR) AS effpondNAT_TOP_CVCORON_CHR,
SUM(effpondNAT_TOP_CVEMBOL_AIG) AS effpondNAT_TOP_CVEMBOL_AIG,
SUM(effpondNAT_TOP_CVIC_AIG) AS effpondNAT_TOP_CVIC_AIG,
SUM(effpondNAT_TOP_CVIC_CHR) AS effpondNAT_TOP_CVIC_CHR,
SUM(effpondNAT_TOP_CVIDM_AIG) AS effpondNAT_TOP_CVIDM_AIG,
SUM(effpondNAT_TOP_CVTRRYC_IND) AS effpondNAT_TOP_CVTRRYC_IND,
SUM(effpondNAT_TOP_CVVALVE_IND) AS effpondNAT_TOP_CVVALVE_IND,
SUM(effpondNAT_TOP_FANTIHTA_MED) AS effpondNAT_TOP_FANTIHTA_MED,
SUM(effpondNAT_TOP_FDIABET_IND) AS effpondNAT_TOP_FDIABET_IND,
SUM(effpondNAT_TOP_FHYPOLI_MED) AS effpondNAT_TOP_FHYPOLI_MED,
SUM(effpondNAT_TOP_HFOIPAN_IND) AS effpondNAT_TOP_HFOIPAN_IND,
SUM(effpondNAT_TOP_IRAUTRE_IND) AS effpondNAT_TOP_IRAUTRE_IND,
SUM(effpondNAT_TOP_IRCRRCH_IND) AS effpondNAT_TOP_IRCRRCH_IND,
SUM(effpondNAT_TOP_IRHEMOP_IND) AS effpondNAT_TOP_IRHEMOP_IND,
SUM(effpondNAT_TOP_IRMMHER_IND) AS effpondNAT_TOP_IRMMHER_IND,
SUM(effpondNAT_TOP_IRMUCO_IND) AS effpondNAT_TOP_IRMUCO_IND,
SUM(effpondNAT_TOP_IRPOLYA_IND) AS effpondNAT_TOP_IRPOLYA_IND,
SUM(effpondNAT_TOP_IRSPONA_IND) AS effpondNAT_TOP_IRSPONA_IND,
SUM(effpondNAT_TOP_IRVIH_IND) AS effpondNAT_TOP_IRVIH_IND,
SUM(effpondNAT_TOP_MATERNI_IND) AS effpondNAT_TOP_MATERNI_IND,
SUM(effpondNAT_TOP_NAUTRES_IND) AS effpondNAT_TOP_NAUTRES_IND,
SUM(effpondNAT_TOP_NDEMENC_IND) AS effpondNAT_TOP_NDEMENC_IND,
SUM(effpondNAT_TOP_NEPILEP_IND) AS effpondNAT_TOP_NEPILEP_IND,
SUM(effpondNAT_TOP_NMYOMYA_IND) AS effpondNAT_TOP_NMYOMYA_IND,
SUM(effpondNAT_TOP_NPARAPL_IND) AS effpondNAT_TOP_NPARAPL_IND,
SUM(effpondNAT_TOP_NPARKIN_IND) AS effpondNAT_TOP_NPARKIN_IND,
SUM(effpondNAT_TOP_NSEPLAQ_IND) AS effpondNAT_TOP_NSEPLAQ_IND,
SUM(effpondNAT_TOP_PADDICT_IND) AS effpondNAT_TOP_PADDICT_IND,
SUM(effpondNAT_TOP_PANTIDE_MED) AS effpondNAT_TOP_PANTIDE_MED,
SUM(effpondNAT_TOP_PANXIOL_MED) AS effpondNAT_TOP_PANXIOL_MED,
SUM(effpondNAT_TOP_PDEPNEV_IND) AS effpondNAT_TOP_PDEPNEV_IND,
SUM(effpondNAT_TOP_PHYPNOT_MED) AS effpondNAT_TOP_PHYPNOT_MED,
SUM(effpondNAT_TOP_PNEUROL_MED) AS effpondNAT_TOP_PNEUROL_MED,
SUM(effpondNAT_TOP_PRETARD_IND) AS effpondNAT_TOP_PRETARD_IND,
SUM(effpondNAT_TOP_PSYAUTR_IND) AS effpondNAT_TOP_PSYAUTR_IND,
SUM(effpondNAT_TOP_PSYCHOS_IND) AS effpondNAT_TOP_PSYCHOS_IND,
SUM(effpondNAT_TOP_PTRENFA_IND) AS effpondNAT_TOP_PTRENFA_IND,
SUM(effpondNAT_TOP_RDIALYSE_IND) AS effpondNAT_TOP_RDIALYSE_IND,
SUM(effpondNAT_TOP_RTRANS_AIG) AS effpondNAT_TOP_RTRANS_AIG,
SUM(effpondNAT_TOP_RTRANS_CHR) AS effpondNAT_TOP_RTRANS_CHR,
SUM(effpondNAT_TOP_ALCOOL) AS effpondNAT_TOP_ALCOOL,
SUM(effpondNAT_TOP_TABAC) AS effpondNAT_TOP_TABAC,
SUM(effpondNAT_TOP_canabis) AS effpondNAT_TOP_canabis,
SUM(effpondNAT_TOP_ADDICT_AUT) AS effpondNAT_TOP_ADDICT_AUT
from sasdata1.carto7 
group by 1; 
quit;
proc sql; 
%connectora;
create table sasdata1.nat as (select * from connection to oracle(
select distinct 
sum(TOP_ABPCOIR_IND)as TOP_ABPCOIR_IND,
sum(TOP_CANAUTR_ACT) as TOP_CANAUTR_ACT,
sum(TOP_CANAUTR_SUR)as TOP_CANAUTR_SUR,
sum(TOP_CANCOLO_ACT) as TOP_CANCOLO_ACT,
sum(TOP_CANCOLO_SUR) as TOP_CANCOLO_SUR,
sum(TOP_CANPOUM_ACT) as TOP_CANPOUM_ACT,
sum(TOP_CANPOUM_SUR) as TOP_CANPOUM_SUR,
sum(TOP_CANPROS_ACT) as TOP_CANPROS_ACT,
sum(TOP_CANPROS_SUR) as TOP_CANPROS_SUR,
sum(TOP_CANSEIF_ACT) as TOP_CANSEIF_ACT,
sum(TOP_CANSEIF_SUR) as TOP_CANSEIF_SUR,
sum(TOP_CVAOMI_IND) as TOP_CVAOMI_IND,
sum(TOP_CVAUTRE_IND) as TOP_CVAUTRE_IND,
sum(TOP_CVAVC_AIG) as TOP_CVAVC_AIG ,
sum(TOP_CVAVC_SEQ) as TOP_CVAVC_SEQ,
sum(TOP_CVCORON_CHR) as TOP_CVCORON_CHR,
sum(TOP_CVEMBOL_AIG) as TOP_CVEMBOL_AIG,
sum(TOP_CVIC_AIG) as TOP_CVIC_AIG,
sum(TOP_CVIC_CHR) as TOP_CVIC_CHR,
sum(TOP_CVIDM_AIG) as TOP_CVIDM_AIG,
sum(TOP_CVTRRYC_IND) as TOP_CVTRRYC_IND,
sum(TOP_CVVALVE_IND) as TOP_CVVALVE_IND,
sum(TOP_FANTIHTA_MED) as TOP_FANTIHTA_MED,
sum(TOP_FDIABET_IND) as TOP_FDIABET_IND,
sum(TOP_FHYPOLI_MED) as TOP_FHYPOLI_MED,
sum(TOP_HFOIPAN_IND) as TOP_HFOIPAN_IND,
sum(TOP_IRAUTRE_IND) as TOP_IRAUTRE_IND,
sum(TOP_IRCRRCH_IND) as TOP_IRCRRCH_IND,
sum(TOP_IRHEMOP_IND) as TOP_IRHEMOP_IND,
sum(TOP_IRMMHER_IND) as TOP_IRMMHER_IND,
sum(TOP_IRMUCO_IND) as TOP_IRMUCO_IND,
sum(TOP_IRPOLYA_IND) as TOP_IRPOLYA_IND,
sum(TOP_IRSPONA_IND) as TOP_IRSPONA_IND,
sum(TOP_IRVIH_IND)as TOP_IRVIH_IND,
sum(TOP_MATERNI_IND) as TOP_MATERNI_IND,
sum(TOP_NAUTRES_IND) as TOP_NAUTRES_IND,
sum(TOP_NDEMENC_IND) as TOP_NDEMENC_IND ,
sum(TOP_NEPILEP_IND)as TOP_NEPILEP_IND,
sum(TOP_NMYOMYA_IND) as TOP_NMYOMYA_IND,
sum(TOP_NPARAPL_IND) as TOP_NPARAPL_IND,
sum(TOP_NPARKIN_IND) as TOP_NPARKIN_IND,
sum(TOP_NSEPLAQ_IND) as TOP_NSEPLAQ_IND,
sum(TOP_PADDICT_IND) as TOP_PADDICT_IND,
sum(TOP_PANTIDE_MED) as TOP_PANTIDE_MED,
sum(TOP_PANXIOL_MED) as TOP_PANXIOL_MED,
sum(TOP_PDEPNEV_IND) as TOP_PDEPNEV_IND,
sum(TOP_PHYPNOT_MED)as TOP_PHYPNOT_MED,
sum(TOP_PNEUROL_MED)as TOP_PNEUROL_MED,
sum(TOP_PRETARD_IND) as TOP_PRETARD_IND,
sum(TOP_PSYAUTR_IND) as TOP_PSYAUTR_IND,
sum(TOP_PSYCHOS_IND)as TOP_PSYCHOS_IND,
sum(TOP_PTRENFA_IND) as TOP_PTRENFA_IND,
sum(TOP_RDIALYSE_IND) as TOP_RDIALYSE_IND,
sum(TOP_RTRANS_AIG) as TOP_RTRANS_AIG,
sum(TOP_RTRANS_CHR)as TOP_RTRANS_CHR,
sum(sup_PAddAlc_ind) as TOP_ALCOOL,
sum(sup_PAddTab_ind) as TOP_TABAC,
sum(sup_PAddCan_ind) as TOP_canabis,
sum(sup_PAddAut_ind) as TOP_ADDICT_AUT
from CT_IND_2015_G4)
); 
disconnect from oracle;
quit;
data sasdata1.nat; set sasdata1.nat;
NB_TOT_REF=63737268;
run;

data sasdata1.com_sortie;
set sasdata1.com_final sasdata1.reg sasdata1.nat;
run;

proc sql; 
create table sasdata1.EPCI_final as 
select distinct 
EPCI2018,
sum(TOP_ABPCOIR_IND)as TOP_ABPCOIR_IND,
sum(TOP_CANAUTR_ACT) as TOP_CANAUTR_ACT,
sum(TOP_CANAUTR_SUR)as TOP_CANAUTR_SUR,
sum(TOP_CANCOLO_ACT) as TOP_CANCOLO_ACT,
sum(TOP_CANCOLO_SUR) as TOP_CANCOLO_SUR,
sum(TOP_CANPOUM_ACT) as TOP_CANPOUM_ACT,
sum(TOP_CANPOUM_SUR) as TOP_CANPOUM_SUR,
sum(TOP_CANPROS_ACT) as TOP_CANPROS_ACT,
sum(TOP_CANPROS_SUR) as TOP_CANPROS_SUR,
sum(TOP_CANSEIF_ACT) as TOP_CANSEIF_ACT,
sum(TOP_CANSEIF_SUR) as TOP_CANSEIF_SUR,
sum(TOP_CVAOMI_IND) as TOP_CVAOMI_IND,
sum(TOP_CVAUTRE_IND) as TOP_CVAUTRE_IND,
sum(TOP_CVAVC_AIG) as TOP_CVAVC_AIG ,
sum(TOP_CVAVC_SEQ) as TOP_CVAVC_SEQ,
sum(TOP_CVCORON_CHR) as TOP_CVCORON_CHR,
sum(TOP_CVEMBOL_AIG) as TOP_CVEMBOL_AIG,
sum(TOP_CVIC_AIG) as TOP_CVIC_AIG,
sum(TOP_CVIC_CHR) as TOP_CVIC_CHR,
sum(TOP_CVIDM_AIG) as TOP_CVIDM_AIG,
sum(TOP_CVTRRYC_IND) as TOP_CVTRRYC_IND,
sum(TOP_CVVALVE_IND) as TOP_CVVALVE_IND,
sum(TOP_FANTIHTA_MED) as TOP_FANTIHTA_MED,
sum(TOP_FDIABET_IND) as TOP_FDIABET_IND,
sum(TOP_FHYPOLI_MED) as TOP_FHYPOLI_MED,
sum(TOP_HFOIPAN_IND) as TOP_HFOIPAN_IND,
sum(TOP_IRAUTRE_IND) as TOP_IRAUTRE_IND,
sum(TOP_IRCRRCH_IND) as TOP_IRCRRCH_IND,
sum(TOP_IRHEMOP_IND) as TOP_IRHEMOP_IND,
sum(TOP_IRMMHER_IND) as TOP_IRMMHER_IND,
sum(TOP_IRMUCO_IND) as TOP_IRMUCO_IND,
sum(TOP_IRPOLYA_IND) as TOP_IRPOLYA_IND,
sum(TOP_IRSPONA_IND) as TOP_IRSPONA_IND,
sum(TOP_IRVIH_IND)as TOP_IRVIH_IND,
sum(TOP_MATERNI_IND) as TOP_MATERNI_IND,
sum(TOP_NAUTRES_IND) as TOP_NAUTRES_IND,
sum(TOP_NDEMENC_IND) as TOP_NDEMENC_IND ,
sum(TOP_NEPILEP_IND)as TOP_NEPILEP_IND,
sum(TOP_NMYOMYA_IND) as TOP_NMYOMYA_IND,
sum(TOP_NPARAPL_IND) as TOP_NPARAPL_IND,
sum(TOP_NPARKIN_IND) as TOP_NPARKIN_IND,
sum(TOP_NSEPLAQ_IND) as TOP_NSEPLAQ_IND,
sum(TOP_PADDICT_IND) as TOP_PADDICT_IND,
sum(TOP_PANTIDE_MED) as TOP_PANTIDE_MED,
sum(TOP_PANXIOL_MED) as TOP_PANXIOL_MED,
sum(TOP_PDEPNEV_IND) as TOP_PDEPNEV_IND,
sum(TOP_PHYPNOT_MED)as TOP_PHYPNOT_MED,
sum(TOP_PNEUROL_MED)as TOP_PNEUROL_MED,
sum(TOP_PRETARD_IND) as TOP_PRETARD_IND,
sum(TOP_PSYAUTR_IND) as TOP_PSYAUTR_IND,
sum(TOP_PSYCHOS_IND)as TOP_PSYCHOS_IND,
sum(TOP_PTRENFA_IND) as TOP_PTRENFA_IND,
sum(TOP_RDIALYSE_IND) as TOP_RDIALYSE_IND,
sum(TOP_RTRANS_AIG) as TOP_RTRANS_AIG,
sum(TOP_RTRANS_CHR)as TOP_RTRANS_CHR,
sum(TOP_ALCOOL) as TOP_ALCOOL,
sum(TOP_TABAC) as TOP_TABAC,
sum(TOP_canabis) as TOP_canabis,
sum(TOP_ADDICT_AUT) as TOP_ADDICT_AUT,
SUM(NB_TOT_REF) as NB_TOT_REF,
SUM(effpondREG_TOP_ABPCOIR_IND) AS effpondREG_TOP_ABPCOIR_IND,
SUM(effpondREG_TOP_CANAUTR_ACT) AS effpondREG_TOP_CANAUTR_ACT,
SUM(effpondREG_TOP_CANAUTR_SUR) AS effpondREG_TOP_CANAUTR_SUR,
SUM(effpondREG_TOP_CANCOLO_ACT) AS effpondREG_TOP_CANCOLO_ACT,
SUM(effpondREG_TOP_CANCOLO_SUR) AS effpondREG_TOP_CANCOLO_SUR,
SUM(effpondREG_TOP_CANPOUM_ACT) AS effpondREG_TOP_CANPOUM_ACT,
SUM(effpondREG_TOP_CANPOUM_SUR) AS effpondREG_TOP_CANPOUM_SUR,
SUM(effpondREG_TOP_CANPROS_ACT) AS effpondREG_TOP_CANPROS_ACT,
SUM(effpondREG_TOP_CANPROS_SUR) AS effpondREG_TOP_CANPROS_SUR,
SUM(effpondREG_TOP_CANSEIF_ACT) AS effpondREG_TOP_CANSEIF_ACT,
SUM(effpondREG_TOP_CANSEIF_SUR) AS effpondREG_TOP_CANSEIF_SUR,
SUM(effpondREG_TOP_CVAOMI_IND) AS effpondREG_TOP_CVAOMI_IND,
SUM(effpondREG_TOP_CVAUTRE_IND) AS effpondREG_TOP_CVAUTRE_IND,
SUM(effpondREG_TOP_CVAVC_AIG) AS effpondREG_TOP_CVAVC_AIG,
SUM(effpondREG_TOP_CVAVC_SEQ) AS effpondREG_TOP_CVAVC_SEQ,
SUM(effpondREG_TOP_CVCORON_CHR) AS effpondREG_TOP_CVCORON_CHR,
SUM(effpondREG_TOP_CVEMBOL_AIG) AS effpondREG_TOP_CVEMBOL_AIG,
SUM(effpondREG_TOP_CVIC_AIG) AS effpondREG_TOP_CVIC_AIG,
SUM(effpondREG_TOP_CVIC_CHR) AS effpondREG_TOP_CVIC_CHR,
SUM(effpondREG_TOP_CVIDM_AIG) AS effpondREG_TOP_CVIDM_AIG,
SUM(effpondREG_TOP_CVTRRYC_IND) AS effpondREG_TOP_CVTRRYC_IND,
SUM(effpondREG_TOP_CVVALVE_IND) AS effpondREG_TOP_CVVALVE_IND,
SUM(effpondREG_TOP_FANTIHTA_MED) AS effpondREG_TOP_FANTIHTA_MED,
SUM(effpondREG_TOP_FDIABET_IND) AS effpondREG_TOP_FDIABET_IND,
SUM(effpondREG_TOP_FHYPOLI_MED) AS effpondREG_TOP_FHYPOLI_MED,
SUM(effpondREG_TOP_HFOIPAN_IND) AS effpondREG_TOP_HFOIPAN_IND,
SUM(effpondREG_TOP_IRAUTRE_IND) AS effpondREG_TOP_IRAUTRE_IND,
SUM(effpondREG_TOP_IRCRRCH_IND) AS effpondREG_TOP_IRCRRCH_IND,
SUM(effpondREG_TOP_IRHEMOP_IND) AS effpondREG_TOP_IRHEMOP_IND,
SUM(effpondREG_TOP_IRMMHER_IND) AS effpondREG_TOP_IRMMHER_IND,
SUM(effpondREG_TOP_IRMUCO_IND) AS effpondREG_TOP_IRMUCO_IND,
SUM(effpondREG_TOP_IRPOLYA_IND) AS effpondREG_TOP_IRPOLYA_IND,
SUM(effpondREG_TOP_IRSPONA_IND) AS effpondREG_TOP_IRSPONA_IND,
SUM(effpondREG_TOP_IRVIH_IND) AS effpondREG_TOP_IRVIH_IND,
SUM(effpondREG_TOP_MATERNI_IND) AS effpondREG_TOP_MATERNI_IND,
SUM(effpondREG_TOP_NAUTRES_IND) AS effpondREG_TOP_NAUTRES_IND,
SUM(effpondREG_TOP_NDEMENC_IND) AS effpondREG_TOP_NDEMENC_IND,
SUM(effpondREG_TOP_NEPILEP_IND) AS effpondREG_TOP_NEPILEP_IND,
SUM(effpondREG_TOP_NMYOMYA_IND) AS effpondREG_TOP_NMYOMYA_IND,
SUM(effpondREG_TOP_NPARAPL_IND) AS effpondREG_TOP_NPARAPL_IND,
SUM(effpondREG_TOP_NPARKIN_IND) AS effpondREG_TOP_NPARKIN_IND,
SUM(effpondREG_TOP_NSEPLAQ_IND) AS effpondREG_TOP_NSEPLAQ_IND,
SUM(effpondREG_TOP_PADDICT_IND) AS effpondREG_TOP_PADDICT_IND,
SUM(effpondREG_TOP_PANTIDE_MED) AS effpondREG_TOP_PANTIDE_MED,
SUM(effpondREG_TOP_PANXIOL_MED) AS effpondREG_TOP_PANXIOL_MED,
SUM(effpondREG_TOP_PDEPNEV_IND) AS effpondREG_TOP_PDEPNEV_IND,
SUM(effpondREG_TOP_PHYPNOT_MED) AS effpondREG_TOP_PHYPNOT_MED,
SUM(effpondREG_TOP_PNEUROL_MED) AS effpondREG_TOP_PNEUROL_MED,
SUM(effpondREG_TOP_PRETARD_IND) AS effpondREG_TOP_PRETARD_IND,
SUM(effpondREG_TOP_PSYAUTR_IND) AS effpondREG_TOP_PSYAUTR_IND,
SUM(effpondREG_TOP_PSYCHOS_IND) AS effpondREG_TOP_PSYCHOS_IND,
SUM(effpondREG_TOP_PTRENFA_IND) AS effpondREG_TOP_PTRENFA_IND,
SUM(effpondREG_TOP_RDIALYSE_IND) AS effpondREG_TOP_RDIALYSE_IND,
SUM(effpondREG_TOP_RTRANS_AIG) AS effpondREG_TOP_RTRANS_AIG,
SUM(effpondREG_TOP_RTRANS_CHR) AS effpondREG_TOP_RTRANS_CHR,
SUM(effpondREG_TOP_ALCOOL) AS effpondREG_TOP_ALCOOL,
SUM(effpondREG_TOP_TABAC) AS effpondREG_TOP_TABAC,
SUM(effpondREG_TOP_canabis) AS effpondREG_TOP_canabis,
SUM(effpondREG_TOP_ADDICT_AUT) AS effpondREG_TOP_ADDICT_AUT,
SUM(effpondNAT_TOP_ABPCOIR_IND) AS effpondNAT_TOP_ABPCOIR_IND,
SUM(effpondNAT_TOP_CANAUTR_ACT) AS effpondNAT_TOP_CANAUTR_ACT,
SUM(effpondNAT_TOP_CANAUTR_SUR) AS effpondNAT_TOP_CANAUTR_SUR,
SUM(effpondNAT_TOP_CANCOLO_ACT) AS effpondNAT_TOP_CANCOLO_ACT,
SUM(effpondNAT_TOP_CANCOLO_SUR) AS effpondNAT_TOP_CANCOLO_SUR,
SUM(effpondNAT_TOP_CANPOUM_ACT) AS effpondNAT_TOP_CANPOUM_ACT,
SUM(effpondNAT_TOP_CANPOUM_SUR) AS effpondNAT_TOP_CANPOUM_SUR,
SUM(effpondNAT_TOP_CANPROS_ACT) AS effpondNAT_TOP_CANPROS_ACT,
SUM(effpondNAT_TOP_CANPROS_SUR) AS effpondNAT_TOP_CANPROS_SUR,
SUM(effpondNAT_TOP_CANSEIF_ACT) AS effpondNAT_TOP_CANSEIF_ACT,
SUM(effpondNAT_TOP_CANSEIF_SUR) AS effpondNAT_TOP_CANSEIF_SUR,
SUM(effpondNAT_TOP_CVAOMI_IND) AS effpondNAT_TOP_CVAOMI_IND,
SUM(effpondNAT_TOP_CVAUTRE_IND) AS effpondNAT_TOP_CVAUTRE_IND,
SUM(effpondNAT_TOP_CVAVC_AIG) AS effpondNAT_TOP_CVAVC_AIG,
SUM(effpondNAT_TOP_CVAVC_SEQ) AS effpondNAT_TOP_CVAVC_SEQ,
SUM(effpondNAT_TOP_CVCORON_CHR) AS effpondNAT_TOP_CVCORON_CHR,
SUM(effpondNAT_TOP_CVEMBOL_AIG) AS effpondNAT_TOP_CVEMBOL_AIG,
SUM(effpondNAT_TOP_CVIC_AIG) AS effpondNAT_TOP_CVIC_AIG,
SUM(effpondNAT_TOP_CVIC_CHR) AS effpondNAT_TOP_CVIC_CHR,
SUM(effpondNAT_TOP_CVIDM_AIG) AS effpondNAT_TOP_CVIDM_AIG,
SUM(effpondNAT_TOP_CVTRRYC_IND) AS effpondNAT_TOP_CVTRRYC_IND,
SUM(effpondNAT_TOP_CVVALVE_IND) AS effpondNAT_TOP_CVVALVE_IND,
SUM(effpondNAT_TOP_FANTIHTA_MED) AS effpondNAT_TOP_FANTIHTA_MED,
SUM(effpondNAT_TOP_FDIABET_IND) AS effpondNAT_TOP_FDIABET_IND,
SUM(effpondNAT_TOP_FHYPOLI_MED) AS effpondNAT_TOP_FHYPOLI_MED,
SUM(effpondNAT_TOP_HFOIPAN_IND) AS effpondNAT_TOP_HFOIPAN_IND,
SUM(effpondNAT_TOP_IRAUTRE_IND) AS effpondNAT_TOP_IRAUTRE_IND,
SUM(effpondNAT_TOP_IRCRRCH_IND) AS effpondNAT_TOP_IRCRRCH_IND,
SUM(effpondNAT_TOP_IRHEMOP_IND) AS effpondNAT_TOP_IRHEMOP_IND,
SUM(effpondNAT_TOP_IRMMHER_IND) AS effpondNAT_TOP_IRMMHER_IND,
SUM(effpondNAT_TOP_IRMUCO_IND) AS effpondNAT_TOP_IRMUCO_IND,
SUM(effpondNAT_TOP_IRPOLYA_IND) AS effpondNAT_TOP_IRPOLYA_IND,
SUM(effpondNAT_TOP_IRSPONA_IND) AS effpondNAT_TOP_IRSPONA_IND,
SUM(effpondNAT_TOP_IRVIH_IND) AS effpondNAT_TOP_IRVIH_IND,
SUM(effpondNAT_TOP_MATERNI_IND) AS effpondNAT_TOP_MATERNI_IND,
SUM(effpondNAT_TOP_NAUTRES_IND) AS effpondNAT_TOP_NAUTRES_IND,
SUM(effpondNAT_TOP_NDEMENC_IND) AS effpondNAT_TOP_NDEMENC_IND,
SUM(effpondNAT_TOP_NEPILEP_IND) AS effpondNAT_TOP_NEPILEP_IND,
SUM(effpondNAT_TOP_NMYOMYA_IND) AS effpondNAT_TOP_NMYOMYA_IND,
SUM(effpondNAT_TOP_NPARAPL_IND) AS effpondNAT_TOP_NPARAPL_IND,
SUM(effpondNAT_TOP_NPARKIN_IND) AS effpondNAT_TOP_NPARKIN_IND,
SUM(effpondNAT_TOP_NSEPLAQ_IND) AS effpondNAT_TOP_NSEPLAQ_IND,
SUM(effpondNAT_TOP_PADDICT_IND) AS effpondNAT_TOP_PADDICT_IND,
SUM(effpondNAT_TOP_PANTIDE_MED) AS effpondNAT_TOP_PANTIDE_MED,
SUM(effpondNAT_TOP_PANXIOL_MED) AS effpondNAT_TOP_PANXIOL_MED,
SUM(effpondNAT_TOP_PDEPNEV_IND) AS effpondNAT_TOP_PDEPNEV_IND,
SUM(effpondNAT_TOP_PHYPNOT_MED) AS effpondNAT_TOP_PHYPNOT_MED,
SUM(effpondNAT_TOP_PNEUROL_MED) AS effpondNAT_TOP_PNEUROL_MED,
SUM(effpondNAT_TOP_PRETARD_IND) AS effpondNAT_TOP_PRETARD_IND,
SUM(effpondNAT_TOP_PSYAUTR_IND) AS effpondNAT_TOP_PSYAUTR_IND,
SUM(effpondNAT_TOP_PSYCHOS_IND) AS effpondNAT_TOP_PSYCHOS_IND,
SUM(effpondNAT_TOP_PTRENFA_IND) AS effpondNAT_TOP_PTRENFA_IND,
SUM(effpondNAT_TOP_RDIALYSE_IND) AS effpondNAT_TOP_RDIALYSE_IND,
SUM(effpondNAT_TOP_RTRANS_AIG) AS effpondNAT_TOP_RTRANS_AIG,
SUM(effpondNAT_TOP_RTRANS_CHR) AS effpondNAT_TOP_RTRANS_CHR,
SUM(effpondNAT_TOP_ALCOOL) AS effpondNAT_TOP_ALCOOL,
SUM(effpondNAT_TOP_TABAC) AS effpondNAT_TOP_TABAC,
SUM(effpondNAT_TOP_canabis) AS effpondNAT_TOP_canabis,
SUM(effpondNAT_TOP_ADDICT_AUT) AS effpondNAT_TOP_ADDICT_AUT
from sasdata1.carto7 
group by 1; 
quit;


proc sql;
create table variable as select name from sashelp.vcolumn where libname="SASDATA1" 
and memname="CARTO7";
quit;


proc sql;
create table EPCI_sortie as select 
EPCI2018,
(TOP_ABPCOIR_IND/NB_TOT_REF) AS FREQ_TOP_ABPCOIR_IND,
(TOP_CANAUTR_ACT/NB_TOT_REF) AS FREQ_TOP_CANAUTR_ACT,
(TOP_CANAUTR_SUR/NB_TOT_REF) AS FREQ_TOP_CANAUTR_SUR,
(TOP_CANCOLO_ACT/NB_TOT_REF) AS FREQ_TOP_CANCOLO_ACT,
(TOP_CANCOLO_SUR/NB_TOT_REF) AS FREQ_TOP_CANCOLO_SUR,
(TOP_CANPOUM_ACT/NB_TOT_REF) AS FREQ_TOP_CANPOUM_ACT,
(TOP_CANPOUM_SUR/NB_TOT_REF) AS FREQ_TOP_CANPOUM_SUR,
(TOP_CANPROS_ACT/NB_TOT_REF) AS FREQ_TOP_CANPROS_ACT,
(TOP_CANPROS_SUR/NB_TOT_REF) AS FREQ_TOP_CANPROS_SUR,
(TOP_CANSEIF_ACT/NB_TOT_REF) AS FREQ_TOP_CANSEIF_ACT,
(TOP_CANSEIF_SUR/NB_TOT_REF) AS FREQ_TOP_CANSEIF_SUR,
(TOP_CVAOMI_IND/NB_TOT_REF) AS FREQ_TOP_CVAOMI_IND,
(TOP_CVAUTRE_IND/NB_TOT_REF) AS FREQ_TOP_CVAUTRE_IND,
(TOP_CVAVC_AIG/NB_TOT_REF) AS FREQ_TOP_CVAVC_AIG,
(TOP_CVAVC_SEQ/NB_TOT_REF) AS FREQ_TOP_CVAVC_SEQ,
(TOP_CVCORON_CHR/NB_TOT_REF) AS FREQ_TOP_CVCORON_CHR,
(TOP_CVEMBOL_AIG/NB_TOT_REF) AS FREQ_TOP_CVEMBOL_AIG,
(TOP_CVIC_AIG/NB_TOT_REF) AS FREQ_TOP_CVIC_AIG,
(TOP_CVIC_CHR/NB_TOT_REF) AS FREQ_TOP_CVIC_CHR,
(TOP_CVIDM_AIG/NB_TOT_REF) AS FREQ_TOP_CVIDM_AIG,
(TOP_CVTRRYC_IND/NB_TOT_REF) AS FREQ_TOP_CVTRRYC_IND,
(TOP_CVVALVE_IND/NB_TOT_REF) AS FREQ_TOP_CVVALVE_IND,
(TOP_FANTIHTA_MED/NB_TOT_REF) AS FREQ_TOP_FANTIHTA_MED,
(TOP_FDIABET_IND/NB_TOT_REF) AS FREQ_TOP_FDIABET_IND,
(TOP_FHYPOLI_MED/NB_TOT_REF) AS FREQ_TOP_FHYPOLI_MED,
(TOP_HFOIPAN_IND/NB_TOT_REF) AS FREQ_TOP_HFOIPAN_IND,
(TOP_IRAUTRE_IND/NB_TOT_REF) AS FREQ_TOP_IRAUTRE_IND,
(TOP_IRCRRCH_IND/NB_TOT_REF) AS FREQ_TOP_IRCRRCH_IND,
(TOP_IRHEMOP_IND/NB_TOT_REF) AS FREQ_TOP_IRHEMOP_IND,
(TOP_IRMMHER_IND/NB_TOT_REF) AS FREQ_TOP_IRMMHER_IND,
(TOP_IRMUCO_IND/NB_TOT_REF) AS FREQ_TOP_IRMUCO_IND,
(TOP_IRPOLYA_IND/NB_TOT_REF) AS FREQ_TOP_IRPOLYA_IND,
(TOP_IRSPONA_IND/NB_TOT_REF) AS FREQ_TOP_IRSPONA_IND,
(TOP_IRVIH_IND/NB_TOT_REF) AS FREQ_TOP_IRVIH_IND,
(TOP_MATERNI_IND/NB_TOT_REF) AS FREQ_TOP_MATERNI_IND,
(TOP_NAUTRES_IND/NB_TOT_REF) AS FREQ_TOP_NAUTRES_IND,
(TOP_NDEMENC_IND/NB_TOT_REF) AS FREQ_TOP_NDEMENC_IND,
(TOP_NEPILEP_IND/NB_TOT_REF) AS FREQ_TOP_NEPILEP_IND,
(TOP_NMYOMYA_IND/NB_TOT_REF) AS FREQ_TOP_NMYOMYA_IND,
(TOP_NPARAPL_IND/NB_TOT_REF) AS FREQ_TOP_NPARAPL_IND,
(TOP_NPARKIN_IND/NB_TOT_REF) AS FREQ_TOP_NPARKIN_IND,
(TOP_NSEPLAQ_IND/NB_TOT_REF) AS FREQ_TOP_NSEPLAQ_IND,
(TOP_PADDICT_IND/NB_TOT_REF) AS FREQ_TOP_PADDICT_IND,
(TOP_PANTIDE_MED/NB_TOT_REF) AS FREQ_TOP_PANTIDE_MED,
(TOP_PANXIOL_MED/NB_TOT_REF) AS FREQ_TOP_PANXIOL_MED,
(TOP_PDEPNEV_IND/NB_TOT_REF) AS FREQ_TOP_PDEPNEV_IND,
(TOP_PHYPNOT_MED/NB_TOT_REF) AS FREQ_TOP_PHYPNOT_MED,
(TOP_PNEUROL_MED/NB_TOT_REF) AS FREQ_TOP_PNEUROL_MED,
(TOP_PRETARD_IND/NB_TOT_REF) AS FREQ_TOP_PRETARD_IND,
(TOP_PSYAUTR_IND/NB_TOT_REF) AS FREQ_TOP_PSYAUTR_IND,
(TOP_PSYCHOS_IND/NB_TOT_REF) AS FREQ_TOP_PSYCHOS_IND,
(TOP_PTRENFA_IND/NB_TOT_REF) AS FREQ_TOP_PTRENFA_IND,
(TOP_RDIALYSE_IND/NB_TOT_REF) AS FREQ_TOP_RDIALYSE_IND,
(TOP_RTRANS_AIG/NB_TOT_REF) AS FREQ_TOP_RTRANS_AIG,
(TOP_RTRANS_CHR/NB_TOT_REF) AS FREQ_TOP_RTRANS_CHR,
(TOP_ALCOOL/NB_TOT_REF) AS FREQ_TOP_ALCOOL,
(TOP_TABAC/NB_TOT_REF) AS FREQ_TOP_TABAC,
(TOP_canabis/NB_TOT_REF) AS FREQ_TOP_canabis,
(TOP_ADDICT_AUT/NB_TOT_REF) AS FREQ_TOP_ADDICT_AUT,
(effpondREG_TOP_ABPCOIR_IND/NB_TOT_REF) AS FREQ_effpondREG_TOP_ABPCOIR_IND,
(effpondREG_TOP_CANAUTR_ACT/NB_TOT_REF) AS FREQ_effpondREG_TOP_CANAUTR_ACT,
(effpondREG_TOP_CANAUTR_SUR/NB_TOT_REF) AS FREQ_effpondREG_TOP_CANAUTR_SUR,
(effpondREG_TOP_CANCOLO_ACT/NB_TOT_REF) AS FREQ_effpondREG_TOP_CANCOLO_ACT,
(effpondREG_TOP_CANCOLO_SUR/NB_TOT_REF) AS FREQ_effpondREG_TOP_CANCOLO_SUR,
(effpondREG_TOP_CANPOUM_ACT/NB_TOT_REF) AS FREQ_effpondREG_TOP_CANPOUM_ACT,
(effpondREG_TOP_CANPOUM_SUR/NB_TOT_REF) AS FREQ_effpondREG_TOP_CANPOUM_SUR,
(effpondREG_TOP_CANPROS_ACT/NB_TOT_REF) AS FREQ_effpondREG_TOP_CANPROS_ACT,
(effpondREG_TOP_CANPROS_SUR/NB_TOT_REF) AS FREQ_effpondREG_TOP_CANPROS_SUR,
(effpondREG_TOP_CANSEIF_ACT/NB_TOT_REF) AS FREQ_effpondREG_TOP_CANSEIF_ACT,
(effpondREG_TOP_CANSEIF_SUR/NB_TOT_REF) AS FREQ_effpondREG_TOP_CANSEIF_SUR,
(effpondREG_TOP_CVAOMI_IND/NB_TOT_REF) AS FREQ_effpondREG_TOP_CVAOMI_IND,
(effpondREG_TOP_CVAUTRE_IND/NB_TOT_REF) AS FREQ_effpondREG_TOP_CVAUTRE_IND,
(effpondREG_TOP_CVAVC_AIG/NB_TOT_REF) AS FREQ_effpondREG_TOP_CVAVC_AIG,
(effpondREG_TOP_CVAVC_SEQ/NB_TOT_REF) AS FREQ_effpondREG_TOP_CVAVC_SEQ,
(effpondREG_TOP_CVCORON_CHR/NB_TOT_REF) AS FREQ_effpondREG_TOP_CVCORON_CHR,
(effpondREG_TOP_CVEMBOL_AIG/NB_TOT_REF) AS FREQ_effpondREG_TOP_CVEMBOL_AIG,
(effpondREG_TOP_CVIC_AIG/NB_TOT_REF) AS FREQ_effpondREG_TOP_CVIC_AIG,
(effpondREG_TOP_CVIC_CHR/NB_TOT_REF) AS FREQ_effpondREG_TOP_CVIC_CHR,
(effpondREG_TOP_CVIDM_AIG/NB_TOT_REF) AS FREQ_effpondREG_TOP_CVIDM_AIG,
(effpondREG_TOP_CVTRRYC_IND/NB_TOT_REF) AS FREQ_effpondREG_TOP_CVTRRYC_IND,
(effpondREG_TOP_CVVALVE_IND/NB_TOT_REF) AS FREQ_effpondREG_TOP_CVVALVE_IND,
(effpondREG_TOP_FANTIHTA_MED/NB_TOT_REF) AS FREQ_effpondREG_TOP_FANTIHTA_MED,
(effpondREG_TOP_FDIABET_IND/NB_TOT_REF) AS FREQ_effpondREG_TOP_FDIABET_IND,
(effpondREG_TOP_FHYPOLI_MED/NB_TOT_REF) AS FREQ_effpondREG_TOP_FHYPOLI_MED,
(effpondREG_TOP_HFOIPAN_IND/NB_TOT_REF) AS FREQ_effpondREG_TOP_HFOIPAN_IND,
(effpondREG_TOP_IRAUTRE_IND/NB_TOT_REF) AS FREQ_effpondREG_TOP_IRAUTRE_IND,
(effpondREG_TOP_IRCRRCH_IND/NB_TOT_REF) AS FREQ_effpondREG_TOP_IRCRRCH_IND,
(effpondREG_TOP_IRHEMOP_IND/NB_TOT_REF) AS FREQ_effpondREG_TOP_IRHEMOP_IND,
(effpondREG_TOP_IRMMHER_IND/NB_TOT_REF) AS FREQ_effpondREG_TOP_IRMMHER_IND,
(effpondREG_TOP_IRMUCO_IND/NB_TOT_REF) AS FREQ_effpondREG_TOP_IRMUCO_IND,
(effpondREG_TOP_IRPOLYA_IND/NB_TOT_REF) AS FREQ_effpondREG_TOP_IRPOLYA_IND,
(effpondREG_TOP_IRSPONA_IND/NB_TOT_REF) AS FREQ_effpondREG_TOP_IRSPONA_IND,
(effpondREG_TOP_IRVIH_IND/NB_TOT_REF) AS FREQ_effpondREG_TOP_IRVIH_IND,
(effpondREG_TOP_MATERNI_IND/NB_TOT_REF) AS FREQ_effpondREG_TOP_MATERNI_IND,
(effpondREG_TOP_NAUTRES_IND/NB_TOT_REF) AS FREQ_effpondREG_TOP_NAUTRES_IND,
(effpondREG_TOP_NDEMENC_IND/NB_TOT_REF) AS FREQ_effpondREG_TOP_NDEMENC_IND,
(effpondREG_TOP_NEPILEP_IND/NB_TOT_REF) AS FREQ_effpondREG_TOP_NEPILEP_IND,
(effpondREG_TOP_NMYOMYA_IND/NB_TOT_REF) AS FREQ_effpondREG_TOP_NMYOMYA_IND,
(effpondREG_TOP_NPARAPL_IND/NB_TOT_REF) AS FREQ_effpondREG_TOP_NPARAPL_IND,
(effpondREG_TOP_NPARKIN_IND/NB_TOT_REF) AS FREQ_effpondREG_TOP_NPARKIN_IND,
(effpondREG_TOP_NSEPLAQ_IND/NB_TOT_REF) AS FREQ_effpondREG_TOP_NSEPLAQ_IND,
(effpondREG_TOP_PADDICT_IND/NB_TOT_REF) AS FREQ_effpondREG_TOP_PADDICT_IND,
(effpondREG_TOP_PANTIDE_MED/NB_TOT_REF) AS FREQ_effpondREG_TOP_PANTIDE_MED,
(effpondREG_TOP_PANXIOL_MED/NB_TOT_REF) AS FREQ_effpondREG_TOP_PANXIOL_MED,
(effpondREG_TOP_PDEPNEV_IND/NB_TOT_REF) AS FREQ_effpondREG_TOP_PDEPNEV_IND,
(effpondREG_TOP_PHYPNOT_MED/NB_TOT_REF) AS FREQ_effpondREG_TOP_PHYPNOT_MED,
(effpondREG_TOP_PNEUROL_MED/NB_TOT_REF) AS FREQ_effpondREG_TOP_PNEUROL_MED,
(effpondREG_TOP_PRETARD_IND/NB_TOT_REF) AS FREQ_effpondREG_TOP_PRETARD_IND,
(effpondREG_TOP_PSYAUTR_IND/NB_TOT_REF) AS FREQ_effpondREG_TOP_PSYAUTR_IND,
(effpondREG_TOP_PSYCHOS_IND/NB_TOT_REF) AS FREQ_effpondREG_TOP_PSYCHOS_IND,
(effpondREG_TOP_PTRENFA_IND/NB_TOT_REF) AS FREQ_effpondREG_TOP_PTRENFA_IND,
(effpondREG_TOP_RDIALYSE_IND/NB_TOT_REF) AS FREQ_effpondREG_TOP_RDIALYSE_IND,
(effpondREG_TOP_RTRANS_AIG/NB_TOT_REF) AS FREQ_effpondREG_TOP_RTRANS_AIG,
(effpondREG_TOP_RTRANS_CHR/NB_TOT_REF) AS FREQ_effpondREG_TOP_RTRANS_CHR,
(effpondREG_TOP_ALCOOL/NB_TOT_REF) AS FREQ_effpondREG_TOP_ALCOOL,
(effpondREG_TOP_TABAC/NB_TOT_REF) AS FREQ_effpondREG_TOP_TABAC,
(effpondREG_TOP_canabis/NB_TOT_REF) AS FREQ_effpondREG_TOP_canabis,
(effpondREG_TOP_ADDICT_AUT/NB_TOT_REF) AS FREQ_effpondREG_TOP_ADDICT_AUT,
(effpondNAT_TOP_ABPCOIR_IND/NB_TOT_REF) AS FREQ_effpondNAT_TOP_ABPCOIR_IND,
(effpondNAT_TOP_CANAUTR_ACT/NB_TOT_REF) AS FREQ_effpondNAT_TOP_CANAUTR_ACT,
(effpondNAT_TOP_CANAUTR_SUR/NB_TOT_REF) AS FREQ_effpondNAT_TOP_CANAUTR_SUR,
(effpondNAT_TOP_CANCOLO_ACT/NB_TOT_REF) AS FREQ_effpondNAT_TOP_CANCOLO_ACT,
(effpondNAT_TOP_CANCOLO_SUR/NB_TOT_REF) AS FREQ_effpondNAT_TOP_CANCOLO_SUR,
(effpondNAT_TOP_CANPOUM_ACT/NB_TOT_REF) AS FREQ_effpondNAT_TOP_CANPOUM_ACT,
(effpondNAT_TOP_CANPOUM_SUR/NB_TOT_REF) AS FREQ_effpondNAT_TOP_CANPOUM_SUR,
(effpondNAT_TOP_CANPROS_ACT/NB_TOT_REF) AS FREQ_effpondNAT_TOP_CANPROS_ACT,
(effpondNAT_TOP_CANPROS_SUR/NB_TOT_REF) AS FREQ_effpondNAT_TOP_CANPROS_SUR,
(effpondNAT_TOP_CANSEIF_ACT/NB_TOT_REF) AS FREQ_effpondNAT_TOP_CANSEIF_ACT,
(effpondNAT_TOP_CANSEIF_SUR/NB_TOT_REF) AS FREQ_effpondNAT_TOP_CANSEIF_SUR,
(effpondNAT_TOP_CVAOMI_IND/NB_TOT_REF) AS FREQ_effpondNAT_TOP_CVAOMI_IND,
(effpondNAT_TOP_CVAUTRE_IND/NB_TOT_REF) AS FREQ_effpondNAT_TOP_CVAUTRE_IND,
(effpondNAT_TOP_CVAVC_AIG/NB_TOT_REF) AS FREQ_effpondNAT_TOP_CVAVC_AIG,
(effpondNAT_TOP_CVAVC_SEQ/NB_TOT_REF) AS FREQ_effpondNAT_TOP_CVAVC_SEQ,
(effpondNAT_TOP_CVCORON_CHR/NB_TOT_REF) AS FREQ_effpondNAT_TOP_CVCORON_CHR,
(effpondNAT_TOP_CVEMBOL_AIG/NB_TOT_REF) AS FREQ_effpondNAT_TOP_CVEMBOL_AIG,
(effpondNAT_TOP_CVIC_AIG/NB_TOT_REF) AS FREQ_effpondNAT_TOP_CVIC_AIG,
(effpondNAT_TOP_CVIC_CHR/NB_TOT_REF) AS FREQ_effpondNAT_TOP_CVIC_CHR,
(effpondNAT_TOP_CVIDM_AIG/NB_TOT_REF) AS FREQ_effpondNAT_TOP_CVIDM_AIG,
(effpondNAT_TOP_CVTRRYC_IND/NB_TOT_REF) AS FREQ_effpondNAT_TOP_CVTRRYC_IND,
(effpondNAT_TOP_CVVALVE_IND/NB_TOT_REF) AS FREQ_effpondNAT_TOP_CVVALVE_IND,
(effpondNAT_TOP_FANTIHTA_MED/NB_TOT_REF) AS FREQ_effpondNAT_TOP_FANTIHTA_MED,
(effpondNAT_TOP_FDIABET_IND/NB_TOT_REF) AS FREQ_effpondNAT_TOP_FDIABET_IND,
(effpondNAT_TOP_FHYPOLI_MED/NB_TOT_REF) AS FREQ_effpondNAT_TOP_FHYPOLI_MED,
(effpondNAT_TOP_HFOIPAN_IND/NB_TOT_REF) AS FREQ_effpondNAT_TOP_HFOIPAN_IND,
(effpondNAT_TOP_IRAUTRE_IND/NB_TOT_REF) AS FREQ_effpondNAT_TOP_IRAUTRE_IND,
(effpondNAT_TOP_IRCRRCH_IND/NB_TOT_REF) AS FREQ_effpondNAT_TOP_IRCRRCH_IND,
(effpondNAT_TOP_IRHEMOP_IND/NB_TOT_REF) AS FREQ_effpondNAT_TOP_IRHEMOP_IND,
(effpondNAT_TOP_IRMMHER_IND/NB_TOT_REF) AS FREQ_effpondNAT_TOP_IRMMHER_IND,
(effpondNAT_TOP_IRMUCO_IND/NB_TOT_REF) AS FREQ_effpondNAT_TOP_IRMUCO_IND,
(effpondNAT_TOP_IRPOLYA_IND/NB_TOT_REF) AS FREQ_effpondNAT_TOP_IRPOLYA_IND,
(effpondNAT_TOP_IRSPONA_IND/NB_TOT_REF) AS FREQ_effpondNAT_TOP_IRSPONA_IND,
(effpondNAT_TOP_IRVIH_IND/NB_TOT_REF) AS FREQ_effpondNAT_TOP_IRVIH_IND,
(effpondNAT_TOP_MATERNI_IND/NB_TOT_REF) AS FREQ_effpondNAT_TOP_MATERNI_IND,
(effpondNAT_TOP_NAUTRES_IND/NB_TOT_REF) AS FREQ_effpondNAT_TOP_NAUTRES_IND,
(effpondNAT_TOP_NDEMENC_IND/NB_TOT_REF) AS FREQ_effpondNAT_TOP_NDEMENC_IND,
(effpondNAT_TOP_NEPILEP_IND/NB_TOT_REF) AS FREQ_effpondNAT_TOP_NEPILEP_IND,
(effpondNAT_TOP_NMYOMYA_IND/NB_TOT_REF) AS FREQ_effpondNAT_TOP_NMYOMYA_IND,
(effpondNAT_TOP_NPARAPL_IND/NB_TOT_REF) AS FREQ_effpondNAT_TOP_NPARAPL_IND,
(effpondNAT_TOP_NPARKIN_IND/NB_TOT_REF) AS FREQ_effpondNAT_TOP_NPARKIN_IND,
(effpondNAT_TOP_NSEPLAQ_IND/NB_TOT_REF) AS FREQ_effpondNAT_TOP_NSEPLAQ_IND,
(effpondNAT_TOP_PADDICT_IND/NB_TOT_REF) AS FREQ_effpondNAT_TOP_PADDICT_IND,
(effpondNAT_TOP_PANTIDE_MED/NB_TOT_REF) AS FREQ_effpondNAT_TOP_PANTIDE_MED,
(effpondNAT_TOP_PANXIOL_MED/NB_TOT_REF) AS FREQ_effpondNAT_TOP_PANXIOL_MED,
(effpondNAT_TOP_PDEPNEV_IND/NB_TOT_REF) AS FREQ_effpondNAT_TOP_PDEPNEV_IND,
(effpondNAT_TOP_PHYPNOT_MED/NB_TOT_REF) AS FREQ_effpondNAT_TOP_PHYPNOT_MED,
(effpondNAT_TOP_PNEUROL_MED/NB_TOT_REF) AS FREQ_effpondNAT_TOP_PNEUROL_MED,
(effpondNAT_TOP_PRETARD_IND/NB_TOT_REF) AS FREQ_effpondNAT_TOP_PRETARD_IND,
(effpondNAT_TOP_PSYAUTR_IND/NB_TOT_REF) AS FREQ_effpondNAT_TOP_PSYAUTR_IND,
(effpondNAT_TOP_PSYCHOS_IND/NB_TOT_REF) AS FREQ_effpondNAT_TOP_PSYCHOS_IND,
(effpondNAT_TOP_PTRENFA_IND/NB_TOT_REF) AS FREQ_effpondNAT_TOP_PTRENFA_IND,
(effpondNAT_TOP_RDIALYSE_IND/NB_TOT_REF) AS FREQ_effpondNAT_TOP_RDIALYSE_IND,
(effpondNAT_TOP_RTRANS_AIG/NB_TOT_REF) AS FREQ_effpondNAT_TOP_RTRANS_AIG,
(effpondNAT_TOP_RTRANS_CHR/NB_TOT_REF) AS FREQ_effpondNAT_TOP_RTRANS_CHR,
(effpondNAT_TOP_ALCOOL/NB_TOT_REF) AS FREQ_effpondNAT_TOP_ALCOOL,
(effpondNAT_TOP_TABAC/NB_TOT_REF) AS FREQ_effpondNAT_TOP_TABAC,
(effpondNAT_TOP_canabis/NB_TOT_REF) AS FREQ_effpondNAT_TOP_canabis,
(effpondNAT_TOP_ADDICT_AUT/NB_TOT_REF) AS FREQ_effpondNAT_TOP_ADDICT_AUT
from SASDATA1.EPCi_final
group by 1;
quit;




proc sql;
create table reg_sortie as select 
"reg",
(TOP_ABPCOIR_IND/NB_TOT_REF) AS FREQ_TOP_ABPCOIR_IND,
(TOP_CANAUTR_ACT/NB_TOT_REF) AS FREQ_TOP_CANAUTR_ACT,
(TOP_CANAUTR_SUR/NB_TOT_REF) AS FREQ_TOP_CANAUTR_SUR,
(TOP_CANCOLO_ACT/NB_TOT_REF) AS FREQ_TOP_CANCOLO_ACT,
(TOP_CANCOLO_SUR/NB_TOT_REF) AS FREQ_TOP_CANCOLO_SUR,
(TOP_CANPOUM_ACT/NB_TOT_REF) AS FREQ_TOP_CANPOUM_ACT,
(TOP_CANPOUM_SUR/NB_TOT_REF) AS FREQ_TOP_CANPOUM_SUR,
(TOP_CANPROS_ACT/NB_TOT_REF) AS FREQ_TOP_CANPROS_ACT,
(TOP_CANPROS_SUR/NB_TOT_REF) AS FREQ_TOP_CANPROS_SUR,
(TOP_CANSEIF_ACT/NB_TOT_REF) AS FREQ_TOP_CANSEIF_ACT,
(TOP_CANSEIF_SUR/NB_TOT_REF) AS FREQ_TOP_CANSEIF_SUR,
(TOP_CVAOMI_IND/NB_TOT_REF) AS FREQ_TOP_CVAOMI_IND,
(TOP_CVAUTRE_IND/NB_TOT_REF) AS FREQ_TOP_CVAUTRE_IND,
(TOP_CVAVC_AIG/NB_TOT_REF) AS FREQ_TOP_CVAVC_AIG,
(TOP_CVAVC_SEQ/NB_TOT_REF) AS FREQ_TOP_CVAVC_SEQ,
(TOP_CVCORON_CHR/NB_TOT_REF) AS FREQ_TOP_CVCORON_CHR,
(TOP_CVEMBOL_AIG/NB_TOT_REF) AS FREQ_TOP_CVEMBOL_AIG,
(TOP_CVIC_AIG/NB_TOT_REF) AS FREQ_TOP_CVIC_AIG,
(TOP_CVIC_CHR/NB_TOT_REF) AS FREQ_TOP_CVIC_CHR,
(TOP_CVIDM_AIG/NB_TOT_REF) AS FREQ_TOP_CVIDM_AIG,
(TOP_CVTRRYC_IND/NB_TOT_REF) AS FREQ_TOP_CVTRRYC_IND,
(TOP_CVVALVE_IND/NB_TOT_REF) AS FREQ_TOP_CVVALVE_IND,
(TOP_FANTIHTA_MED/NB_TOT_REF) AS FREQ_TOP_FANTIHTA_MED,
(TOP_FDIABET_IND/NB_TOT_REF) AS FREQ_TOP_FDIABET_IND,
(TOP_FHYPOLI_MED/NB_TOT_REF) AS FREQ_TOP_FHYPOLI_MED,
(TOP_HFOIPAN_IND/NB_TOT_REF) AS FREQ_TOP_HFOIPAN_IND,
(TOP_IRAUTRE_IND/NB_TOT_REF) AS FREQ_TOP_IRAUTRE_IND,
(TOP_IRCRRCH_IND/NB_TOT_REF) AS FREQ_TOP_IRCRRCH_IND,
(TOP_IRHEMOP_IND/NB_TOT_REF) AS FREQ_TOP_IRHEMOP_IND,
(TOP_IRMMHER_IND/NB_TOT_REF) AS FREQ_TOP_IRMMHER_IND,
(TOP_IRMUCO_IND/NB_TOT_REF) AS FREQ_TOP_IRMUCO_IND,
(TOP_IRPOLYA_IND/NB_TOT_REF) AS FREQ_TOP_IRPOLYA_IND,
(TOP_IRSPONA_IND/NB_TOT_REF) AS FREQ_TOP_IRSPONA_IND,
(TOP_IRVIH_IND/NB_TOT_REF) AS FREQ_TOP_IRVIH_IND,
(TOP_MATERNI_IND/NB_TOT_REF) AS FREQ_TOP_MATERNI_IND,
(TOP_NAUTRES_IND/NB_TOT_REF) AS FREQ_TOP_NAUTRES_IND,
(TOP_NDEMENC_IND/NB_TOT_REF) AS FREQ_TOP_NDEMENC_IND,
(TOP_NEPILEP_IND/NB_TOT_REF) AS FREQ_TOP_NEPILEP_IND,
(TOP_NMYOMYA_IND/NB_TOT_REF) AS FREQ_TOP_NMYOMYA_IND,
(TOP_NPARAPL_IND/NB_TOT_REF) AS FREQ_TOP_NPARAPL_IND,
(TOP_NPARKIN_IND/NB_TOT_REF) AS FREQ_TOP_NPARKIN_IND,
(TOP_NSEPLAQ_IND/NB_TOT_REF) AS FREQ_TOP_NSEPLAQ_IND,
(TOP_PADDICT_IND/NB_TOT_REF) AS FREQ_TOP_PADDICT_IND,
(TOP_PANTIDE_MED/NB_TOT_REF) AS FREQ_TOP_PANTIDE_MED,
(TOP_PANXIOL_MED/NB_TOT_REF) AS FREQ_TOP_PANXIOL_MED,
(TOP_PDEPNEV_IND/NB_TOT_REF) AS FREQ_TOP_PDEPNEV_IND,
(TOP_PHYPNOT_MED/NB_TOT_REF) AS FREQ_TOP_PHYPNOT_MED,
(TOP_PNEUROL_MED/NB_TOT_REF) AS FREQ_TOP_PNEUROL_MED,
(TOP_PRETARD_IND/NB_TOT_REF) AS FREQ_TOP_PRETARD_IND,
(TOP_PSYAUTR_IND/NB_TOT_REF) AS FREQ_TOP_PSYAUTR_IND,
(TOP_PSYCHOS_IND/NB_TOT_REF) AS FREQ_TOP_PSYCHOS_IND,
(TOP_PTRENFA_IND/NB_TOT_REF) AS FREQ_TOP_PTRENFA_IND,
(TOP_RDIALYSE_IND/NB_TOT_REF) AS FREQ_TOP_RDIALYSE_IND,
(TOP_RTRANS_AIG/NB_TOT_REF) AS FREQ_TOP_RTRANS_AIG,
(TOP_RTRANS_CHR/NB_TOT_REF) AS FREQ_TOP_RTRANS_CHR,
(TOP_ALCOOL/NB_TOT_REF) AS FREQ_TOP_ALCOOL,
(TOP_TABAC/NB_TOT_REF) AS FREQ_TOP_TABAC,
(TOP_canabis/NB_TOT_REF) AS FREQ_TOP_canabis,
(TOP_ADDICT_AUT/NB_TOT_REF) AS FREQ_TOP_ADDICT_AUT,
(effpondREG_TOP_ABPCOIR_IND/NB_TOT_REF) AS FREQ_effpondREG_TOP_ABPCOIR_IND,
(effpondREG_TOP_CANAUTR_ACT/NB_TOT_REF) AS FREQ_effpondREG_TOP_CANAUTR_ACT,
(effpondREG_TOP_CANAUTR_SUR/NB_TOT_REF) AS FREQ_effpondREG_TOP_CANAUTR_SUR,
(effpondREG_TOP_CANCOLO_ACT/NB_TOT_REF) AS FREQ_effpondREG_TOP_CANCOLO_ACT,
(effpondREG_TOP_CANCOLO_SUR/NB_TOT_REF) AS FREQ_effpondREG_TOP_CANCOLO_SUR,
(effpondREG_TOP_CANPOUM_ACT/NB_TOT_REF) AS FREQ_effpondREG_TOP_CANPOUM_ACT,
(effpondREG_TOP_CANPOUM_SUR/NB_TOT_REF) AS FREQ_effpondREG_TOP_CANPOUM_SUR,
(effpondREG_TOP_CANPROS_ACT/NB_TOT_REF) AS FREQ_effpondREG_TOP_CANPROS_ACT,
(effpondREG_TOP_CANPROS_SUR/NB_TOT_REF) AS FREQ_effpondREG_TOP_CANPROS_SUR,
(effpondREG_TOP_CANSEIF_ACT/NB_TOT_REF) AS FREQ_effpondREG_TOP_CANSEIF_ACT,
(effpondREG_TOP_CANSEIF_SUR/NB_TOT_REF) AS FREQ_effpondREG_TOP_CANSEIF_SUR,
(effpondREG_TOP_CVAOMI_IND/NB_TOT_REF) AS FREQ_effpondREG_TOP_CVAOMI_IND,
(effpondREG_TOP_CVAUTRE_IND/NB_TOT_REF) AS FREQ_effpondREG_TOP_CVAUTRE_IND,
(effpondREG_TOP_CVAVC_AIG/NB_TOT_REF) AS FREQ_effpondREG_TOP_CVAVC_AIG,
(effpondREG_TOP_CVAVC_SEQ/NB_TOT_REF) AS FREQ_effpondREG_TOP_CVAVC_SEQ,
(effpondREG_TOP_CVCORON_CHR/NB_TOT_REF) AS FREQ_effpondREG_TOP_CVCORON_CHR,
(effpondREG_TOP_CVEMBOL_AIG/NB_TOT_REF) AS FREQ_effpondREG_TOP_CVEMBOL_AIG,
(effpondREG_TOP_CVIC_AIG/NB_TOT_REF) AS FREQ_effpondREG_TOP_CVIC_AIG,
(effpondREG_TOP_CVIC_CHR/NB_TOT_REF) AS FREQ_effpondREG_TOP_CVIC_CHR,
(effpondREG_TOP_CVIDM_AIG/NB_TOT_REF) AS FREQ_effpondREG_TOP_CVIDM_AIG,
(effpondREG_TOP_CVTRRYC_IND/NB_TOT_REF) AS FREQ_effpondREG_TOP_CVTRRYC_IND,
(effpondREG_TOP_CVVALVE_IND/NB_TOT_REF) AS FREQ_effpondREG_TOP_CVVALVE_IND,
(effpondREG_TOP_FANTIHTA_MED/NB_TOT_REF) AS FREQ_effpondREG_TOP_FANTIHTA_MED,
(effpondREG_TOP_FDIABET_IND/NB_TOT_REF) AS FREQ_effpondREG_TOP_FDIABET_IND,
(effpondREG_TOP_FHYPOLI_MED/NB_TOT_REF) AS FREQ_effpondREG_TOP_FHYPOLI_MED,
(effpondREG_TOP_HFOIPAN_IND/NB_TOT_REF) AS FREQ_effpondREG_TOP_HFOIPAN_IND,
(effpondREG_TOP_IRAUTRE_IND/NB_TOT_REF) AS FREQ_effpondREG_TOP_IRAUTRE_IND,
(effpondREG_TOP_IRCRRCH_IND/NB_TOT_REF) AS FREQ_effpondREG_TOP_IRCRRCH_IND,
(effpondREG_TOP_IRHEMOP_IND/NB_TOT_REF) AS FREQ_effpondREG_TOP_IRHEMOP_IND,
(effpondREG_TOP_IRMMHER_IND/NB_TOT_REF) AS FREQ_effpondREG_TOP_IRMMHER_IND,
(effpondREG_TOP_IRMUCO_IND/NB_TOT_REF) AS FREQ_effpondREG_TOP_IRMUCO_IND,
(effpondREG_TOP_IRPOLYA_IND/NB_TOT_REF) AS FREQ_effpondREG_TOP_IRPOLYA_IND,
(effpondREG_TOP_IRSPONA_IND/NB_TOT_REF) AS FREQ_effpondREG_TOP_IRSPONA_IND,
(effpondREG_TOP_IRVIH_IND/NB_TOT_REF) AS FREQ_effpondREG_TOP_IRVIH_IND,
(effpondREG_TOP_MATERNI_IND/NB_TOT_REF) AS FREQ_effpondREG_TOP_MATERNI_IND,
(effpondREG_TOP_NAUTRES_IND/NB_TOT_REF) AS FREQ_effpondREG_TOP_NAUTRES_IND,
(effpondREG_TOP_NDEMENC_IND/NB_TOT_REF) AS FREQ_effpondREG_TOP_NDEMENC_IND,
(effpondREG_TOP_NEPILEP_IND/NB_TOT_REF) AS FREQ_effpondREG_TOP_NEPILEP_IND,
(effpondREG_TOP_NMYOMYA_IND/NB_TOT_REF) AS FREQ_effpondREG_TOP_NMYOMYA_IND,
(effpondREG_TOP_NPARAPL_IND/NB_TOT_REF) AS FREQ_effpondREG_TOP_NPARAPL_IND,
(effpondREG_TOP_NPARKIN_IND/NB_TOT_REF) AS FREQ_effpondREG_TOP_NPARKIN_IND,
(effpondREG_TOP_NSEPLAQ_IND/NB_TOT_REF) AS FREQ_effpondREG_TOP_NSEPLAQ_IND,
(effpondREG_TOP_PADDICT_IND/NB_TOT_REF) AS FREQ_effpondREG_TOP_PADDICT_IND,
(effpondREG_TOP_PANTIDE_MED/NB_TOT_REF) AS FREQ_effpondREG_TOP_PANTIDE_MED,
(effpondREG_TOP_PANXIOL_MED/NB_TOT_REF) AS FREQ_effpondREG_TOP_PANXIOL_MED,
(effpondREG_TOP_PDEPNEV_IND/NB_TOT_REF) AS FREQ_effpondREG_TOP_PDEPNEV_IND,
(effpondREG_TOP_PHYPNOT_MED/NB_TOT_REF) AS FREQ_effpondREG_TOP_PHYPNOT_MED,
(effpondREG_TOP_PNEUROL_MED/NB_TOT_REF) AS FREQ_effpondREG_TOP_PNEUROL_MED,
(effpondREG_TOP_PRETARD_IND/NB_TOT_REF) AS FREQ_effpondREG_TOP_PRETARD_IND,
(effpondREG_TOP_PSYAUTR_IND/NB_TOT_REF) AS FREQ_effpondREG_TOP_PSYAUTR_IND,
(effpondREG_TOP_PSYCHOS_IND/NB_TOT_REF) AS FREQ_effpondREG_TOP_PSYCHOS_IND,
(effpondREG_TOP_PTRENFA_IND/NB_TOT_REF) AS FREQ_effpondREG_TOP_PTRENFA_IND,
(effpondREG_TOP_RDIALYSE_IND/NB_TOT_REF) AS FREQ_effpondREG_TOP_RDIALYSE_IND,
(effpondREG_TOP_RTRANS_AIG/NB_TOT_REF) AS FREQ_effpondREG_TOP_RTRANS_AIG,
(effpondREG_TOP_RTRANS_CHR/NB_TOT_REF) AS FREQ_effpondREG_TOP_RTRANS_CHR,
(effpondREG_TOP_ALCOOL/NB_TOT_REF) AS FREQ_effpondREG_TOP_ALCOOL,
(effpondREG_TOP_TABAC/NB_TOT_REF) AS FREQ_effpondREG_TOP_TABAC,
(effpondREG_TOP_canabis/NB_TOT_REF) AS FREQ_effpondREG_TOP_canabis,
(effpondREG_TOP_ADDICT_AUT/NB_TOT_REF) AS FREQ_effpondREG_TOP_ADDICT_AUT,
(effpondNAT_TOP_ABPCOIR_IND/NB_TOT_REF) AS FREQ_effpondNAT_TOP_ABPCOIR_IND,
(effpondNAT_TOP_CANAUTR_ACT/NB_TOT_REF) AS FREQ_effpondNAT_TOP_CANAUTR_ACT,
(effpondNAT_TOP_CANAUTR_SUR/NB_TOT_REF) AS FREQ_effpondNAT_TOP_CANAUTR_SUR,
(effpondNAT_TOP_CANCOLO_ACT/NB_TOT_REF) AS FREQ_effpondNAT_TOP_CANCOLO_ACT,
(effpondNAT_TOP_CANCOLO_SUR/NB_TOT_REF) AS FREQ_effpondNAT_TOP_CANCOLO_SUR,
(effpondNAT_TOP_CANPOUM_ACT/NB_TOT_REF) AS FREQ_effpondNAT_TOP_CANPOUM_ACT,
(effpondNAT_TOP_CANPOUM_SUR/NB_TOT_REF) AS FREQ_effpondNAT_TOP_CANPOUM_SUR,
(effpondNAT_TOP_CANPROS_ACT/NB_TOT_REF) AS FREQ_effpondNAT_TOP_CANPROS_ACT,
(effpondNAT_TOP_CANPROS_SUR/NB_TOT_REF) AS FREQ_effpondNAT_TOP_CANPROS_SUR,
(effpondNAT_TOP_CANSEIF_ACT/NB_TOT_REF) AS FREQ_effpondNAT_TOP_CANSEIF_ACT,
(effpondNAT_TOP_CANSEIF_SUR/NB_TOT_REF) AS FREQ_effpondNAT_TOP_CANSEIF_SUR,
(effpondNAT_TOP_CVAOMI_IND/NB_TOT_REF) AS FREQ_effpondNAT_TOP_CVAOMI_IND,
(effpondNAT_TOP_CVAUTRE_IND/NB_TOT_REF) AS FREQ_effpondNAT_TOP_CVAUTRE_IND,
(effpondNAT_TOP_CVAVC_AIG/NB_TOT_REF) AS FREQ_effpondNAT_TOP_CVAVC_AIG,
(effpondNAT_TOP_CVAVC_SEQ/NB_TOT_REF) AS FREQ_effpondNAT_TOP_CVAVC_SEQ,
(effpondNAT_TOP_CVCORON_CHR/NB_TOT_REF) AS FREQ_effpondNAT_TOP_CVCORON_CHR,
(effpondNAT_TOP_CVEMBOL_AIG/NB_TOT_REF) AS FREQ_effpondNAT_TOP_CVEMBOL_AIG,
(effpondNAT_TOP_CVIC_AIG/NB_TOT_REF) AS FREQ_effpondNAT_TOP_CVIC_AIG,
(effpondNAT_TOP_CVIC_CHR/NB_TOT_REF) AS FREQ_effpondNAT_TOP_CVIC_CHR,
(effpondNAT_TOP_CVIDM_AIG/NB_TOT_REF) AS FREQ_effpondNAT_TOP_CVIDM_AIG,
(effpondNAT_TOP_CVTRRYC_IND/NB_TOT_REF) AS FREQ_effpondNAT_TOP_CVTRRYC_IND,
(effpondNAT_TOP_CVVALVE_IND/NB_TOT_REF) AS FREQ_effpondNAT_TOP_CVVALVE_IND,
(effpondNAT_TOP_FANTIHTA_MED/NB_TOT_REF) AS FREQ_effpondNAT_TOP_FANTIHTA_MED,
(effpondNAT_TOP_FDIABET_IND/NB_TOT_REF) AS FREQ_effpondNAT_TOP_FDIABET_IND,
(effpondNAT_TOP_FHYPOLI_MED/NB_TOT_REF) AS FREQ_effpondNAT_TOP_FHYPOLI_MED,
(effpondNAT_TOP_HFOIPAN_IND/NB_TOT_REF) AS FREQ_effpondNAT_TOP_HFOIPAN_IND,
(effpondNAT_TOP_IRAUTRE_IND/NB_TOT_REF) AS FREQ_effpondNAT_TOP_IRAUTRE_IND,
(effpondNAT_TOP_IRCRRCH_IND/NB_TOT_REF) AS FREQ_effpondNAT_TOP_IRCRRCH_IND,
(effpondNAT_TOP_IRHEMOP_IND/NB_TOT_REF) AS FREQ_effpondNAT_TOP_IRHEMOP_IND,
(effpondNAT_TOP_IRMMHER_IND/NB_TOT_REF) AS FREQ_effpondNAT_TOP_IRMMHER_IND,
(effpondNAT_TOP_IRMUCO_IND/NB_TOT_REF) AS FREQ_effpondNAT_TOP_IRMUCO_IND,
(effpondNAT_TOP_IRPOLYA_IND/NB_TOT_REF) AS FREQ_effpondNAT_TOP_IRPOLYA_IND,
(effpondNAT_TOP_IRSPONA_IND/NB_TOT_REF) AS FREQ_effpondNAT_TOP_IRSPONA_IND,
(effpondNAT_TOP_IRVIH_IND/NB_TOT_REF) AS FREQ_effpondNAT_TOP_IRVIH_IND,
(effpondNAT_TOP_MATERNI_IND/NB_TOT_REF) AS FREQ_effpondNAT_TOP_MATERNI_IND,
(effpondNAT_TOP_NAUTRES_IND/NB_TOT_REF) AS FREQ_effpondNAT_TOP_NAUTRES_IND,
(effpondNAT_TOP_NDEMENC_IND/NB_TOT_REF) AS FREQ_effpondNAT_TOP_NDEMENC_IND,
(effpondNAT_TOP_NEPILEP_IND/NB_TOT_REF) AS FREQ_effpondNAT_TOP_NEPILEP_IND,
(effpondNAT_TOP_NMYOMYA_IND/NB_TOT_REF) AS FREQ_effpondNAT_TOP_NMYOMYA_IND,
(effpondNAT_TOP_NPARAPL_IND/NB_TOT_REF) AS FREQ_effpondNAT_TOP_NPARAPL_IND,
(effpondNAT_TOP_NPARKIN_IND/NB_TOT_REF) AS FREQ_effpondNAT_TOP_NPARKIN_IND,
(effpondNAT_TOP_NSEPLAQ_IND/NB_TOT_REF) AS FREQ_effpondNAT_TOP_NSEPLAQ_IND,
(effpondNAT_TOP_PADDICT_IND/NB_TOT_REF) AS FREQ_effpondNAT_TOP_PADDICT_IND,
(effpondNAT_TOP_PANTIDE_MED/NB_TOT_REF) AS FREQ_effpondNAT_TOP_PANTIDE_MED,
(effpondNAT_TOP_PANXIOL_MED/NB_TOT_REF) AS FREQ_effpondNAT_TOP_PANXIOL_MED,
(effpondNAT_TOP_PDEPNEV_IND/NB_TOT_REF) AS FREQ_effpondNAT_TOP_PDEPNEV_IND,
(effpondNAT_TOP_PHYPNOT_MED/NB_TOT_REF) AS FREQ_effpondNAT_TOP_PHYPNOT_MED,
(effpondNAT_TOP_PNEUROL_MED/NB_TOT_REF) AS FREQ_effpondNAT_TOP_PNEUROL_MED,
(effpondNAT_TOP_PRETARD_IND/NB_TOT_REF) AS FREQ_effpondNAT_TOP_PRETARD_IND,
(effpondNAT_TOP_PSYAUTR_IND/NB_TOT_REF) AS FREQ_effpondNAT_TOP_PSYAUTR_IND,
(effpondNAT_TOP_PSYCHOS_IND/NB_TOT_REF) AS FREQ_effpondNAT_TOP_PSYCHOS_IND,
(effpondNAT_TOP_PTRENFA_IND/NB_TOT_REF) AS FREQ_effpondNAT_TOP_PTRENFA_IND,
(effpondNAT_TOP_RDIALYSE_IND/NB_TOT_REF) AS FREQ_effpondNAT_TOP_RDIALYSE_IND,
(effpondNAT_TOP_RTRANS_AIG/NB_TOT_REF) AS FREQ_effpondNAT_TOP_RTRANS_AIG,
(effpondNAT_TOP_RTRANS_CHR/NB_TOT_REF) AS FREQ_effpondNAT_TOP_RTRANS_CHR,
(effpondNAT_TOP_ALCOOL/NB_TOT_REF) AS FREQ_effpondNAT_TOP_ALCOOL,
(effpondNAT_TOP_TABAC/NB_TOT_REF) AS FREQ_effpondNAT_TOP_TABAC,
(effpondNAT_TOP_canabis/NB_TOT_REF) AS FREQ_effpondNAT_TOP_canabis,
(effpondNAT_TOP_ADDICT_AUT/NB_TOT_REF) AS FREQ_effpondNAT_TOP_ADDICT_AUT
from SASDATA1.reg
group by 1;
quit;



proc sql;
create table nat_sortie as select 
"nat",
(TOP_ABPCOIR_IND/NB_TOT_REF) AS FREQ_TOP_ABPCOIR_IND,
(TOP_CANAUTR_ACT/NB_TOT_REF) AS FREQ_TOP_CANAUTR_ACT,
(TOP_CANAUTR_SUR/NB_TOT_REF) AS FREQ_TOP_CANAUTR_SUR,
(TOP_CANCOLO_ACT/NB_TOT_REF) AS FREQ_TOP_CANCOLO_ACT,
(TOP_CANCOLO_SUR/NB_TOT_REF) AS FREQ_TOP_CANCOLO_SUR,
(TOP_CANPOUM_ACT/NB_TOT_REF) AS FREQ_TOP_CANPOUM_ACT,
(TOP_CANPOUM_SUR/NB_TOT_REF) AS FREQ_TOP_CANPOUM_SUR,
(TOP_CANPROS_ACT/NB_TOT_REF) AS FREQ_TOP_CANPROS_ACT,
(TOP_CANPROS_SUR/NB_TOT_REF) AS FREQ_TOP_CANPROS_SUR,
(TOP_CANSEIF_ACT/NB_TOT_REF) AS FREQ_TOP_CANSEIF_ACT,
(TOP_CANSEIF_SUR/NB_TOT_REF) AS FREQ_TOP_CANSEIF_SUR,
(TOP_CVAOMI_IND/NB_TOT_REF) AS FREQ_TOP_CVAOMI_IND,
(TOP_CVAUTRE_IND/NB_TOT_REF) AS FREQ_TOP_CVAUTRE_IND,
(TOP_CVAVC_AIG/NB_TOT_REF) AS FREQ_TOP_CVAVC_AIG,
(TOP_CVAVC_SEQ/NB_TOT_REF) AS FREQ_TOP_CVAVC_SEQ,
(TOP_CVCORON_CHR/NB_TOT_REF) AS FREQ_TOP_CVCORON_CHR,
(TOP_CVEMBOL_AIG/NB_TOT_REF) AS FREQ_TOP_CVEMBOL_AIG,
(TOP_CVIC_AIG/NB_TOT_REF) AS FREQ_TOP_CVIC_AIG,
(TOP_CVIC_CHR/NB_TOT_REF) AS FREQ_TOP_CVIC_CHR,
(TOP_CVIDM_AIG/NB_TOT_REF) AS FREQ_TOP_CVIDM_AIG,
(TOP_CVTRRYC_IND/NB_TOT_REF) AS FREQ_TOP_CVTRRYC_IND,
(TOP_CVVALVE_IND/NB_TOT_REF) AS FREQ_TOP_CVVALVE_IND,
(TOP_FANTIHTA_MED/NB_TOT_REF) AS FREQ_TOP_FANTIHTA_MED,
(TOP_FDIABET_IND/NB_TOT_REF) AS FREQ_TOP_FDIABET_IND,
(TOP_FHYPOLI_MED/NB_TOT_REF) AS FREQ_TOP_FHYPOLI_MED,
(TOP_HFOIPAN_IND/NB_TOT_REF) AS FREQ_TOP_HFOIPAN_IND,
(TOP_IRAUTRE_IND/NB_TOT_REF) AS FREQ_TOP_IRAUTRE_IND,
(TOP_IRCRRCH_IND/NB_TOT_REF) AS FREQ_TOP_IRCRRCH_IND,
(TOP_IRHEMOP_IND/NB_TOT_REF) AS FREQ_TOP_IRHEMOP_IND,
(TOP_IRMMHER_IND/NB_TOT_REF) AS FREQ_TOP_IRMMHER_IND,
(TOP_IRMUCO_IND/NB_TOT_REF) AS FREQ_TOP_IRMUCO_IND,
(TOP_IRPOLYA_IND/NB_TOT_REF) AS FREQ_TOP_IRPOLYA_IND,
(TOP_IRSPONA_IND/NB_TOT_REF) AS FREQ_TOP_IRSPONA_IND,
(TOP_IRVIH_IND/NB_TOT_REF) AS FREQ_TOP_IRVIH_IND,
(TOP_MATERNI_IND/NB_TOT_REF) AS FREQ_TOP_MATERNI_IND,
(TOP_NAUTRES_IND/NB_TOT_REF) AS FREQ_TOP_NAUTRES_IND,
(TOP_NDEMENC_IND/NB_TOT_REF) AS FREQ_TOP_NDEMENC_IND,
(TOP_NEPILEP_IND/NB_TOT_REF) AS FREQ_TOP_NEPILEP_IND,
(TOP_NMYOMYA_IND/NB_TOT_REF) AS FREQ_TOP_NMYOMYA_IND,
(TOP_NPARAPL_IND/NB_TOT_REF) AS FREQ_TOP_NPARAPL_IND,
(TOP_NPARKIN_IND/NB_TOT_REF) AS FREQ_TOP_NPARKIN_IND,
(TOP_NSEPLAQ_IND/NB_TOT_REF) AS FREQ_TOP_NSEPLAQ_IND,
(TOP_PADDICT_IND/NB_TOT_REF) AS FREQ_TOP_PADDICT_IND,
(TOP_PANTIDE_MED/NB_TOT_REF) AS FREQ_TOP_PANTIDE_MED,
(TOP_PANXIOL_MED/NB_TOT_REF) AS FREQ_TOP_PANXIOL_MED,
(TOP_PDEPNEV_IND/NB_TOT_REF) AS FREQ_TOP_PDEPNEV_IND,
(TOP_PHYPNOT_MED/NB_TOT_REF) AS FREQ_TOP_PHYPNOT_MED,
(TOP_PNEUROL_MED/NB_TOT_REF) AS FREQ_TOP_PNEUROL_MED,
(TOP_PRETARD_IND/NB_TOT_REF) AS FREQ_TOP_PRETARD_IND,
(TOP_PSYAUTR_IND/NB_TOT_REF) AS FREQ_TOP_PSYAUTR_IND,
(TOP_PSYCHOS_IND/NB_TOT_REF) AS FREQ_TOP_PSYCHOS_IND,
(TOP_PTRENFA_IND/NB_TOT_REF) AS FREQ_TOP_PTRENFA_IND,
(TOP_RDIALYSE_IND/NB_TOT_REF) AS FREQ_TOP_RDIALYSE_IND,
(TOP_RTRANS_AIG/NB_TOT_REF) AS FREQ_TOP_RTRANS_AIG,
(TOP_RTRANS_CHR/NB_TOT_REF) AS FREQ_TOP_RTRANS_CHR,
(TOP_ALCOOL/NB_TOT_REF) AS FREQ_TOP_ALCOOL,
(TOP_TABAC/NB_TOT_REF) AS FREQ_TOP_TABAC,
(TOP_canabis/NB_TOT_REF) AS FREQ_TOP_canabis,
(TOP_ADDICT_AUT/NB_TOT_REF) AS FREQ_TOP_ADDICT_AUT,
(effpondREG_TOP_ABPCOIR_IND/NB_TOT_REF) AS FREQ_effpondREG_TOP_ABPCOIR_IND,
(effpondREG_TOP_CANAUTR_ACT/NB_TOT_REF) AS FREQ_effpondREG_TOP_CANAUTR_ACT,
(effpondREG_TOP_CANAUTR_SUR/NB_TOT_REF) AS FREQ_effpondREG_TOP_CANAUTR_SUR,
(effpondREG_TOP_CANCOLO_ACT/NB_TOT_REF) AS FREQ_effpondREG_TOP_CANCOLO_ACT,
(effpondREG_TOP_CANCOLO_SUR/NB_TOT_REF) AS FREQ_effpondREG_TOP_CANCOLO_SUR,
(effpondREG_TOP_CANPOUM_ACT/NB_TOT_REF) AS FREQ_effpondREG_TOP_CANPOUM_ACT,
(effpondREG_TOP_CANPOUM_SUR/NB_TOT_REF) AS FREQ_effpondREG_TOP_CANPOUM_SUR,
(effpondREG_TOP_CANPROS_ACT/NB_TOT_REF) AS FREQ_effpondREG_TOP_CANPROS_ACT,
(effpondREG_TOP_CANPROS_SUR/NB_TOT_REF) AS FREQ_effpondREG_TOP_CANPROS_SUR,
(effpondREG_TOP_CANSEIF_ACT/NB_TOT_REF) AS FREQ_effpondREG_TOP_CANSEIF_ACT,
(effpondREG_TOP_CANSEIF_SUR/NB_TOT_REF) AS FREQ_effpondREG_TOP_CANSEIF_SUR,
(effpondREG_TOP_CVAOMI_IND/NB_TOT_REF) AS FREQ_effpondREG_TOP_CVAOMI_IND,
(effpondREG_TOP_CVAUTRE_IND/NB_TOT_REF) AS FREQ_effpondREG_TOP_CVAUTRE_IND,
(effpondREG_TOP_CVAVC_AIG/NB_TOT_REF) AS FREQ_effpondREG_TOP_CVAVC_AIG,
(effpondREG_TOP_CVAVC_SEQ/NB_TOT_REF) AS FREQ_effpondREG_TOP_CVAVC_SEQ,
(effpondREG_TOP_CVCORON_CHR/NB_TOT_REF) AS FREQ_effpondREG_TOP_CVCORON_CHR,
(effpondREG_TOP_CVEMBOL_AIG/NB_TOT_REF) AS FREQ_effpondREG_TOP_CVEMBOL_AIG,
(effpondREG_TOP_CVIC_AIG/NB_TOT_REF) AS FREQ_effpondREG_TOP_CVIC_AIG,
(effpondREG_TOP_CVIC_CHR/NB_TOT_REF) AS FREQ_effpondREG_TOP_CVIC_CHR,
(effpondREG_TOP_CVIDM_AIG/NB_TOT_REF) AS FREQ_effpondREG_TOP_CVIDM_AIG,
(effpondREG_TOP_CVTRRYC_IND/NB_TOT_REF) AS FREQ_effpondREG_TOP_CVTRRYC_IND,
(effpondREG_TOP_CVVALVE_IND/NB_TOT_REF) AS FREQ_effpondREG_TOP_CVVALVE_IND,
(effpondREG_TOP_FANTIHTA_MED/NB_TOT_REF) AS FREQ_effpondREG_TOP_FANTIHTA_MED,
(effpondREG_TOP_FDIABET_IND/NB_TOT_REF) AS FREQ_effpondREG_TOP_FDIABET_IND,
(effpondREG_TOP_FHYPOLI_MED/NB_TOT_REF) AS FREQ_effpondREG_TOP_FHYPOLI_MED,
(effpondREG_TOP_HFOIPAN_IND/NB_TOT_REF) AS FREQ_effpondREG_TOP_HFOIPAN_IND,
(effpondREG_TOP_IRAUTRE_IND/NB_TOT_REF) AS FREQ_effpondREG_TOP_IRAUTRE_IND,
(effpondREG_TOP_IRCRRCH_IND/NB_TOT_REF) AS FREQ_effpondREG_TOP_IRCRRCH_IND,
(effpondREG_TOP_IRHEMOP_IND/NB_TOT_REF) AS FREQ_effpondREG_TOP_IRHEMOP_IND,
(effpondREG_TOP_IRMMHER_IND/NB_TOT_REF) AS FREQ_effpondREG_TOP_IRMMHER_IND,
(effpondREG_TOP_IRMUCO_IND/NB_TOT_REF) AS FREQ_effpondREG_TOP_IRMUCO_IND,
(effpondREG_TOP_IRPOLYA_IND/NB_TOT_REF) AS FREQ_effpondREG_TOP_IRPOLYA_IND,
(effpondREG_TOP_IRSPONA_IND/NB_TOT_REF) AS FREQ_effpondREG_TOP_IRSPONA_IND,
(effpondREG_TOP_IRVIH_IND/NB_TOT_REF) AS FREQ_effpondREG_TOP_IRVIH_IND,
(effpondREG_TOP_MATERNI_IND/NB_TOT_REF) AS FREQ_effpondREG_TOP_MATERNI_IND,
(effpondREG_TOP_NAUTRES_IND/NB_TOT_REF) AS FREQ_effpondREG_TOP_NAUTRES_IND,
(effpondREG_TOP_NDEMENC_IND/NB_TOT_REF) AS FREQ_effpondREG_TOP_NDEMENC_IND,
(effpondREG_TOP_NEPILEP_IND/NB_TOT_REF) AS FREQ_effpondREG_TOP_NEPILEP_IND,
(effpondREG_TOP_NMYOMYA_IND/NB_TOT_REF) AS FREQ_effpondREG_TOP_NMYOMYA_IND,
(effpondREG_TOP_NPARAPL_IND/NB_TOT_REF) AS FREQ_effpondREG_TOP_NPARAPL_IND,
(effpondREG_TOP_NPARKIN_IND/NB_TOT_REF) AS FREQ_effpondREG_TOP_NPARKIN_IND,
(effpondREG_TOP_NSEPLAQ_IND/NB_TOT_REF) AS FREQ_effpondREG_TOP_NSEPLAQ_IND,
(effpondREG_TOP_PADDICT_IND/NB_TOT_REF) AS FREQ_effpondREG_TOP_PADDICT_IND,
(effpondREG_TOP_PANTIDE_MED/NB_TOT_REF) AS FREQ_effpondREG_TOP_PANTIDE_MED,
(effpondREG_TOP_PANXIOL_MED/NB_TOT_REF) AS FREQ_effpondREG_TOP_PANXIOL_MED,
(effpondREG_TOP_PDEPNEV_IND/NB_TOT_REF) AS FREQ_effpondREG_TOP_PDEPNEV_IND,
(effpondREG_TOP_PHYPNOT_MED/NB_TOT_REF) AS FREQ_effpondREG_TOP_PHYPNOT_MED,
(effpondREG_TOP_PNEUROL_MED/NB_TOT_REF) AS FREQ_effpondREG_TOP_PNEUROL_MED,
(effpondREG_TOP_PRETARD_IND/NB_TOT_REF) AS FREQ_effpondREG_TOP_PRETARD_IND,
(effpondREG_TOP_PSYAUTR_IND/NB_TOT_REF) AS FREQ_effpondREG_TOP_PSYAUTR_IND,
(effpondREG_TOP_PSYCHOS_IND/NB_TOT_REF) AS FREQ_effpondREG_TOP_PSYCHOS_IND,
(effpondREG_TOP_PTRENFA_IND/NB_TOT_REF) AS FREQ_effpondREG_TOP_PTRENFA_IND,
(effpondREG_TOP_RDIALYSE_IND/NB_TOT_REF) AS FREQ_effpondREG_TOP_RDIALYSE_IND,
(effpondREG_TOP_RTRANS_AIG/NB_TOT_REF) AS FREQ_effpondREG_TOP_RTRANS_AIG,
(effpondREG_TOP_RTRANS_CHR/NB_TOT_REF) AS FREQ_effpondREG_TOP_RTRANS_CHR,
(effpondREG_TOP_ALCOOL/NB_TOT_REF) AS FREQ_effpondREG_TOP_ALCOOL,
(effpondREG_TOP_TABAC/NB_TOT_REF) AS FREQ_effpondREG_TOP_TABAC,
(effpondREG_TOP_canabis/NB_TOT_REF) AS FREQ_effpondREG_TOP_canabis,
(effpondREG_TOP_ADDICT_AUT/NB_TOT_REF) AS FREQ_effpondREG_TOP_ADDICT_AUT,
(effpondNAT_TOP_ABPCOIR_IND/NB_TOT_REF) AS FREQ_effpondNAT_TOP_ABPCOIR_IND,
(effpondNAT_TOP_CANAUTR_ACT/NB_TOT_REF) AS FREQ_effpondNAT_TOP_CANAUTR_ACT,
(effpondNAT_TOP_CANAUTR_SUR/NB_TOT_REF) AS FREQ_effpondNAT_TOP_CANAUTR_SUR,
(effpondNAT_TOP_CANCOLO_ACT/NB_TOT_REF) AS FREQ_effpondNAT_TOP_CANCOLO_ACT,
(effpondNAT_TOP_CANCOLO_SUR/NB_TOT_REF) AS FREQ_effpondNAT_TOP_CANCOLO_SUR,
(effpondNAT_TOP_CANPOUM_ACT/NB_TOT_REF) AS FREQ_effpondNAT_TOP_CANPOUM_ACT,
(effpondNAT_TOP_CANPOUM_SUR/NB_TOT_REF) AS FREQ_effpondNAT_TOP_CANPOUM_SUR,
(effpondNAT_TOP_CANPROS_ACT/NB_TOT_REF) AS FREQ_effpondNAT_TOP_CANPROS_ACT,
(effpondNAT_TOP_CANPROS_SUR/NB_TOT_REF) AS FREQ_effpondNAT_TOP_CANPROS_SUR,
(effpondNAT_TOP_CANSEIF_ACT/NB_TOT_REF) AS FREQ_effpondNAT_TOP_CANSEIF_ACT,
(effpondNAT_TOP_CANSEIF_SUR/NB_TOT_REF) AS FREQ_effpondNAT_TOP_CANSEIF_SUR,
(effpondNAT_TOP_CVAOMI_IND/NB_TOT_REF) AS FREQ_effpondNAT_TOP_CVAOMI_IND,
(effpondNAT_TOP_CVAUTRE_IND/NB_TOT_REF) AS FREQ_effpondNAT_TOP_CVAUTRE_IND,
(effpondNAT_TOP_CVAVC_AIG/NB_TOT_REF) AS FREQ_effpondNAT_TOP_CVAVC_AIG,
(effpondNAT_TOP_CVAVC_SEQ/NB_TOT_REF) AS FREQ_effpondNAT_TOP_CVAVC_SEQ,
(effpondNAT_TOP_CVCORON_CHR/NB_TOT_REF) AS FREQ_effpondNAT_TOP_CVCORON_CHR,
(effpondNAT_TOP_CVEMBOL_AIG/NB_TOT_REF) AS FREQ_effpondNAT_TOP_CVEMBOL_AIG,
(effpondNAT_TOP_CVIC_AIG/NB_TOT_REF) AS FREQ_effpondNAT_TOP_CVIC_AIG,
(effpondNAT_TOP_CVIC_CHR/NB_TOT_REF) AS FREQ_effpondNAT_TOP_CVIC_CHR,
(effpondNAT_TOP_CVIDM_AIG/NB_TOT_REF) AS FREQ_effpondNAT_TOP_CVIDM_AIG,
(effpondNAT_TOP_CVTRRYC_IND/NB_TOT_REF) AS FREQ_effpondNAT_TOP_CVTRRYC_IND,
(effpondNAT_TOP_CVVALVE_IND/NB_TOT_REF) AS FREQ_effpondNAT_TOP_CVVALVE_IND,
(effpondNAT_TOP_FANTIHTA_MED/NB_TOT_REF) AS FREQ_effpondNAT_TOP_FANTIHTA_MED,
(effpondNAT_TOP_FDIABET_IND/NB_TOT_REF) AS FREQ_effpondNAT_TOP_FDIABET_IND,
(effpondNAT_TOP_FHYPOLI_MED/NB_TOT_REF) AS FREQ_effpondNAT_TOP_FHYPOLI_MED,
(effpondNAT_TOP_HFOIPAN_IND/NB_TOT_REF) AS FREQ_effpondNAT_TOP_HFOIPAN_IND,
(effpondNAT_TOP_IRAUTRE_IND/NB_TOT_REF) AS FREQ_effpondNAT_TOP_IRAUTRE_IND,
(effpondNAT_TOP_IRCRRCH_IND/NB_TOT_REF) AS FREQ_effpondNAT_TOP_IRCRRCH_IND,
(effpondNAT_TOP_IRHEMOP_IND/NB_TOT_REF) AS FREQ_effpondNAT_TOP_IRHEMOP_IND,
(effpondNAT_TOP_IRMMHER_IND/NB_TOT_REF) AS FREQ_effpondNAT_TOP_IRMMHER_IND,
(effpondNAT_TOP_IRMUCO_IND/NB_TOT_REF) AS FREQ_effpondNAT_TOP_IRMUCO_IND,
(effpondNAT_TOP_IRPOLYA_IND/NB_TOT_REF) AS FREQ_effpondNAT_TOP_IRPOLYA_IND,
(effpondNAT_TOP_IRSPONA_IND/NB_TOT_REF) AS FREQ_effpondNAT_TOP_IRSPONA_IND,
(effpondNAT_TOP_IRVIH_IND/NB_TOT_REF) AS FREQ_effpondNAT_TOP_IRVIH_IND,
(effpondNAT_TOP_MATERNI_IND/NB_TOT_REF) AS FREQ_effpondNAT_TOP_MATERNI_IND,
(effpondNAT_TOP_NAUTRES_IND/NB_TOT_REF) AS FREQ_effpondNAT_TOP_NAUTRES_IND,
(effpondNAT_TOP_NDEMENC_IND/NB_TOT_REF) AS FREQ_effpondNAT_TOP_NDEMENC_IND,
(effpondNAT_TOP_NEPILEP_IND/NB_TOT_REF) AS FREQ_effpondNAT_TOP_NEPILEP_IND,
(effpondNAT_TOP_NMYOMYA_IND/NB_TOT_REF) AS FREQ_effpondNAT_TOP_NMYOMYA_IND,
(effpondNAT_TOP_NPARAPL_IND/NB_TOT_REF) AS FREQ_effpondNAT_TOP_NPARAPL_IND,
(effpondNAT_TOP_NPARKIN_IND/NB_TOT_REF) AS FREQ_effpondNAT_TOP_NPARKIN_IND,
(effpondNAT_TOP_NSEPLAQ_IND/NB_TOT_REF) AS FREQ_effpondNAT_TOP_NSEPLAQ_IND,
(effpondNAT_TOP_PADDICT_IND/NB_TOT_REF) AS FREQ_effpondNAT_TOP_PADDICT_IND,
(effpondNAT_TOP_PANTIDE_MED/NB_TOT_REF) AS FREQ_effpondNAT_TOP_PANTIDE_MED,
(effpondNAT_TOP_PANXIOL_MED/NB_TOT_REF) AS FREQ_effpondNAT_TOP_PANXIOL_MED,
(effpondNAT_TOP_PDEPNEV_IND/NB_TOT_REF) AS FREQ_effpondNAT_TOP_PDEPNEV_IND,
(effpondNAT_TOP_PHYPNOT_MED/NB_TOT_REF) AS FREQ_effpondNAT_TOP_PHYPNOT_MED,
(effpondNAT_TOP_PNEUROL_MED/NB_TOT_REF) AS FREQ_effpondNAT_TOP_PNEUROL_MED,
(effpondNAT_TOP_PRETARD_IND/NB_TOT_REF) AS FREQ_effpondNAT_TOP_PRETARD_IND,
(effpondNAT_TOP_PSYAUTR_IND/NB_TOT_REF) AS FREQ_effpondNAT_TOP_PSYAUTR_IND,
(effpondNAT_TOP_PSYCHOS_IND/NB_TOT_REF) AS FREQ_effpondNAT_TOP_PSYCHOS_IND,
(effpondNAT_TOP_PTRENFA_IND/NB_TOT_REF) AS FREQ_effpondNAT_TOP_PTRENFA_IND,
(effpondNAT_TOP_RDIALYSE_IND/NB_TOT_REF) AS FREQ_effpondNAT_TOP_RDIALYSE_IND,
(effpondNAT_TOP_RTRANS_AIG/NB_TOT_REF) AS FREQ_effpondNAT_TOP_RTRANS_AIG,
(effpondNAT_TOP_RTRANS_CHR/NB_TOT_REF) AS FREQ_effpondNAT_TOP_RTRANS_CHR,
(effpondNAT_TOP_ALCOOL/NB_TOT_REF) AS FREQ_effpondNAT_TOP_ALCOOL,
(effpondNAT_TOP_TABAC/NB_TOT_REF) AS FREQ_effpondNAT_TOP_TABAC,
(effpondNAT_TOP_canabis/NB_TOT_REF) AS FREQ_effpondNAT_TOP_canabis,
(effpondNAT_TOP_ADDICT_AUT/NB_TOT_REF) AS FREQ_effpondNAT_TOP_ADDICT_AUT
from SASDATA1.nat
group by 1;
quit;



proc sql;
create table com_sortie2 as select 
code_insee_cor,
(TOP_ABPCOIR_IND/NB_TOT_REF) AS FREQ_TOP_ABPCOIR_IND,
(TOP_CANAUTR_ACT/NB_TOT_REF) AS FREQ_TOP_CANAUTR_ACT,
(TOP_CANAUTR_SUR/NB_TOT_REF) AS FREQ_TOP_CANAUTR_SUR,
(TOP_CANCOLO_ACT/NB_TOT_REF) AS FREQ_TOP_CANCOLO_ACT,
(TOP_CANCOLO_SUR/NB_TOT_REF) AS FREQ_TOP_CANCOLO_SUR,
(TOP_CANPOUM_ACT/NB_TOT_REF) AS FREQ_TOP_CANPOUM_ACT,
(TOP_CANPOUM_SUR/NB_TOT_REF) AS FREQ_TOP_CANPOUM_SUR,
(TOP_CANPROS_ACT/NB_TOT_REF) AS FREQ_TOP_CANPROS_ACT,
(TOP_CANPROS_SUR/NB_TOT_REF) AS FREQ_TOP_CANPROS_SUR,
(TOP_CANSEIF_ACT/NB_TOT_REF) AS FREQ_TOP_CANSEIF_ACT,
(TOP_CANSEIF_SUR/NB_TOT_REF) AS FREQ_TOP_CANSEIF_SUR,
(TOP_CVAOMI_IND/NB_TOT_REF) AS FREQ_TOP_CVAOMI_IND,
(TOP_CVAUTRE_IND/NB_TOT_REF) AS FREQ_TOP_CVAUTRE_IND,
(TOP_CVAVC_AIG/NB_TOT_REF) AS FREQ_TOP_CVAVC_AIG,
(TOP_CVAVC_SEQ/NB_TOT_REF) AS FREQ_TOP_CVAVC_SEQ,
(TOP_CVCORON_CHR/NB_TOT_REF) AS FREQ_TOP_CVCORON_CHR,
(TOP_CVEMBOL_AIG/NB_TOT_REF) AS FREQ_TOP_CVEMBOL_AIG,
(TOP_CVIC_AIG/NB_TOT_REF) AS FREQ_TOP_CVIC_AIG,
(TOP_CVIC_CHR/NB_TOT_REF) AS FREQ_TOP_CVIC_CHR,
(TOP_CVIDM_AIG/NB_TOT_REF) AS FREQ_TOP_CVIDM_AIG,
(TOP_CVTRRYC_IND/NB_TOT_REF) AS FREQ_TOP_CVTRRYC_IND,
(TOP_CVVALVE_IND/NB_TOT_REF) AS FREQ_TOP_CVVALVE_IND,
(TOP_FANTIHTA_MED/NB_TOT_REF) AS FREQ_TOP_FANTIHTA_MED,
(TOP_FDIABET_IND/NB_TOT_REF) AS FREQ_TOP_FDIABET_IND,
(TOP_FHYPOLI_MED/NB_TOT_REF) AS FREQ_TOP_FHYPOLI_MED,
(TOP_HFOIPAN_IND/NB_TOT_REF) AS FREQ_TOP_HFOIPAN_IND,
(TOP_IRAUTRE_IND/NB_TOT_REF) AS FREQ_TOP_IRAUTRE_IND,
(TOP_IRCRRCH_IND/NB_TOT_REF) AS FREQ_TOP_IRCRRCH_IND,
(TOP_IRHEMOP_IND/NB_TOT_REF) AS FREQ_TOP_IRHEMOP_IND,
(TOP_IRMMHER_IND/NB_TOT_REF) AS FREQ_TOP_IRMMHER_IND,
(TOP_IRMUCO_IND/NB_TOT_REF) AS FREQ_TOP_IRMUCO_IND,
(TOP_IRPOLYA_IND/NB_TOT_REF) AS FREQ_TOP_IRPOLYA_IND,
(TOP_IRSPONA_IND/NB_TOT_REF) AS FREQ_TOP_IRSPONA_IND,
(TOP_IRVIH_IND/NB_TOT_REF) AS FREQ_TOP_IRVIH_IND,
(TOP_MATERNI_IND/NB_TOT_REF) AS FREQ_TOP_MATERNI_IND,
(TOP_NAUTRES_IND/NB_TOT_REF) AS FREQ_TOP_NAUTRES_IND,
(TOP_NDEMENC_IND/NB_TOT_REF) AS FREQ_TOP_NDEMENC_IND,
(TOP_NEPILEP_IND/NB_TOT_REF) AS FREQ_TOP_NEPILEP_IND,
(TOP_NMYOMYA_IND/NB_TOT_REF) AS FREQ_TOP_NMYOMYA_IND,
(TOP_NPARAPL_IND/NB_TOT_REF) AS FREQ_TOP_NPARAPL_IND,
(TOP_NPARKIN_IND/NB_TOT_REF) AS FREQ_TOP_NPARKIN_IND,
(TOP_NSEPLAQ_IND/NB_TOT_REF) AS FREQ_TOP_NSEPLAQ_IND,
(TOP_PADDICT_IND/NB_TOT_REF) AS FREQ_TOP_PADDICT_IND,
(TOP_PANTIDE_MED/NB_TOT_REF) AS FREQ_TOP_PANTIDE_MED,
(TOP_PANXIOL_MED/NB_TOT_REF) AS FREQ_TOP_PANXIOL_MED,
(TOP_PDEPNEV_IND/NB_TOT_REF) AS FREQ_TOP_PDEPNEV_IND,
(TOP_PHYPNOT_MED/NB_TOT_REF) AS FREQ_TOP_PHYPNOT_MED,
(TOP_PNEUROL_MED/NB_TOT_REF) AS FREQ_TOP_PNEUROL_MED,
(TOP_PRETARD_IND/NB_TOT_REF) AS FREQ_TOP_PRETARD_IND,
(TOP_PSYAUTR_IND/NB_TOT_REF) AS FREQ_TOP_PSYAUTR_IND,
(TOP_PSYCHOS_IND/NB_TOT_REF) AS FREQ_TOP_PSYCHOS_IND,
(TOP_PTRENFA_IND/NB_TOT_REF) AS FREQ_TOP_PTRENFA_IND,
(TOP_RDIALYSE_IND/NB_TOT_REF) AS FREQ_TOP_RDIALYSE_IND,
(TOP_RTRANS_AIG/NB_TOT_REF) AS FREQ_TOP_RTRANS_AIG,
(TOP_RTRANS_CHR/NB_TOT_REF) AS FREQ_TOP_RTRANS_CHR,
(TOP_ALCOOL/NB_TOT_REF) AS FREQ_TOP_ALCOOL,
(TOP_TABAC/NB_TOT_REF) AS FREQ_TOP_TABAC,
(TOP_canabis/NB_TOT_REF) AS FREQ_TOP_canabis,
(TOP_ADDICT_AUT/NB_TOT_REF) AS FREQ_TOP_ADDICT_AUT,
(effpondREG_TOP_ABPCOIR_IND/NB_TOT_REF) AS FREQ_effpondREG_TOP_ABPCOIR_IND,
(effpondREG_TOP_CANAUTR_ACT/NB_TOT_REF) AS FREQ_effpondREG_TOP_CANAUTR_ACT,
(effpondREG_TOP_CANAUTR_SUR/NB_TOT_REF) AS FREQ_effpondREG_TOP_CANAUTR_SUR,
(effpondREG_TOP_CANCOLO_ACT/NB_TOT_REF) AS FREQ_effpondREG_TOP_CANCOLO_ACT,
(effpondREG_TOP_CANCOLO_SUR/NB_TOT_REF) AS FREQ_effpondREG_TOP_CANCOLO_SUR,
(effpondREG_TOP_CANPOUM_ACT/NB_TOT_REF) AS FREQ_effpondREG_TOP_CANPOUM_ACT,
(effpondREG_TOP_CANPOUM_SUR/NB_TOT_REF) AS FREQ_effpondREG_TOP_CANPOUM_SUR,
(effpondREG_TOP_CANPROS_ACT/NB_TOT_REF) AS FREQ_effpondREG_TOP_CANPROS_ACT,
(effpondREG_TOP_CANPROS_SUR/NB_TOT_REF) AS FREQ_effpondREG_TOP_CANPROS_SUR,
(effpondREG_TOP_CANSEIF_ACT/NB_TOT_REF) AS FREQ_effpondREG_TOP_CANSEIF_ACT,
(effpondREG_TOP_CANSEIF_SUR/NB_TOT_REF) AS FREQ_effpondREG_TOP_CANSEIF_SUR,
(effpondREG_TOP_CVAOMI_IND/NB_TOT_REF) AS FREQ_effpondREG_TOP_CVAOMI_IND,
(effpondREG_TOP_CVAUTRE_IND/NB_TOT_REF) AS FREQ_effpondREG_TOP_CVAUTRE_IND,
(effpondREG_TOP_CVAVC_AIG/NB_TOT_REF) AS FREQ_effpondREG_TOP_CVAVC_AIG,
(effpondREG_TOP_CVAVC_SEQ/NB_TOT_REF) AS FREQ_effpondREG_TOP_CVAVC_SEQ,
(effpondREG_TOP_CVCORON_CHR/NB_TOT_REF) AS FREQ_effpondREG_TOP_CVCORON_CHR,
(effpondREG_TOP_CVEMBOL_AIG/NB_TOT_REF) AS FREQ_effpondREG_TOP_CVEMBOL_AIG,
(effpondREG_TOP_CVIC_AIG/NB_TOT_REF) AS FREQ_effpondREG_TOP_CVIC_AIG,
(effpondREG_TOP_CVIC_CHR/NB_TOT_REF) AS FREQ_effpondREG_TOP_CVIC_CHR,
(effpondREG_TOP_CVIDM_AIG/NB_TOT_REF) AS FREQ_effpondREG_TOP_CVIDM_AIG,
(effpondREG_TOP_CVTRRYC_IND/NB_TOT_REF) AS FREQ_effpondREG_TOP_CVTRRYC_IND,
(effpondREG_TOP_CVVALVE_IND/NB_TOT_REF) AS FREQ_effpondREG_TOP_CVVALVE_IND,
(effpondREG_TOP_FANTIHTA_MED/NB_TOT_REF) AS FREQ_effpondREG_TOP_FANTIHTA_MED,
(effpondREG_TOP_FDIABET_IND/NB_TOT_REF) AS FREQ_effpondREG_TOP_FDIABET_IND,
(effpondREG_TOP_FHYPOLI_MED/NB_TOT_REF) AS FREQ_effpondREG_TOP_FHYPOLI_MED,
(effpondREG_TOP_HFOIPAN_IND/NB_TOT_REF) AS FREQ_effpondREG_TOP_HFOIPAN_IND,
(effpondREG_TOP_IRAUTRE_IND/NB_TOT_REF) AS FREQ_effpondREG_TOP_IRAUTRE_IND,
(effpondREG_TOP_IRCRRCH_IND/NB_TOT_REF) AS FREQ_effpondREG_TOP_IRCRRCH_IND,
(effpondREG_TOP_IRHEMOP_IND/NB_TOT_REF) AS FREQ_effpondREG_TOP_IRHEMOP_IND,
(effpondREG_TOP_IRMMHER_IND/NB_TOT_REF) AS FREQ_effpondREG_TOP_IRMMHER_IND,
(effpondREG_TOP_IRMUCO_IND/NB_TOT_REF) AS FREQ_effpondREG_TOP_IRMUCO_IND,
(effpondREG_TOP_IRPOLYA_IND/NB_TOT_REF) AS FREQ_effpondREG_TOP_IRPOLYA_IND,
(effpondREG_TOP_IRSPONA_IND/NB_TOT_REF) AS FREQ_effpondREG_TOP_IRSPONA_IND,
(effpondREG_TOP_IRVIH_IND/NB_TOT_REF) AS FREQ_effpondREG_TOP_IRVIH_IND,
(effpondREG_TOP_MATERNI_IND/NB_TOT_REF) AS FREQ_effpondREG_TOP_MATERNI_IND,
(effpondREG_TOP_NAUTRES_IND/NB_TOT_REF) AS FREQ_effpondREG_TOP_NAUTRES_IND,
(effpondREG_TOP_NDEMENC_IND/NB_TOT_REF) AS FREQ_effpondREG_TOP_NDEMENC_IND,
(effpondREG_TOP_NEPILEP_IND/NB_TOT_REF) AS FREQ_effpondREG_TOP_NEPILEP_IND,
(effpondREG_TOP_NMYOMYA_IND/NB_TOT_REF) AS FREQ_effpondREG_TOP_NMYOMYA_IND,
(effpondREG_TOP_NPARAPL_IND/NB_TOT_REF) AS FREQ_effpondREG_TOP_NPARAPL_IND,
(effpondREG_TOP_NPARKIN_IND/NB_TOT_REF) AS FREQ_effpondREG_TOP_NPARKIN_IND,
(effpondREG_TOP_NSEPLAQ_IND/NB_TOT_REF) AS FREQ_effpondREG_TOP_NSEPLAQ_IND,
(effpondREG_TOP_PADDICT_IND/NB_TOT_REF) AS FREQ_effpondREG_TOP_PADDICT_IND,
(effpondREG_TOP_PANTIDE_MED/NB_TOT_REF) AS FREQ_effpondREG_TOP_PANTIDE_MED,
(effpondREG_TOP_PANXIOL_MED/NB_TOT_REF) AS FREQ_effpondREG_TOP_PANXIOL_MED,
(effpondREG_TOP_PDEPNEV_IND/NB_TOT_REF) AS FREQ_effpondREG_TOP_PDEPNEV_IND,
(effpondREG_TOP_PHYPNOT_MED/NB_TOT_REF) AS FREQ_effpondREG_TOP_PHYPNOT_MED,
(effpondREG_TOP_PNEUROL_MED/NB_TOT_REF) AS FREQ_effpondREG_TOP_PNEUROL_MED,
(effpondREG_TOP_PRETARD_IND/NB_TOT_REF) AS FREQ_effpondREG_TOP_PRETARD_IND,
(effpondREG_TOP_PSYAUTR_IND/NB_TOT_REF) AS FREQ_effpondREG_TOP_PSYAUTR_IND,
(effpondREG_TOP_PSYCHOS_IND/NB_TOT_REF) AS FREQ_effpondREG_TOP_PSYCHOS_IND,
(effpondREG_TOP_PTRENFA_IND/NB_TOT_REF) AS FREQ_effpondREG_TOP_PTRENFA_IND,
(effpondREG_TOP_RDIALYSE_IND/NB_TOT_REF) AS FREQ_effpondREG_TOP_RDIALYSE_IND,
(effpondREG_TOP_RTRANS_AIG/NB_TOT_REF) AS FREQ_effpondREG_TOP_RTRANS_AIG,
(effpondREG_TOP_RTRANS_CHR/NB_TOT_REF) AS FREQ_effpondREG_TOP_RTRANS_CHR,
(effpondREG_TOP_ALCOOL/NB_TOT_REF) AS FREQ_effpondREG_TOP_ALCOOL,
(effpondREG_TOP_TABAC/NB_TOT_REF) AS FREQ_effpondREG_TOP_TABAC,
(effpondREG_TOP_canabis/NB_TOT_REF) AS FREQ_effpondREG_TOP_canabis,
(effpondREG_TOP_ADDICT_AUT/NB_TOT_REF) AS FREQ_effpondREG_TOP_ADDICT_AUT,
(effpondNAT_TOP_ABPCOIR_IND/NB_TOT_REF) AS FREQ_effpondNAT_TOP_ABPCOIR_IND,
(effpondNAT_TOP_CANAUTR_ACT/NB_TOT_REF) AS FREQ_effpondNAT_TOP_CANAUTR_ACT,
(effpondNAT_TOP_CANAUTR_SUR/NB_TOT_REF) AS FREQ_effpondNAT_TOP_CANAUTR_SUR,
(effpondNAT_TOP_CANCOLO_ACT/NB_TOT_REF) AS FREQ_effpondNAT_TOP_CANCOLO_ACT,
(effpondNAT_TOP_CANCOLO_SUR/NB_TOT_REF) AS FREQ_effpondNAT_TOP_CANCOLO_SUR,
(effpondNAT_TOP_CANPOUM_ACT/NB_TOT_REF) AS FREQ_effpondNAT_TOP_CANPOUM_ACT,
(effpondNAT_TOP_CANPOUM_SUR/NB_TOT_REF) AS FREQ_effpondNAT_TOP_CANPOUM_SUR,
(effpondNAT_TOP_CANPROS_ACT/NB_TOT_REF) AS FREQ_effpondNAT_TOP_CANPROS_ACT,
(effpondNAT_TOP_CANPROS_SUR/NB_TOT_REF) AS FREQ_effpondNAT_TOP_CANPROS_SUR,
(effpondNAT_TOP_CANSEIF_ACT/NB_TOT_REF) AS FREQ_effpondNAT_TOP_CANSEIF_ACT,
(effpondNAT_TOP_CANSEIF_SUR/NB_TOT_REF) AS FREQ_effpondNAT_TOP_CANSEIF_SUR,
(effpondNAT_TOP_CVAOMI_IND/NB_TOT_REF) AS FREQ_effpondNAT_TOP_CVAOMI_IND,
(effpondNAT_TOP_CVAUTRE_IND/NB_TOT_REF) AS FREQ_effpondNAT_TOP_CVAUTRE_IND,
(effpondNAT_TOP_CVAVC_AIG/NB_TOT_REF) AS FREQ_effpondNAT_TOP_CVAVC_AIG,
(effpondNAT_TOP_CVAVC_SEQ/NB_TOT_REF) AS FREQ_effpondNAT_TOP_CVAVC_SEQ,
(effpondNAT_TOP_CVCORON_CHR/NB_TOT_REF) AS FREQ_effpondNAT_TOP_CVCORON_CHR,
(effpondNAT_TOP_CVEMBOL_AIG/NB_TOT_REF) AS FREQ_effpondNAT_TOP_CVEMBOL_AIG,
(effpondNAT_TOP_CVIC_AIG/NB_TOT_REF) AS FREQ_effpondNAT_TOP_CVIC_AIG,
(effpondNAT_TOP_CVIC_CHR/NB_TOT_REF) AS FREQ_effpondNAT_TOP_CVIC_CHR,
(effpondNAT_TOP_CVIDM_AIG/NB_TOT_REF) AS FREQ_effpondNAT_TOP_CVIDM_AIG,
(effpondNAT_TOP_CVTRRYC_IND/NB_TOT_REF) AS FREQ_effpondNAT_TOP_CVTRRYC_IND,
(effpondNAT_TOP_CVVALVE_IND/NB_TOT_REF) AS FREQ_effpondNAT_TOP_CVVALVE_IND,
(effpondNAT_TOP_FANTIHTA_MED/NB_TOT_REF) AS FREQ_effpondNAT_TOP_FANTIHTA_MED,
(effpondNAT_TOP_FDIABET_IND/NB_TOT_REF) AS FREQ_effpondNAT_TOP_FDIABET_IND,
(effpondNAT_TOP_FHYPOLI_MED/NB_TOT_REF) AS FREQ_effpondNAT_TOP_FHYPOLI_MED,
(effpondNAT_TOP_HFOIPAN_IND/NB_TOT_REF) AS FREQ_effpondNAT_TOP_HFOIPAN_IND,
(effpondNAT_TOP_IRAUTRE_IND/NB_TOT_REF) AS FREQ_effpondNAT_TOP_IRAUTRE_IND,
(effpondNAT_TOP_IRCRRCH_IND/NB_TOT_REF) AS FREQ_effpondNAT_TOP_IRCRRCH_IND,
(effpondNAT_TOP_IRHEMOP_IND/NB_TOT_REF) AS FREQ_effpondNAT_TOP_IRHEMOP_IND,
(effpondNAT_TOP_IRMMHER_IND/NB_TOT_REF) AS FREQ_effpondNAT_TOP_IRMMHER_IND,
(effpondNAT_TOP_IRMUCO_IND/NB_TOT_REF) AS FREQ_effpondNAT_TOP_IRMUCO_IND,
(effpondNAT_TOP_IRPOLYA_IND/NB_TOT_REF) AS FREQ_effpondNAT_TOP_IRPOLYA_IND,
(effpondNAT_TOP_IRSPONA_IND/NB_TOT_REF) AS FREQ_effpondNAT_TOP_IRSPONA_IND,
(effpondNAT_TOP_IRVIH_IND/NB_TOT_REF) AS FREQ_effpondNAT_TOP_IRVIH_IND,
(effpondNAT_TOP_MATERNI_IND/NB_TOT_REF) AS FREQ_effpondNAT_TOP_MATERNI_IND,
(effpondNAT_TOP_NAUTRES_IND/NB_TOT_REF) AS FREQ_effpondNAT_TOP_NAUTRES_IND,
(effpondNAT_TOP_NDEMENC_IND/NB_TOT_REF) AS FREQ_effpondNAT_TOP_NDEMENC_IND,
(effpondNAT_TOP_NEPILEP_IND/NB_TOT_REF) AS FREQ_effpondNAT_TOP_NEPILEP_IND,
(effpondNAT_TOP_NMYOMYA_IND/NB_TOT_REF) AS FREQ_effpondNAT_TOP_NMYOMYA_IND,
(effpondNAT_TOP_NPARAPL_IND/NB_TOT_REF) AS FREQ_effpondNAT_TOP_NPARAPL_IND,
(effpondNAT_TOP_NPARKIN_IND/NB_TOT_REF) AS FREQ_effpondNAT_TOP_NPARKIN_IND,
(effpondNAT_TOP_NSEPLAQ_IND/NB_TOT_REF) AS FREQ_effpondNAT_TOP_NSEPLAQ_IND,
(effpondNAT_TOP_PADDICT_IND/NB_TOT_REF) AS FREQ_effpondNAT_TOP_PADDICT_IND,
(effpondNAT_TOP_PANTIDE_MED/NB_TOT_REF) AS FREQ_effpondNAT_TOP_PANTIDE_MED,
(effpondNAT_TOP_PANXIOL_MED/NB_TOT_REF) AS FREQ_effpondNAT_TOP_PANXIOL_MED,
(effpondNAT_TOP_PDEPNEV_IND/NB_TOT_REF) AS FREQ_effpondNAT_TOP_PDEPNEV_IND,
(effpondNAT_TOP_PHYPNOT_MED/NB_TOT_REF) AS FREQ_effpondNAT_TOP_PHYPNOT_MED,
(effpondNAT_TOP_PNEUROL_MED/NB_TOT_REF) AS FREQ_effpondNAT_TOP_PNEUROL_MED,
(effpondNAT_TOP_PRETARD_IND/NB_TOT_REF) AS FREQ_effpondNAT_TOP_PRETARD_IND,
(effpondNAT_TOP_PSYAUTR_IND/NB_TOT_REF) AS FREQ_effpondNAT_TOP_PSYAUTR_IND,
(effpondNAT_TOP_PSYCHOS_IND/NB_TOT_REF) AS FREQ_effpondNAT_TOP_PSYCHOS_IND,
(effpondNAT_TOP_PTRENFA_IND/NB_TOT_REF) AS FREQ_effpondNAT_TOP_PTRENFA_IND,
(effpondNAT_TOP_RDIALYSE_IND/NB_TOT_REF) AS FREQ_effpondNAT_TOP_RDIALYSE_IND,
(effpondNAT_TOP_RTRANS_AIG/NB_TOT_REF) AS FREQ_effpondNAT_TOP_RTRANS_AIG,
(effpondNAT_TOP_RTRANS_CHR/NB_TOT_REF) AS FREQ_effpondNAT_TOP_RTRANS_CHR,
(effpondNAT_TOP_ALCOOL/NB_TOT_REF) AS FREQ_effpondNAT_TOP_ALCOOL,
(effpondNAT_TOP_TABAC/NB_TOT_REF) AS FREQ_effpondNAT_TOP_TABAC,
(effpondNAT_TOP_canabis/NB_TOT_REF) AS FREQ_effpondNAT_TOP_canabis,
(effpondNAT_TOP_ADDICT_AUT/NB_TOT_REF) AS FREQ_effpondNAT_TOP_ADDICT_AUT
from SASDATA1.com_sortie
group by 1;
quit;

