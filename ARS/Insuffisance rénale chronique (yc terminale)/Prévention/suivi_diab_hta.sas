/***********************************************/
/*Etablir un niveau de d�pistage biologique de l'IRC chez les patients risque vasculaire en r�gion Centre en 2010 */
/* Suivi Cr�atinine et Albuminerie chez les Patients Diab�tique Type 2 et Hypertendus **/


* Liste du ou des d�partement entre cote avec virgule si plusieurs;
%LET DEPT ='018','028','036','037','041','045';

* S�lection des m�dicaments et Affectation dans la Cat�gorie D diab�te et H HTA;
proc sql;
drop table med_irc;

PROC SQL;
   CREATE TABLE WORK.MED_IRC AS 
   SELECT DISTINCT /* ATC_4 */
                     (SUBSTR(t1.PHA_ATC_C07,1,4)) AS ATC_4, 
					 case  when substr(t1.PHA_ATC_C07,1,4) in('A10A','A10B') then 'D'
      when substr(t1.PHA_ATC_C07,1,4) in('C02A','C02C','C02D','C02L','C03A','CO3B','C03C','C03D','C03E',
                                     'C07A','C07B','C07C','C07D','C07F','C08C','C08D','C08E','C09A','CO9B','C09C','C09D','C09X') then 'H' end  AS top,
          t1.PHA_CIP_C13, 
          t1.PHA_NOM_COURT, 
          t1.PHA_ATC_C03, 
          t1.PHA_ATC_L03, 
          t1.PHA_ATC_C07, 
          t1.PHA_ATC_L07
      FROM ORAVUE.IR_PHA_R t1
      WHERE substr(t1.PHA_ATC_C07,1,4) in('C02A','C02B','C02C','C02D','C02L','C02N','C03A','CO3B','C03C','C03D','C03E','C03X',
                                     'C07A','C07B','C07C','C07D','C07E','C07F','C08C','C08D','C08E','C08G','C09A','CO9B','C09C','C09D','C09X')
									 OR substr(t1.PHA_ATC_C07,1,5) in('A10AB',
'A10AC',
'A10AD',
'A10AE',
'A10AF',
'A10BA',
'A10BB',
'A10BC',
'A10BD',
'A10BF',
'A10BG',
'A10BH',
'A10BJ',
'A10BK',
'A10BX',
'A10XA','C10BX','B01AC','C10AA','C10AX','C10BA')
OR t1.PHA_ATC_C07 in('C10BX03')


      ORDER BY ATC_4,t1.PHA_ATC_C07,
               t1.PHA_CIP_C13;


 
QUIT;

* On injecte dans ORACLE la table m�dicaments;
proc sql;
drop table orauser.med_irc;
create table orauser.med_irc as select * from med_irc;
quit;

* Recherche du nombre de d�livrance de chaque classe de m�dicaments et le dernier d�partement d'affectaton connu  � partir de la derni�re d�livrance trouv�e;
PROC SQL;
%connectora;
create table WORK.pop_diab_hta as
select * from connection to oracle (SELECT
          t1.BEN_IDT_ANO, 
        (COUNT(DISTINCT( case when  t2.TOP='D' then  t1.EXE_SOI_DTD end))) AS nb_deliv_diab,
        (COUNT(DISTINCT( case when  t2.TOP='H' then  t1.EXE_SOI_DTD end))) AS nb_deliv_hta,
       max(TO_CHAR(t1.EXE_SOI_DTD,'YYYYMMDD')||t1.BEN_RES_DPT)   AS max_affect
      FROM NS_PHA_F t1
           INNER JOIN MED_IRC t2 ON (t1.PHA_PRS_C13 = t2.PHA_CIP_C13)
      WHERE t1.EXE_SOI_AMD BETWEEN '201701' AND '201712' AND t2.top IN 
           (
           'D',
           'H'
           ) AND ( t1.PHA_ACT_QSC > 0 OR t1.PHA_DEC_QSD > 0 )
      GROUP BY  t1.BEN_IDT_ANO);
disconnect from oracle;
quit;

*--- On ne garde que la population qui nous int�resse � savoir 3 d�liv d'antidiab�tique et/ou 3 d�liv anti hypertenseurs;
* pour les d�partement de la R�gion cf Macro DEPT;

proc sql;

drop table un_pat;

create table un_pat as select distinct BEN_IDT_ANO ,SUBSTR(max_affect,9,3) AS DEPT_PAT,nb_deliv_diab,nb_deliv_hta,
case when (nb_deliv_diab>=3 AND nb_deliv_hta=0) then '1-Diab�tique sans HTA'
     when (nb_deliv_diab>=3 AND nb_deliv_hta>0) then '2-Diab�tique avec HTA'
     when (nb_deliv_diab=0 AND nb_deliv_hta>=3) then '3-HTA sans Diab�te'
     else '4-HTA avec m�dic. pour diab�te' end AS typ_pat
from pop_diab_hta
where (nb_deliv_hta>=3 OR nb_deliv_diab>=3) AND SUBSTR(max_affect,9,3) in (&DEPT);
quit;



/* Recherche des examens bio en ville */

PROC SQL;
   CREATE TABLE WORK.EXAM_BIO AS 
   SELECT DISTINCT t2.BEN_IDT_ANO,
		SUM(case when t2.BIO_PRS_IDE in(407,592,593) then t2.BIO_ACT_QSN else 0 end) AS  nb_creat,
			SUM(case when t2.BIO_PRS_IDE in(1133,2004) then t2.BIO_ACT_QSN else 0 end) AS nb_albu
      FROM  ORAVUE.NS_BIO_F t2 
           LEFT JOIN ORAVUE.NS_PRS_F t3 ON (t2.CLE_DCI_JNT = t3.CLE_DCI_JNT)
      WHERE (t3.ETE_IND_TAA NOT = 1 OR t3.ETE_IND_TAA IS MISSING) 
        AND (t2.EXE_SOI_AMD BETWEEN '201701' AND '201712' OR t2.EXE_SOI_AMD IS MISSING)
        AND (t2.BIO_PRS_IDE IN (407, 592, 593, 1133,2004) OR t2.BIO_PRS_IDE IS MISSING)
	GROUP BY t2.BEN_IDT_ANO;


QUIT;

* On ram�ne les r�sultats Bio en Ville dans  la table des patients s�lectionn�s; 
PROC SQL;
   CREATE TABLE WORK.PAT_EXAM_BIO AS 
   SELECT DISTINCT t1.BEN_IDT_ANO,t1.DEPT_PAT, t1.typ_pat,t1.nb_deliv_diab,t1.nb_deliv_hta,
              T2.nb_creat,
			 T2.nb_albu
      FROM UN_PAT t1
           LEFT JOIN EXAM_BIO t2 ON (t1.BEN_IDT_ANO = t2.BEN_IDT_ANO)
           ;
drop table 
QUIT;

/* Passage de DCIRS � PMSI Actes et consultation Externe */ 
/* On r�cup�re le BEN_NIR_PSA de IR_BEN_R correspondant aux BEN_IDT_ANO trouv�s dans la Conso m�dicament */ 

PROC SQL;
drop table orauser.un_pat;
create table orauser.un_pat as select distinct BEN_IDT_ANO from EXAM_BIO;

   CREATE TABLE WORK.UN_BEN_IDT AS 
   SELECT DISTINCT t2.BEN_NIR_PSA, 
          t1.BEN_IDT_ANO
      FROM orauser.un_pat t1
           INNER JOIN ORAVUE.IR_BEN_R t2 ON (t1.BEN_IDT_ANO = t2.BEN_IDT_ANO);
QUIT;

/** recherche � l'h�pital en consultation externe les Examens Bio **/
/** Les codes bio ne sont pas toujours bien renseign�s obligation de prendre les diff�rents formats */ 
PROC SQL;
   CREATE TABLE WORK.EXABIO_EXT AS 
   SELECT t2.NIR_ANO_17,
            (SUM( case when t1.NABM_COD  IN ('407','592','593','0407','0592','0593','00000407','00000592','00000593') then t1.ACT_NBR else 0 end)) FORMAT=NUMX11. AS nb_creat_hosp, 
            (SUM( case when t1.NABM_COD  IN ('1133','2004','01133','02004','00001133','00002004') then t1.ACT_NBR else 0 end)) FORMAT=NUMX11. AS nb_albu_hosp          
      FROM ORAVUE.T_MCO17FLSTC t1, ORAVUE.T_MCO17CSTC t2
      WHERE (t1.ETA_NUM = t2.ETA_NUM AND t1.SEQ_NUM = t2.SEQ_NUM) AND t1.NABM_COD  IN ('407','592','593','1133','2004',
           '0407','0592','0593','01133','02004','00000407','00000592','00000593','00001133','00002004')
      GROUP BY t2.NIR_ANO_17;



QUIT;


PROC SQL;
   CREATE TABLE WORK.EXAM_EXT_RECUP AS 
   SELECT DISTINCT t1.BEN_IDT_ANO, 
          sum(t2.nb_albu_hosp) as nb_albu_hosp, 
          sum(t2.nb_creat_hosp) as nb_creat_hosp
      FROM WORK.UN_BEN_IDT t1
           INNER JOIN WORK.EXABIO_EXT t2 ON (t1.BEN_NIR_PSA = t2.NIR_ANO_17)
    group by t1.BEN_IDT_ANO;
QUIT;

proc sort  data=PAT_EXAM_BIO;
by BEN_IDT_ANO;
run;


proc sort  data=EXAM_EXT_RECUP;
by BEN_IDT_ANO;
run;

* On cr�e les TOP ALBU et Top Cr�at en prenant le max des Exam Bio trouv�s en ville et hopital;
data total_res;
	merge PAT_EXAM_BIO EXAM_EXT_RECUP;
by BEN_IDT_ANO;
top_creat_hosp=(nb_creat_hosp>0);
top_creat_ville=(nb_creat>0);

top_creat=max(top_creat_hosp,top_creat_ville);
top_albu_hosp=(nb_albu_hosp>0);
top_albu_ville=(nb_albu>0);

top_albu=max(top_albu_hosp,top_albu_ville);

if typ_pat ="" then delete; 
run;


* Edition des r�sultats pour les d�partements et la R�gion;
proc  sql;
create table sasdata1.res_suivi_dept2017 as
select distinct DEPT_PAT,typ_pat,
count (distinct BEN_IDT_ANO) as nb_benef_tot,
count (distinct case when  top_creat=1 OR top_albu=1 then BEN_IDT_ANO end) as nb_benef_avec_suivi,
put((count (distinct case when top_creat=1 OR top_albu=1 then BEN_IDT_ANO end)/count (distinct BEN_IDT_ANO))*100,Commax8.2)||'%' as taux_avec_suivi,
count (distinct case when  top_creat=1 AND top_albu=1 then BEN_IDT_ANO end) as nb_benef_creat_albu,
put((count (distinct case when top_creat=1 AND top_albu=1 then BEN_IDT_ANO end)/count (distinct BEN_IDT_ANO))*100,Commax8.2)||'%' as taux__creat_albu, 
count (distinct case when  top_creat=1 AND top_albu=0 then BEN_IDT_ANO end) as nb_benef_creat_seul,
put((count (distinct case when top_creat=1 AND top_albu=0 then BEN_IDT_ANO end)/count (distinct BEN_IDT_ANO))*100,Commax8.2)||'%' as taux__creat_seul,
count (distinct case when  top_creat=0 AND top_albu=1 then BEN_IDT_ANO end) as nb_benef_albu_seul,
put((count (distinct case when top_creat=0 AND top_albu=1 then BEN_IDT_ANO end)/count (distinct BEN_IDT_ANO))*100,Commax8.2)||'%' as taux__albu_seul,
count (distinct case when  top_creat=0 AND top_albu=0 then BEN_IDT_ANO end) as nb_benef_sans_suivi,
put((count (distinct case when top_creat=0 AND top_albu=0 then BEN_IDT_ANO end)/count (distinct BEN_IDT_ANO))*100,Commax8.2)||'%' as taux_sans_suivi
from TOTAL_RES
group by DEPT_PAT,typ_pat
UNION select DEPT_PAT,'5-Total Diab�tiques' AS typ_pat,
count (distinct BEN_IDT_ANO) as nb_benef_tot,
count (distinct case when  top_creat=1 OR top_albu=1 then BEN_IDT_ANO end) as nb_benef_avec_suivi,
put((count (distinct case when top_creat=1 OR top_albu=1 then BEN_IDT_ANO end)/count (distinct BEN_IDT_ANO))*100,Commax8.2)||'%' as taux_avec_suivi,
count (distinct case when  top_creat=1 AND top_albu=1 then BEN_IDT_ANO end) as nb_benef_creat_albu,
put((count (distinct case when top_creat=1 AND top_albu=1 then BEN_IDT_ANO end)/count (distinct BEN_IDT_ANO))*100,Commax8.2)||'%' as taux__creat_albu, 
count (distinct case when  top_creat=1 AND top_albu=0 then BEN_IDT_ANO end) as nb_benef_creat_seul,
put((count (distinct case when top_creat=1 AND top_albu=0 then BEN_IDT_ANO end)/count (distinct BEN_IDT_ANO))*100,Commax8.2)||'%' as taux__creat_seul,
count (distinct case when  top_creat=0 AND top_albu=1 then BEN_IDT_ANO end) as nb_benef_albu_seul,
put((count (distinct case when top_creat=0 AND top_albu=1 then BEN_IDT_ANO end)/count (distinct BEN_IDT_ANO))*100,Commax8.2)||'%' as taux__albu_seul,
count (distinct case when  top_creat=0 AND top_albu=0 then BEN_IDT_ANO end) as nb_benef_sans_suivi,
put((count (distinct case when top_creat=0 AND top_albu=0 then BEN_IDT_ANO end)/count (distinct BEN_IDT_ANO))*100,Commax8.2)||'%' as taux_sans_suivi
from TOTAL_RES
where substr(typ_pat,1,1) in('1','2','4')
group by DEPT_PAT
UNION select DEPT_PAT,'6-Total HTA' AS typ_pat,
count (distinct BEN_IDT_ANO) as nb_benef_tot,
count (distinct case when  top_creat=1 OR top_albu=1 then BEN_IDT_ANO end) as nb_benef_avec_suivi,
put((count (distinct case when top_creat=1 OR top_albu=1 then BEN_IDT_ANO end)/count (distinct BEN_IDT_ANO))*100,Commax8.2)||'%' as taux_avec_suivi,
count (distinct case when  top_creat=1 AND top_albu=1 then BEN_IDT_ANO end) as nb_benef_creat_albu,
put((count (distinct case when top_creat=1 AND top_albu=1 then BEN_IDT_ANO end)/count (distinct BEN_IDT_ANO))*100,Commax8.2)||'%' as taux__creat_albu, 
count (distinct case when  top_creat=1 AND top_albu=0 then BEN_IDT_ANO end) as nb_benef_creat_seul,
put((count (distinct case when top_creat=1 AND top_albu=0 then BEN_IDT_ANO end)/count (distinct BEN_IDT_ANO))*100,Commax8.2)||'%' as taux__creat_seul,
count (distinct case when  top_creat=0 AND top_albu=1 then BEN_IDT_ANO end) as nb_benef_albu_seul,
put((count (distinct case when top_creat=0 AND top_albu=1 then BEN_IDT_ANO end)/count (distinct BEN_IDT_ANO))*100,Commax8.2)||'%' as taux__albu_seul,
count (distinct case when  top_creat=0 AND top_albu=0 then BEN_IDT_ANO end) as nb_benef_sans_suivi,
put((count (distinct case when top_creat=0 AND top_albu=0 then BEN_IDT_ANO end)/count (distinct BEN_IDT_ANO))*100,Commax8.2)||'%' as taux_sans_suivi
from TOTAL_RES
where substr(typ_pat,1,1) in('2','3','4')
group by DEPT_PAT
UNION select DEPT_PAT,'7-Total Diab�te et HTA' AS typ_pat,
count (distinct BEN_IDT_ANO) as nb_benef_tot,
count (distinct case when  top_creat=1 OR top_albu=1 then BEN_IDT_ANO end) as nb_benef_avec_suivi,
put((count (distinct case when top_creat=1 OR top_albu=1 then BEN_IDT_ANO end)/count (distinct BEN_IDT_ANO))*100,Commax8.2)||'%' as taux_avec_suivi,
count (distinct case when  top_creat=1 AND top_albu=1 then BEN_IDT_ANO end) as nb_benef_creat_albu,
put((count (distinct case when top_creat=1 AND top_albu=1 then BEN_IDT_ANO end)/count (distinct BEN_IDT_ANO))*100,Commax8.2)||'%' as taux__creat_albu, 
count (distinct case when  top_creat=1 AND top_albu=0 then BEN_IDT_ANO end) as nb_benef_creat_seul,
put((count (distinct case when top_creat=1 AND top_albu=0 then BEN_IDT_ANO end)/count (distinct BEN_IDT_ANO))*100,Commax8.2)||'%' as taux__creat_seul,
count (distinct case when  top_creat=0 AND top_albu=1 then BEN_IDT_ANO end) as nb_benef_albu_seul,
put((count (distinct case when top_creat=0 AND top_albu=1 then BEN_IDT_ANO end)/count (distinct BEN_IDT_ANO))*100,Commax8.2)||'%' as taux__albu_seul,
count (distinct case when  top_creat=0 AND top_albu=0 then BEN_IDT_ANO end) as nb_benef_sans_suivi,
put((count (distinct case when top_creat=0 AND top_albu=0 then BEN_IDT_ANO end)/count (distinct BEN_IDT_ANO))*100,Commax8.2)||'%' as taux_sans_suivi
from TOTAL_RES
group by DEPT_PAT
UNION select DEPT_PAT,'8-Total Diab�tiques CNAMTS' AS typ_pat,
count (distinct BEN_IDT_ANO) as nb_benef_tot,
count (distinct case when  top_creat=1 OR top_albu=1 then BEN_IDT_ANO end) as nb_benef_avec_suivi,
put((count (distinct case when top_creat=1 OR top_albu=1 then BEN_IDT_ANO end)/count (distinct BEN_IDT_ANO))*100,Commax8.2)||'%' as taux_avec_suivi,
count (distinct case when  top_creat=1 AND top_albu=1 then BEN_IDT_ANO end) as nb_benef_creat_albu,
put((count (distinct case when top_creat=1 AND top_albu=1 then BEN_IDT_ANO end)/count (distinct BEN_IDT_ANO))*100,Commax8.2)||'%' as taux__creat_albu, 
count (distinct case when  top_creat=1 AND top_albu=0 then BEN_IDT_ANO end) as nb_benef_creat_seul,
put((count (distinct case when top_creat=1 AND top_albu=0 then BEN_IDT_ANO end)/count (distinct BEN_IDT_ANO))*100,Commax8.2)||'%' as taux__creat_seul,
count (distinct case when  top_creat=0 AND top_albu=1 then BEN_IDT_ANO end) as nb_benef_albu_seul,
put((count (distinct case when top_creat=0 AND top_albu=1 then BEN_IDT_ANO end)/count (distinct BEN_IDT_ANO))*100,Commax8.2)||'%' as taux__albu_seul,
count (distinct case when  top_creat=0 AND top_albu=0 then BEN_IDT_ANO end) as nb_benef_sans_suivi,
put((count (distinct case when top_creat=0 AND top_albu=0 then BEN_IDT_ANO end)/count (distinct BEN_IDT_ANO))*100,Commax8.2)||'%' as taux_sans_suivi
from TOTAL_RES
where substr(typ_pat,1,1) in('1','2') AND NB_DELIV_DIAB>=3
group by DEPT_PAT
order by DEPT_PAT,typ_pat;

quit;




proc  sql;
create table sasdata1.res_suivi_region2017 as
select distinct 'R�gion' as region,typ_pat,
count (distinct BEN_IDT_ANO) as nb_benef_tot,
count (distinct case when  top_creat=1 OR top_albu=1 then BEN_IDT_ANO end) as nb_benef_avec_suivi,
put((count (distinct case when top_creat=1 OR top_albu=1 then BEN_IDT_ANO end)/count (distinct BEN_IDT_ANO))*100,Commax8.2)||'%' as taux_avec_suivi,
count (distinct case when  top_creat=1 AND top_albu=1 then BEN_IDT_ANO end) as nb_benef_creat_albu,
put((count (distinct case when top_creat=1 AND top_albu=1 then BEN_IDT_ANO end)/count (distinct BEN_IDT_ANO))*100,Commax8.2)||'%' as taux__creat_albu, 
count (distinct case when  top_creat=1 AND top_albu=0 then BEN_IDT_ANO end) as nb_benef_creat_seul,
put((count (distinct case when top_creat=1 AND top_albu=0 then BEN_IDT_ANO end)/count (distinct BEN_IDT_ANO))*100,Commax8.2)||'%' as taux__creat_seul,
count (distinct case when  top_creat=0 AND top_albu=1 then BEN_IDT_ANO end) as nb_benef_albu_seul,
put((count (distinct case when top_creat=0 AND top_albu=1 then BEN_IDT_ANO end)/count (distinct BEN_IDT_ANO))*100,Commax8.2)||'%' as taux__albu_seul,
count (distinct case when  top_creat=0 AND top_albu=0 then BEN_IDT_ANO end) as nb_benef_sans_suivi,
put((count (distinct case when top_creat=0 AND top_albu=0 then BEN_IDT_ANO end)/count (distinct BEN_IDT_ANO))*100,Commax8.2)||'%' as taux_sans_suivi
from TOTAL_RES
group by 'R�gion',typ_pat
UNION select 'R�gion' as region,'5-Total Diab�tiques' AS typ_pat,
count (distinct BEN_IDT_ANO) as nb_benef_tot,
count (distinct case when  top_creat=1 OR top_albu=1 then BEN_IDT_ANO end) as nb_benef_avec_suivi,
put((count (distinct case when top_creat=1 OR top_albu=1 then BEN_IDT_ANO end)/count (distinct BEN_IDT_ANO))*100,Commax8.2)||'%' as taux_avec_suivi,
count (distinct case when  top_creat=1 AND top_albu=1 then BEN_IDT_ANO end) as nb_benef_creat_albu,
put((count (distinct case when top_creat=1 AND top_albu=1 then BEN_IDT_ANO end)/count (distinct BEN_IDT_ANO))*100,Commax8.2)||'%' as taux__creat_albu, 
count (distinct case when  top_creat=1 AND top_albu=0 then BEN_IDT_ANO end) as nb_benef_creat_seul,
put((count (distinct case when top_creat=1 AND top_albu=0 then BEN_IDT_ANO end)/count (distinct BEN_IDT_ANO))*100,Commax8.2)||'%' as taux__creat_seul,
count (distinct case when  top_creat=0 AND top_albu=1 then BEN_IDT_ANO end) as nb_benef_albu_seul,
put((count (distinct case when top_creat=0 AND top_albu=1 then BEN_IDT_ANO end)/count (distinct BEN_IDT_ANO))*100,Commax8.2)||'%' as taux__albu_seul,
count (distinct case when  top_creat=0 AND top_albu=0 then BEN_IDT_ANO end) as nb_benef_sans_suivi,
put((count (distinct case when top_creat=0 AND top_albu=0 then BEN_IDT_ANO end)/count (distinct BEN_IDT_ANO))*100,Commax8.2)||'%' as taux_sans_suivi
from TOTAL_RES
where substr(typ_pat,1,1) in('1','2','4')
group by 'R�gion'
UNION select 'R�gion' AS region,'6-Total HTA' AS typ_pat,
count (distinct BEN_IDT_ANO) as nb_benef_tot,
count (distinct case when  top_creat=1 OR top_albu=1 then BEN_IDT_ANO end) as nb_benef_avec_suivi,
put((count (distinct case when top_creat=1 OR top_albu=1 then BEN_IDT_ANO end)/count (distinct BEN_IDT_ANO))*100,Commax8.2)||'%' as taux_avec_suivi,
count (distinct case when  top_creat=1 AND top_albu=1 then BEN_IDT_ANO end) as nb_benef_creat_albu,
put((count (distinct case when top_creat=1 AND top_albu=1 then BEN_IDT_ANO end)/count (distinct BEN_IDT_ANO))*100,Commax8.2)||'%' as taux__creat_albu, 
count (distinct case when  top_creat=1 AND top_albu=0 then BEN_IDT_ANO end) as nb_benef_creat_seul,
put((count (distinct case when top_creat=1 AND top_albu=0 then BEN_IDT_ANO end)/count (distinct BEN_IDT_ANO))*100,Commax8.2)||'%' as taux__creat_seul,
count (distinct case when  top_creat=0 AND top_albu=1 then BEN_IDT_ANO end) as nb_benef_albu_seul,
put((count (distinct case when top_creat=0 AND top_albu=1 then BEN_IDT_ANO end)/count (distinct BEN_IDT_ANO))*100,Commax8.2)||'%' as taux__albu_seul,
count (distinct case when  top_creat=0 AND top_albu=0 then BEN_IDT_ANO end) as nb_benef_sans_suivi,
put((count (distinct case when top_creat=0 AND top_albu=0 then BEN_IDT_ANO end)/count (distinct BEN_IDT_ANO))*100,Commax8.2)||'%' as taux_sans_suivi
from TOTAL_RES
where substr(typ_pat,1,1) in('2','3','4')
group by 'R�gion'
UNION select 'R�gion' AS region,'7-Total Diab�te et HTA' AS typ_pat,
count (distinct BEN_IDT_ANO) as nb_benef_tot,
count (distinct case when  top_creat=1 OR top_albu=1 then BEN_IDT_ANO end) as nb_benef_avec_suivi,
put((count (distinct case when top_creat=1 OR top_albu=1 then BEN_IDT_ANO end)/count (distinct BEN_IDT_ANO))*100,Commax8.2)||'%' as taux_avec_suivi,
count (distinct case when  top_creat=1 AND top_albu=1 then BEN_IDT_ANO end) as nb_benef_creat_albu,
put((count (distinct case when top_creat=1 AND top_albu=1 then BEN_IDT_ANO end)/count (distinct BEN_IDT_ANO))*100,Commax8.2)||'%' as taux__creat_albu, 
count (distinct case when  top_creat=1 AND top_albu=0 then BEN_IDT_ANO end) as nb_benef_creat_seul,
put((count (distinct case when top_creat=1 AND top_albu=0 then BEN_IDT_ANO end)/count (distinct BEN_IDT_ANO))*100,Commax8.2)||'%' as taux__creat_seul,
count (distinct case when  top_creat=0 AND top_albu=1 then BEN_IDT_ANO end) as nb_benef_albu_seul,
put((count (distinct case when top_creat=0 AND top_albu=1 then BEN_IDT_ANO end)/count (distinct BEN_IDT_ANO))*100,Commax8.2)||'%' as taux__albu_seul,
count (distinct case when  top_creat=0 AND top_albu=0 then BEN_IDT_ANO end) as nb_benef_sans_suivi,
put((count (distinct case when top_creat=0 AND top_albu=0 then BEN_IDT_ANO end)/count (distinct BEN_IDT_ANO))*100,Commax8.2)||'%' as taux_sans_suivi
from TOTAL_RES
group by 'R�gion'
UNION select 'R�gion' AS region,'8-Total Diab�tiques CNAMTS' AS typ_pat,
count (distinct BEN_IDT_ANO) as nb_benef_tot,
count (distinct case when  top_creat=1 OR top_albu=1 then BEN_IDT_ANO end) as nb_benef_avec_suivi,
put((count (distinct case when top_creat=1 OR top_albu=1 then BEN_IDT_ANO end)/count (distinct BEN_IDT_ANO))*100,Commax8.2)||'%' as taux_avec_suivi,
count (distinct case when  top_creat=1 AND top_albu=1 then BEN_IDT_ANO end) as nb_benef_creat_albu,
put((count (distinct case when top_creat=1 AND top_albu=1 then BEN_IDT_ANO end)/count (distinct BEN_IDT_ANO))*100,Commax8.2)||'%' as taux__creat_albu, 
count (distinct case when  top_creat=1 AND top_albu=0 then BEN_IDT_ANO end) as nb_benef_creat_seul,
put((count (distinct case when top_creat=1 AND top_albu=0 then BEN_IDT_ANO end)/count (distinct BEN_IDT_ANO))*100,Commax8.2)||'%' as taux__creat_seul,
count (distinct case when  top_creat=0 AND top_albu=1 then BEN_IDT_ANO end) as nb_benef_albu_seul,
put((count (distinct case when top_creat=0 AND top_albu=1 then BEN_IDT_ANO end)/count (distinct BEN_IDT_ANO))*100,Commax8.2)||'%' as taux__albu_seul,
count (distinct case when  top_creat=0 AND top_albu=0 then BEN_IDT_ANO end) as nb_benef_sans_suivi,
put((count (distinct case when top_creat=0 AND top_albu=0 then BEN_IDT_ANO end)/count (distinct BEN_IDT_ANO))*100,Commax8.2)||'%' as taux_sans_suivi
from TOTAL_RES
where substr(typ_pat,1,1) in('1','2') AND NB_DELIV_DIAB>=3
group by 'R�gion'
order by typ_pat;

quit;

