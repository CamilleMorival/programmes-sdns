/*********************************************************************************************************************/
/* Requete Concernat infertilite 		    											 */
/* Date de Mise � Jour : Dec 2017 								             */
/*C�line LEROY    	
/*  Profils  84 : vue patient    				     */
/*********************************************************************************************************************/


*On r�cup�re les actes CCAM en ville ;

PROC SQL;
drop table infertilite;
%connectora;
create table infertilite as
select * from connection to oracle 
	(
	SELECT  distinct 
	t1.ETB_EXE_FIN,
	t1.ETB_PRE_FIN, 
	t1.PFS_PRE_NUM,
    t1.PFS_EXE_NUM, /* numero du PS executant*/ 
	t3.IDE_CPT_RSC,
		/*PSE_ACT_NAT,  nature d'activite du PS*/
	 t1.ben_idt_ano, 		/*EXE_SOI_DTD,*/
	t2.exo_mtf,
	t1.cam_act_cod,
	t1.cam_prs_ide,
	/*	ben_res_com, /* au moment du soin*/ 
	/*	ben_res_dpt,*/
         	sum(t1.CAM_ACT_QSN) as actqtt
	  FROM NS_CAM_F t1
           INNER JOIN NS_PRS_F t2 ON (t1.CLE_DCI_JNT = t2.CLE_DCI_JNT)
           LEFT JOIN BE_IDE_R t3 ON (t2.ETB_EXE_FIN = t3.IDE_ETA_NU8)
         WHERE t1.CAM_prs_ide in ('JHFB001','JSLD002','JHFB001','JJFC011','JJFJ001','JKHD002','JKHD003','JSEC001','JSED001','JSLD001','YYYY032','ZCQM007')
	and t1.CLe_dci_JNT=T2.cle_dci_jnt
	and t1.ETB_EXE_FIN=T3.IDE_ETA_nu8
	AND t1.EXE_SOI_DTD BETWEEN to_date('20160101','YYYYMMDD') AND to_date('20161231','YYYYMMDD')
	and(t2.ETE_IND_TAA not in (1) OR t2.ETE_IND_TAA is null) /* on enleve Fides*/
		AND t1.exe_soi_amd > '201512'
		and t1.cam_act_cod in ('1') /* on ne garde qu les codes activite 1 pour ne pas g�n�rer de doublons*/
		GROUP BY t1.ETB_EXE_FIN, t1.ETB_PRE_FIN,t1.BEN_IDT_ANO,t1.PFS_PRE_NUM,t1.PFS_EXE_NUM,t2.exo_mtf,t1.cam_prs_ide,
t1.cam_act_cod, t3.IDE_CPT_RSC
	  /* AND t1.dpn_qlf not in (71)*/ /* pas n�cessaire ici, car d�j� filtre*/
		order by ben_idt_ano
     ) ;
disconnect from oracle;
quit;

PROC SQL;
select count(*) from infertilite where ETB_exe_fin=''
; 
quit;

/*on envoie la table sur orauser pour pouvoir la jointer avec une table oracle */

proc sql;
drop table ORAUSER.infertilite;
create table ORAUSER.infertilite as select * from infertilite ; quit;




/* chainage avec la table IR_BEN_R pour r�cuperer la variable BEN_NIR_PSA ; */ 

PROC SQL;
drop table sasdata1.infertilite2;
%connectora;
create table sasdata1.infertilite2 as
select * from connection to oracle (
select t1.*, t2.ben_nir_psa as NIR_ANO_17
from infertilite t1 left join IR_BEN_R t2
on t1.ben_idt_ano=t2.ben_idt_ano
    );
disconnect from oracle;
quit;

* modication de la variable exo_mtf ;
proc sql;
select distinct (exo_mtf), count(*) from sasdata1.infertilite2
group by exo_mtf;
quit;

data sasdata1.infertilite2; set sasdata1.infertilite2;
if exo_mtf not in (0) then exo=1;
else exo=0;
run;
proc sql;
select exo,count(*) from sasdata1.infertilite2
group by exo;
quit;

** formatage table de sortie ;
proc sql;
create table  infertil_normandie as select 
ETB_EXE_FIN as etb_exe,
CAM_PRS_IDE as acte,
IDE_CPT_RSC as raison_sociale,
count (distinct NIR_ANO_17) as nb_patient,
sum(actqtt) as actqtt
from sasdata1.infertilite2
/*where substr(etb_exe_fin,1,2) in ('14','50','61',')*/
group by etb_exe, acte,raison_sociale,acte;
quit;
data infertil_normandie;set infertil_normandie;
if etb_exe not in ('50000235') then delete;
run;



* on recupere l'activite externe pour le public dans fichier du PMSI rsfa MCO   ;

PROC SQL;
drop table sasdata1.infertilite_public;
%connectora;
create table sasdata1.infertilite_PUBLIC as
select * from connection to oracle (
   SELECT t1.NIR_ANO_17, 
         t1.ETA_NUM, 
		 t4.soc_rai,
      /*    t1.SEQ_NUM, */
	 /* t1.EXE_SOI_DTD,*/ 
        /*  t1.EXE_SOI_DTF,*/
	  t2.CCAM_COD,
	  T3.bdi_cod,
	sum(t2.ACV_act) as nbr_act
	/*t3.exo_tm*/
      FROM T_MCO16_12CSTC t1, T_MCO16_12FMSTC t2, T_MCO16_12FASTC t3, T_MCO16E t4
	  /* la table T_MCO16_12CSTC est la seule qui contient le NIR_ANO_17;*/
      WHERE t1.ETA_NUM = t2.ETA_NUM AND t1.SEQ_NUM = t2.SEQ_NUM 
	  and t1.ETA_NUM=T3.ETA_NUM and t1.SEQ_NUM=T3.SEQ_NUM
	  and t1.eta_num=t4.eta_num
          and ACV_act in ('1')	/* on ne garde que les 1 et enl�ve les codes activit� pour ne pas doublonner les actes*/
	 AND  t2.CCAM_COD in ('JJFJ001', 'JJFC011','JJFJ001','JJFC011', 'JHFB001','JSLD002','JHFB001','JJFC011','JJFJ001','JKHD002','JKHD003','JSEC001','JSED001','JSLD001','YYYY032','ZCQM007')
 	and SUBSTR(bdi_cod,1,2) in ('14','50','61','27','76')
GROUP BY t1.NIR_ANO_17,t1.ETA_NUM, T2.CCAM_COD,t3.bdi_cod,/*,t3.exo_tm,*/ t4.soc_rai
	order by nir_ano_17
   );
  disconnect from oracle;
quit;

data sasdata1.infertilite_public; set sasdata1.infertilite_public;
if exo_tm not in (0) then exo=1;
else exo=0;
run;
proc sql;
create table infertil2_normandie as 
select ETA_NUM as etb_exe,
CCAM_COD as acte,
soc_rai as raison_sociale,
count (distinct NIR_ANO_17) as nb_patient,
sum(nbr_act) as actqtt/*,
exo */
from sasdata1.infertilite_public
/*where substr(eta_num,1,2) in ('14','50','61','27','76')*/
group by etb_exe, acte,raison_sociale/*, exo*/;
quit;


data inferil3_normandie;
set infertil_normandie infertil2_normandie;
run;


***sejours pour prelevement d'ovocyte  ;
PROC SQL;
drop table sej_fertilite;
%connectora;
create table sej_fertilite as
select * from connection to oracle (
	SELECT t2.NIR_ANO_17,
		t1.ETA_NUM,
	/*	t1.RSA_NUM,*/
		t1.GRG_GHM,
		t1.DGN_PAL,
		t4.soc_rai,
		/*t3.exo_TM,*/
	count (distinct (T1.ETA_NUM||t1.RSA_NUM)) as nb_sej /* on compte les s�jours ;
	/*	t2.exe_soi_dtd as  date_deb,*/
	/*	t2.exe_soi_dtf as date_fin*/
	FROM  T_MCO16B t1, T_MCO16C t2/*, T_MCO16FB t3*/, T_MCO16E t4
	WHERE t1.rsa_num=t2.rsa_num 
	AND T1.eta_num=t2.eta_num
/*	and t1.eta_num=T3.eta_num
	and t1.rsa_num=t3.rsa_num*/
	and t1.eta_num=T4.eta_num
	AND substr(t1.GRG_GHM,1,5) in ('13C16','13M08','13C19') /*or substr(t1.GRG_GHM,1,5) in ('13M08','13C19') */
	AND  t2.EXE_SOI_DTD BETWEEN to_date('01012016','DDMMYYYY') AND to_date('31122016','DDMMYYYY')
	and substr(T1.bdi_cod,1,2) in ('14','50','61','27','76') 
	and T1.eta_num  not in
('130780521', '130783236', '130783293', '130784234', '130804297', '600100101', '750041543', '750100018', '750100042', '750100075',
'750100083', '750100091', '750100109', '750100125', '750100166', '750100208', '750100216', '750100232', '750100273', '750100299' ,
'750801441', '750803447', '750803454', '910100015', '910100023', '920100013', '920100021', '920100039', '920100047', '920100054',
'920100062', '930100011', '930100037', '930100045', '940100027', '940100035', '940100043', '940100050', '940100068', '950100016',
'690783154', '690784137', '690784152', '690784178', '690787478', '830100558')
	GROUP BY t2.NIR_ANO_17,t1.DGN_PAL,t1.eta_num,t1.GRG_GHM/*,t3.exo_tm*/, t4.soc_rai
	   );
  disconnect from oracle;
  quit;
/*** verification du DP pour enlever le dp Z5280 qui correspond � des dons d'ovocyte ;*/

  PROC SQL;
select count(*) from sej_fertilite where 
DGN_PAL in ('Z5280')
;
quit;

data sej_fertilite; set sej_fertilite;
if DGN_PAL in ('Z5280') then delete;
run;

/* table de sortie pour les sejours ;*/
proc sql;
create table sej_infertilite as select 
ETA_NUM,
soc_rai,
sum(nb_sej) as nb_sejours,
count(distinct NIR_ANO_17) as nb_patient,
substr(GRG_GHM,1,5)as ghm5
FROM Sej_fertilite
/*where substr(eta_num,1,2) in ('14','50','61','27','76')*/
group by eta_num, soc_rai,ghm5;
quit;



/** verification du codage GHM pour un actes CCAM particulier ;*/

PROC SQL;
%connectora;
select * from connection to oracle (
	SELECT count(*), T1.GRG_GHM
	FROM  T_MCO16A t3, T_MCO16B t1
	WHERE 
	t3.CDC_act in ('JSEC001','JHFB001','JSLD002','JHFB001','JJFC011','JJFJ001','JKHD002','JKHD003','JSEC001','JSED001','JSLD001','YYYY032','ZCQM007')
	and t1.eta_num=T3.eta_num
	and t1.rsa_num=T3.rsa_num
	and T1.eta_num  not in
('130780521', '130783236', '130783293', '130784234', '130804297', '600100101', '750041543', '750100018', '750100042', '750100075',
'750100083', '750100091', '750100109', '750100125', '750100166', '750100208', '750100216', '750100232', '750100273', '750100299' ,
'750801441', '750803447', '750803454', '910100015', '910100023', '920100013', '920100021', '920100039', '920100047', '920100054',
'920100062', '930100011', '930100037', '930100045', '940100027', '940100035', '940100043', '940100050', '940100068', '950100016',
'690783154', '690784137', '690784152', '690784178', '690787478', '830100558')
GRoup by grg_ghm
	);
	disconnect from oracle ;
	quit;
  *creation d'une table agregation ; 
/* table de sortie pour les sejours ;*/
proc sql;
create table sej_infertilite as select 
ETA_NUM,
soc_rai,
sum(nb_sej) as nb_sejours,
count(distinct NIR_ANO_17) as nb_patient,
substr(GRG_GHM,1,5)as ghm5
FROM Sej_fertilite
where substr(eta_num,1,2) in ('14','50','61','27','76')
group by eta_num, soc_rai,ghm5;
quit;



  /*** recuperation des spermogrammes des priv�s; */



  PROC SQL;
drop table bio_inf;
%connectora;
create table bio_inf as
select * from connection to oracle 
	(
	SELECT  distinct 
	t1.ETB_EXE_FIN,
	t1.ETB_PRE_FIN, 
	t1.PFS_PRE_NUM,
    t1.PFS_EXE_NUM, /* numero du PS executant*/ 
		/*PSE_ACT_NAT,  nature d'activite du PS*/
	 t1.ben_idt_ano, 
	 T1.bio_prs_ide,
/*	 T3.Bio_inf_lib,*/
	 t3.IDE_CPT_RSC,
	 t2.ETE_IND_TAA,
	 /*EXE_SOI_DTD,*/
/*	t2.exo_mtf,*/
	/*	ben_res_com, /* au moment du soin*/ 
	/*	ben_res_dpt,*/
         	sum(t1.BIO_ACT_QSN) as actqtt
			FROM NS_BIO_F t1
           INNER JOIN NS_PRS_F t2 ON (t1.CLE_DCI_JNT = t2.CLE_DCI_JNT)
           LEFT JOIN BE_IDE_R t3 ON (t2.ETB_EXE_FIN = t3.IDE_ETA_NU8)
	/* FROM NS_BIO_F t1, NS_PRS_F t2, IR_BIO_R t3 left join BE_IDE_R t4 
         WHERE 	t1.CLe_dci_JNT=T2.cle_dci_jnt
	and T1.bio_prs_ide=t3.bio_prs_ide*/
	WHERE t1.EXE_SOI_DTD BETWEEN to_date('20160101','YYYYMMDD') AND to_date('20161231','YYYYMMDD')
	and t1.bio_prs_ide in (70,71)
	and (t2.ETE_IND_TAA not in (1) OR t2.ETE_IND_TAA is null)/* on enleve Fides*/
/*	and t1.ETB_EXE_fin=t4.IDE_ETA_NU8*/
AND t1.exe_soi_amd > '201512'
		GROUP BY t1.ETB_EXE_FIN, t1.ETB_PRE_FIN,t1.BEN_IDT_ANO,t1.PFS_PRE_NUM,t1.PFS_EXE_NUM,/*t2.exo_mtf,*/
t3.IDE_CPT_RSC,t1.bio_prs_ide/*,t3.bio_inf_lib*/,t2.ETE_IND_TAA
	  /* AND t1.dpn_qlf not in (71)*/ /* pas n�cessaire ici, car d�j� filtre*/
     ) ;
disconnect from oracle;
quit;
* on recup le NIR_ANO_17 
; 

proc sql;
drop table ORAUSER.bio_inf;
create table ORAUSER.bio_inf as select * from bio_inf ; quit;

/* chainage avec la table IR_BEN_R pour r�cuperer la variable BEN_NIR_PSA ; */ 

PROC SQL;
drop table sasdata1.bio_inf2;
%connectora;
create table sasdata1.bio_inf2 as
select * from connection to oracle (
select t1.*, t2.ben_nir_psa as NIR_ANO_17
from bio_inf t1 left join IR_BEN_R t2
on t1.ben_idt_ano=t2.ben_idt_ano
    );
disconnect from oracle;
quit;

*formatage de la table; 
proc sql;create table spermo as select 
ETB_EXE_FIN as etb_exe,
IDE_CPT_RSC as raison_sociale,
PFS_EXE_NUM as exec,
count (distinct NIR_ANO_17) as nb_patient,
sum(actqtt) as actqtt
from sasdata1.bio_inf2
/*where substr(etb_exe_fin,1,2) in ('14','50','61')*/
group by etb_exe, exec, raison_sociale;
quit;

proc sql;
create table spermo2 as select 
distinct * from spermo left join sasdata1.med_presc
on (exec=PFS_PFS_num);
quit;


* jointure avec la table des medecins pour recuperer l'excutant ;




/* recuperation des actes en externe public avec le d�tail du code biologie;*/
PROC SQL;
drop table sasdata1.inf_bio_pub;
%connectora;
create table sasdata1.inf_bio_pub as
select * from connection to oracle (
   SELECT t1.NIR_ANO_17, 
         t1.ETA_NUM, 
      /*    t1.SEQ_NUM, */
	 /* t1.EXE_SOI_DTD,*/ 
        /*  t1.EXE_SOI_DTF,*/
		 t3.soc_rai,
		 	  t2.NABM_COD,
	  sum(T2.act_nbr) as nbr_act
      FROM T_MCO16_12CSTC t1, T_MCO16_12FLSTC t2, T_MCO16E  t3, T_MCO16_12FASTC t4/* la table T_MCO16_12CSTC est la seule qui contient le NIR_ANO_17;*/
      WHERE t1.ETA_NUM = t2.ETA_NUM AND t1.SEQ_NUM = t2.SEQ_NUM 
	  and t1.ETA_num=t4.eta_num
	  and t1.seq_num=t4.seq_num
	  and t1.eta_num=t3.eta_num
	  and substr(t4.bdi_cod,1,2) in ('14','50','61','27','76')
	 AND  t2.NABM_COD in ('00000070','00000071')
	GROUP BY t1.NIR_ANO_17,t1.ETA_NUM, T2.NABM_COD,t3.soc_rai
   );
  disconnect from oracle;
quit;

proc sql;
create table spermo_pub as 
select ETA_NUM as etb_exe,
soc_rai as raison_sociale,
count (distinct NIR_ANO_17) as nb_patient,
sum(nbr_act) as actqtt
from sasdata1.inf_bio_pub
/*where substr(eta_num,1,2) in ('14','50','61','27','76')*/
group by etb_exe, raison_sociale;
quit;

data spermo_tot;
set spermo2 spermo_pub;
run;
/** R�cherche des executants pour spermo */
PROC SQL;
   CREATE TABLE WORK.MED_PRESC AS 
   SELECT DISTINCT t1.PFS_PFS_NUM, 
          t1.PFS_COD_POS, t1.PFS_NUM_VOI,
          t1.PFS_CPL_ADR, 
          t1.PFS_CPL_VOI, 
          t1.PFS_EXC_COM, 
          t1.PFS_EXC_NOM, 
          t1.PFS_LIB_COM, 
          t1.PFS_LIB_VOI, 
          t1.PFS_LIE_DIT,
		  t1.PFS_EXC_COM,max(t1.DTE_ANN_TRT ||t1.DTE_MOI_FIN) AS maxmois
      FROM PRFEXT.DA_PRA_R_ARS t1 
      WHERE 
	  substr(fis_cai_num,1,2) in ('14','50','61','27','76')
		 group by t1.PFS_PFS_NUM, 
          t1.PFS_COD_POS, t1.PFS_NUM_VOI,
          t1.PFS_CPL_ADR, 
          t1.PFS_CPL_VOI, 
          t1.PFS_EXC_COM, 
          t1.PFS_EXC_NOM, 
          t1.PFS_LIB_COM, 
          t1.PFS_LIB_VOI, 
          t1.PFS_LIE_DIT,
		  t1.PFS_EXC_COM
 ;
QUIT;

proc sql ;
/* On cr�e la table Oracle avec un seul num�ro de Prescripteur pour la jointure et en r�cup�rant le derni�re situation du num�ro de PS  */
drop table UNPRESC;

create table UNPRESC as select distinct PFS_PFS_NUM,max(maxmois) as maxps from MED_PRESC group by PFS_PFS_NUM ;
quit;
/*On garde la derni�re situation connue dans la base de r�f�rence*/
proc sql;
delete from MED_PRESC where PFS_PFS_NUM||maxmois NOT IN (SELECT PFS_PFS_NUM||maxps from UNPRESC);
quit;





















/* recuperation des actes en externe public avec le d�tail du code biologie;*/
PROC SQL;
drop table sasdata1.inf_bio_pub;
%connectora;
create table sasdata1.inf_bio_pub as
select * from connection to oracle (
   SELECT t1.NIR_ANO_17, 
         t1.ETA_NUM, 
      /*    t1.SEQ_NUM, */
	 /* t1.EXE_SOI_DTD,*/ 
        /*  t1.EXE_SOI_DTF,*/
		 t3.soc_rai,
	  t2.NABM_COD,
	  sum(T2.act_nbr) as nbr_act
      FROM T_MCO16_12CSTC t1, T_MCO16_12FLSTC t2, T_MCO16E  t3/* la table T_MCO16_12CSTC est la seule qui contient le NIR_ANO_17;*/
      WHERE t1.ETA_NUM = t2.ETA_NUM AND t1.SEQ_NUM = t2.SEQ_NUM 
	  and t1.eta_num=t3.eta_num
	 AND  t2.NABM_COD in ('00000070','00000071')
	GROUP BY t1.NIR_ANO_17,t1.ETA_NUM, T2.NABM_COD,t3.soc_rai
   );
  disconnect from oracle;
quit;

proc sql;
create table spermo_pub as 
select ETA_NUM as etb_exe,
soc_rai as raison_sociale,
"0" as exec,
count (distinct NIR_ANO_17) as nb_patient,
sum(nbr_act) as actqtt
from sasdata1.inf_bio_pub
where substr(eta_num,1,2) in ('14','50','61','27','76')
group by etb_exe,exec, raison_sociale;
quit;

data spermo_tot;
set spermo2 spermo_pub;
run;
